#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

#*******************************************************************************
#
# Generic ICS OS21 makefile.
#
#*******************************************************************************
#*******************************************************************************

################################################################################

LIBDIR = lib/$(OS)/$(CPU)$(MULTILIB)
OBJDIR = obj/$(OS)/$(CPU)$(MULTILIB)

################################################################################

# Main ICS library
ICSLIB       = ics

# EMBX Mailbox Emulation
EMBXMBOXLIB = embxmailbox

# EMBX Shell Emulation
EMBXLIB     = embxshell

# EMBX SHM Emulation
EMBXSHMLIB  = embxshm

# MME library
MMELIB      = mme

################################################################################

$(ICSLIB)_OBJS =				\
	$(OBJDIR)/ics/admin/admin$(OBJSFX).$(O) \
	$(OBJDIR)/ics/admin/admin_client$(OBJSFX).$(O) \
	$(OBJDIR)/ics/channel/channel$(OBJSFX).$(O) \
	$(OBJDIR)/ics/connect/connect$(OBJSFX).$(O) \
	$(OBJDIR)/ics/cpu/cpu$(OBJSFX).$(O) \
	$(OBJDIR)/ics/dyn/dyn$(OBJSFX).$(O) \
	$(OBJDIR)/ics/dyn/dyn_client$(OBJSFX).$(O) \
	$(OBJDIR)/ics/debug/debug$(OBJSFX).$(O) \
	$(OBJDIR)/ics/debug/debug_mem$(OBJSFX).$(O) \
	$(OBJDIR)/ics/debug/debug_mmap$(OBJSFX).$(O) \
	$(OBJDIR)/ics/elf/elf$(OBJSFX).$(O) \
	$(OBJDIR)/ics/event/event$(OBJSFX).$(O) \
	$(OBJDIR)/ics/heap/heap$(OBJSFX).$(O) \
	$(OBJDIR)/ics/init/ics_init$(OBJSFX).$(O) \
	$(OBJDIR)/ics/load/load$(OBJSFX).$(O) \
	$(OBJDIR)/ics/mailbox/mailbox$(OBJSFX).$(O)	\
	$(OBJDIR)/ics/msg/msg_cancel$(OBJSFX).$(O) \
	$(OBJDIR)/ics/msg/msg_recv$(OBJSFX).$(O) \
	$(OBJDIR)/ics/msg/msg_send$(OBJSFX).$(O) \
	$(OBJDIR)/ics/nsrv/nsrv$(OBJSFX).$(O) \
	$(OBJDIR)/ics/nsrv/nsrv_client$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/os21$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/os21_debug$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/os21_event$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/os21_task$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/os21_write$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/os21_write_r$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/vmemadapt$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/sleep$(OBJSFX).$(O) \
	$(OBJDIR)/ics/os21/irq$(OBJSFX).$(O) \
	$(OBJDIR)/ics/port/port$(OBJSFX).$(O) \
	$(OBJDIR)/ics/region/region$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_debug$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_init$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_channel_alloc$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_channel_open$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_channel_recv$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_channel_send$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_connect$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_stats$(OBJSFX).$(O) \
	$(OBJDIR)/ics/shm/shm_watchdog$(OBJSFX).$(O) \
	$(OBJDIR)/ics/stats/stats$(OBJSFX).$(O) \
	$(OBJDIR)/ics/transport/transport$(OBJSFX).$(O) \
	$(OBJDIR)/ics/watchdog/watchdog$(OBJSFX).$(O) \

# EMBX SHM Emulation
# This is ICS in disguise (with a dummy BSP)
$(EMBXSHMLIB)_OBJS =				\
	$($(ICSLIB)_OBJS)			\
	$(OBJDIR)/embx/embx_bsp$(OBJSFX).$(O) \

# EMBX Shell Emulation
$(EMBXLIB)_OBJS =				\
	$(OBJDIR)/embx/embx_shell$(OBJSFX).$(O) \
	$(OBJDIR)/embx/embx_transport$(OBJSFX).$(O) \

# EMBX Mailbox Emulation
$(EMBXMBOXLIB)_OBJS =					 \
	$(OBJDIR)/embxmailbox/embx_mailbox$(OBJSFX).$(O) \

# MME library
$(MMELIB)_OBJS =				  \
	$(OBJDIR)/mme/buffer/buffer$(OBJSFX).$(O) \
	$(OBJDIR)/mme/command/abort$(OBJSFX).$(O) \
	$(OBJDIR)/mme/command/command$(OBJSFX).$(O) \
	$(OBJDIR)/mme/command/kill$(OBJSFX).$(O) \
	$(OBJDIR)/mme/command/send$(OBJSFX).$(O) \
	$(OBJDIR)/mme/command/wait$(OBJSFX).$(O) \
	$(OBJDIR)/mme/debug/mme_debug$(OBJSFX).$(O) \
	$(OBJDIR)/mme/execution/execution$(OBJSFX).$(O) \
	$(OBJDIR)/mme/init/mme_init$(OBJSFX).$(O) \
	$(OBJDIR)/mme/init/term$(OBJSFX).$(O) \
	$(OBJDIR)/mme/manager/manager$(OBJSFX).$(O) \
	$(OBJDIR)/mme/manager/manager_client$(OBJSFX).$(O) \
	$(OBJDIR)/mme/memory/memory$(OBJSFX).$(O) \
	$(OBJDIR)/mme/messageq/messageq$(OBJSFX).$(O) \
	$(OBJDIR)/mme/msg/msg$(OBJSFX).$(O) \
	$(OBJDIR)/mme/receiver/receiver$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/alive$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/capability$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/cmd$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/instantiate$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/register$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/terminate$(OBJSFX).$(O) \
	$(OBJDIR)/mme/transformer/transformer$(OBJSFX).$(O) \
	$(OBJDIR)/mme/tune/tune$(OBJSFX).$(O) \

################################################################################

################################################################################

CCUSERFLAGS += \
	-Iinclude    \
	-Iinclude/bsp \
	-Isrc/ics/include \
	-Isrc/mme/include \
	-Isrc/embx/include \

################################################################################
# All libraries we want built

ALLLIBS = $(ICSLIB) $(EMBXMBOXLIB) $(EMBXLIB) $(EMBXSHMLIB) $(MMELIB)

################################################################################
# Main targets

all: build

build: libs bsp

################################################################################
# BSP stuff

# Default location of the BSP sources, but allow it to be overloaded
# so it can be placed in the STAPI SDK tree too
BSP_SRCDIR ?= ./src/bsp
BSP_INCDIR ?= ./src/bsp

# Templates and rules for generating BSP libraries
include makebsp.inc

# Per CPU type template instantiations for each SoC core
include $(BSP_SRCDIR)/makebsp$(CPU).inc

################################################################################
# Libraries

include makelib.inc

# Define all LIB build templates
$(foreach lib,$(ALLLIBS),$(eval $(call LIB_template,$(lib))))

libs: $(LIBS)

bsp: $(BSPLIBS)

################################################################################

$(OBJDIR)/%$(OBJSFX).$(O): src/%.c
	-@$(MKTREE) $(call DOSCMD,$(dir $@))
	$(CCBUILD) $<

$(OBJDIR)/%$(OBJSFX).$(O): src/%.S
	-@$(MKTREE) $(call DOSCMD,$(dir $@))
	$(CCBUILD) $<

################################################################################

clean:
	-$(RM) $(call DOSCMD, $(OBJS) $(LIBS) $(BSPLIBS))
	-$(RM) $(call DOSCMD, $(patsubst %.$(O),%.d,$(OBJS)))

################################################################################

-include $(patsubst %.$(O),%.d,$(OBJS))

################################################################################
