#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

#*******************************************************************************
#
# OS21 ST200 specific Multicom4 makefile.
#
#*******************************************************************************
#*******************************************************************************

CPU = st231
OS  = os21
RUNTIME = os21

################################################################################

ST200TOOLPREFIX = st200

CC = $(ST200TOOLPREFIX)cc -mcore=$(CPU)
#CCSTDFLAGS = -ansi -pedantic -Wall -mruntime=$(RUNTIME) $(CCDEPENDFLAGS)
CCSTDFLAGS = -std=c99 -Wall -mruntime=$(RUNTIME) $(CCDEPENDFLAGS)

# Debugging options
CCDEBUGFLAGS = -g
CCDEBUGFLAGS += $(DEBUG_CFLAGS)
#CCDEBUGFLAGS += -DDEBUG_ASSERT
#CCDEBUGFLAGS += -DDEBUG
CCMULTICOM_EXTRA_CFLAGS += $(MULTICOM_EXTRA_CFLAGS)

CCOPTFLAGS = -Os -fno-dismissible-load
CCFLAGS = $(CCSTDFLAGS) $(CCDEBUGFLAGS) $(CCOPTFLAGS) $(CCUSERFLAGS) $(CCOSPLUSFLAGS) $(CCMULTILIBFLAGS) $(CCMULTICOM_EXTRA_CFLAGS) 
CCBUILD = $(CC) $(CCFLAGS) -c -o $@

ifeq ($(MAKEDEPEND),1)
CCDEPENDFLAGS = -MD -MT $@
endif

AR      = $(ST200TOOLPREFIX)ar
ARFLAGS = -cr $(ARUSERFLAGS)
ARBUILD = $(AR) $(ARFLAGS) $@
RANLIB  = $(ST200TOOLPREFIX)ranlib

#
# Unix tool wrappers for Windoze users
#
include makeunix.inc

LIBPFX = lib
O = o
L = a

################################################################################

include makegen.inc

################################################################################
