#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

UNAME=$(shell uname)

ifeq (,$(UNAME))
# Assume we are running the Evil Windoze (TM)
UNAME=Windows
endif

ifeq (Windows,$(UNAME))

#
# The Evil Windoze (TM)
#
CP         = copy
DATE       = date /t
ECHO        = echo
MKDIR      = mkdir
# mkdir whinges about existing dirs
MKTREE     = mkdir 2>NUL
PWD        = cd
RM         = del 2>NUL /f /q
RMTREE     = rmdir /s /q
DOSCMD     = $(subst /,\,$1)

else

#
# Phew! we are running Unix
#

CP         = cp -f
DATE       = date
ECHO       = echo
MKDIR      = mkdir
MKTREE     = mkdir -p
PWD        = pwd
RM         = rm -f
RMTREE     = rm -rf
DOSCMD     = $1

endif
