#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

#
# Special rules to allow BSP source code out of tree
#
$(OBJDIR)/%$(OBJSFX).$(O): $(BSP_SRCDIR)/%.c
	-@$(MKTREE) $(call DOSCMD,$(dir $@))
	$(CCBUILD) $< -I$(BSP_INCDIR)

$(OBJDIR)/%$(OBJSFX).$(O): $(BSP_SRCDIR)/%.S
	-@$(MKTREE) $(call DOSCMD,$(dir $@))
	$(CCBUILD) $< -I$(BSP_INCDIR)

# Call BSP_OBJS()
#   $1 = SOC
#   $2 = CPU
#   $3 = CORE
#
# Generates a list of BSP object files by using wildcards;
#
#      $(SOC)/$(CPU)/$(CORE)/*.c
#      $(SOC)/$(CPU)/$(CORE)/*.S
#      $(SOC)/$(CPU)/*.c
#      $(SOC)/*.c
# 
BSP_OBJS = $(patsubst $(BSP_SRCDIR)/%,$(OBJDIR)/%,$(patsubst %.c,%$(OBJSFX).$(O),$(wildcard $(BSP_SRCDIR)/$1/$2/$3/*.c))) \
	    $(patsubst $(BSP_SRCDIR)/%,$(OBJDIR)/%,$(patsubst %.S,%$(OBJSFX).$(O),$(wildcard $(BSP_SRCDIR)/$1/$2/$3/*.S))) \
  	    $(patsubst $(BSP_SRCDIR)/%,$(OBJDIR)/%,$(patsubst %.c,%$(OBJSFX).$(O),$(wildcard $(BSP_SRCDIR)/$1/$2/*.c))) \
	    $(patsubst $(BSP_SRCDIR)/%,$(OBJDIR)/%,$(patsubst %.c,%$(OBJSFX).$(O),$(wildcard $(BSP_SRCDIR)/$1/*.c)))


# Call BSPLIB_template()
#   $1 = SOC
#   $2 = CORE
#
# Defines a template for building/linking BSP library.
# Also adds to the BSPLIBS and OBJS variables
#
# This template is then instantiated in the makebspst40.inc and makebspst231.inc
# files for each SoC core present
#
define BSPLIB_template
 $$(LIBDIR)/$(1)/$(2)/$$(LIBPFX)ics_bsp$$(LIBSFX).$$(L) : $$(call BSP_OBJS,$(1),$(CPU),$(2))
	-$$(MKTREE) $$(call DOSCMD,$$(dir $$@))
	-$$(RM) $$(call DOSCMD,$$@)
	$$(ARBUILD) $$^
	$$(RANLIB) $$@
 BSPLIBS += $$(LIBDIR)/$(1)/$(2)/$$(LIBPFX)ics_bsp$$(LIBSFX).$$(L)
 OBJS    += $$(call BSP_OBJS,$(1),$(CPU),$(2))
endef

