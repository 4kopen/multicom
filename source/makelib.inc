#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

define LIB_template
 $$(LIBDIR)/$$(LIBPFX)$(1)$$(LIBSFX).$$(L) : $$($(1)_OBJS)
	-$$(MKTREE) $$(call DOSCMD,$$(dir $$@))
	-$$(RM) $$(call DOSCMD,$$@)
	$$(ARBUILD) $$^
	$$(RANLIB) $$@
 LIBS += $$(LIBDIR)/$$(LIBPFX)$(1)$$(LIBSFX).$$(L)
 OBJS += $$($(1)_OBJS)
endef

define LIBSO_template
 $$(LIBDIR)/$$(LIBPFX)$(1)$$(LIBSFX).$$(SO) : $$($(1)_OBJS)
	-$$(MKTREE) $$(call DOSCMD,$$(dir $$@))
	-$$(RM) $$(call DOSCMD,$$@)
	$$(LDBUILD) $$^ -Wl,-soname=$$(LIBPFX)$(1)$$(LIBSFX).$$(SO) $$(LDADD)
 LIBS_SO += $$(LIBDIR)/$$(LIBPFX)$(1)$$(LIBSFX).$$(SO)
 OBJS_SO += $$($(1)_OBJS)
endef
