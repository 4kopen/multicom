/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _EMBXSHM_H
#define _EMBXSHM_H

/*
 * DEPRECTED EMBX SHM header file.
 *
 * Supplied to provide compatibility with Multicom 3.x
 */

#include <embx/embx_types.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define EMBXSHM_MAX_CPUS 8

typedef struct
{
    EMBX_CHAR  name[EMBX_MAX_TRANSPORT_NAME+1]; /* The name of this transport */
    EMBX_UINT  cpuID;                           /* Local CPU ID (0 == master) */
    EMBX_UINT  participants[EMBXSHM_MAX_CPUS];  /* 1 == participating in this transport */
    EMBX_UINT  pointerWarp;                     /* Offset when pointer warping */
                                                /* LocalAddr + offset == BusAddr */
                                                /* BusAddr - offset == LocalAddr */
    EMBX_UINT  maxPorts;                        /* Max ports supported (0 == no limit) */
    EMBX_UINT  maxObjects;                      /* Number of shared objects to support */
    EMBX_UINT  freeListSize;                    /* Number of pre-allocated free nodes per port */
    EMBX_VOID *sharedAddr;                      /* Address of base of shared memory */
    EMBX_UINT  sharedSize;                      /* Size of shared memory */
    EMBX_VOID *warpRangeAddr;                   /* Primary warp range base address */
    EMBX_UINT  warpRangeSize;                   /* Primary warp range size */
    EMBX_VOID *warpRangeAddr2;                  /* Secondary Warp range base address */
    EMBX_UINT  warpRangeSize2;                  /* Secondary Warp range size */
} EMBXSHM_MailboxConfig_t;

EMBX_Transport_t *EMBXSHM_mailbox_factory (EMBX_VOID *param);
EMBX_Transport_t *EMBXSHMC_mailbox_factory (EMBX_VOID *param);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _EMBXSHM_H */


/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 */
