/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

#ifndef __SH4REGTYPE_H
#define __SH4REGTYPE_H

#ifndef _SH4REG_ASM_
typedef volatile unsigned char *const sh4_byte_reg_t;
typedef volatile unsigned short *const sh4_word_reg_t;
typedef volatile unsigned int *const sh4_dword_reg_t;
__extension__ typedef volatile unsigned long long int *const sh4_gword_reg_t;

#define SH4_BYTE_REG(address) ((sh4_byte_reg_t) (address))
#define SH4_WORD_REG(address) ((sh4_word_reg_t) (address))
#define SH4_DWORD_REG(address) ((sh4_dword_reg_t) (address))
#define SH4_GWORD_REG(address) ((sh4_gword_reg_t) (address))
#define SH4_PHYS_REG(address) (((((unsigned int) (address)) & 0xe0000000) == 0xe0000000) ? ((unsigned int) (address)) : (((unsigned int) (address)) & 0x1fffffff))
#else /* _SH4REG_ASM_ */
#define SH4_PHYS_REG(address) ((((address) & 0xe0000000) == 0xe0000000) ? (address) : ((address) & 0x1fffffff))
#define SH4_BYTE_REG(address) (address)
#define SH4_WORD_REG(address) (address)
#define SH4_DWORD_REG(address) (address)
#define SH4_GWORD_REG(address) (address)
#endif /* _SH4REG_ASM_ */

#endif /* __SH4REGTYPE_H */
