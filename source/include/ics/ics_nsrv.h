/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_NSRV_H
#define _ICS_NSRV_H

/* Maximum length of an ICS Nameserver name (not including the '\0') */
#define ICS_NSRV_MAX_NAME			63

/* Maximum amount of object data that can be stored */
#define ICS_NSRV_MAX_DATA			8

typedef ICS_HANDLE   ICS_NSRV_HANDLE;

/*
 * Nameserver: Client side API functions
 */
ICS_EXPORT ICS_ERROR ICS_nsrv_add (const ICS_CHAR *name, ICS_VOID *data, ICS_SIZE size, ICS_UINT flags,
				   ICS_NSRV_HANDLE *handlep);
ICS_EXPORT ICS_ERROR ICS_nsrv_remove (ICS_NSRV_HANDLE handle, ICS_UINT flags);

ICS_EXPORT ICS_ERROR ICS_nsrv_lookup (const ICS_CHAR *name, ICS_UINT flags, ICS_LONG timeout, ICS_VOID *data, ICS_SIZE *sizep);

#endif /* _ICS_NSRV_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
