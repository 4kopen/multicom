/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_HEAP_H
#define _ICS_HEAP_H

/*
 * Heap management functions
 * Can be called before ICS is initialised
 */
typedef void * ICS_HEAP;

ICS_EXPORT ICS_ERROR  ics_heap_create (ICS_VOID *heapBase, ICS_SIZE heapSize, ICS_UINT flags, ICS_HEAP *heapp);
ICS_EXPORT ICS_ERROR  ics_heap_destroy (ICS_HEAP heap, ICS_UINT flags);

ICS_EXPORT ICS_VOID  *ics_heap_alloc (ICS_HEAP heap, ICS_SIZE size, ICS_MEM_FLAGS mflags);
ICS_EXPORT ICS_ERROR  ics_heap_free (ICS_HEAP heap, ICS_VOID *mem);

ICS_EXPORT ICS_VOID  *ics_heap_base (ICS_HEAP heap, ICS_MEM_FLAGS mflags);
ICS_EXPORT ICS_OFFSET ics_heap_pbase (ICS_HEAP heap);
ICS_EXPORT ICS_SIZE   ics_heap_size (ICS_HEAP heap);

#endif /* _ICS_HEAP_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
