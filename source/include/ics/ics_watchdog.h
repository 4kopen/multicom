/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_WATCHDOG_H
#define _ICS_WATCHDOG_H

typedef void *ICS_WATCHDOG;

typedef void (*ICS_WATCHDOG_CALLBACK)(ICS_WATCHDOG handle, ICS_VOID *param, ICS_UINT cpuNum);

/*
 * Watchdog operations
 */
ICS_EXPORT ICS_ERROR ICS_watchdog_add (ICS_WATCHDOG_CALLBACK callback, ICS_VOID *param,
				       ICS_ULONG cpuMask, ICS_UINT flags, ICS_WATCHDOG *handlep);
ICS_EXPORT ICS_ERROR ICS_watchdog_remove (ICS_WATCHDOG handle);

ICS_EXPORT ICS_ERROR ICS_watchdog_reprime (ICS_WATCHDOG handle, ICS_UINT cpu);

#endif /* _ICS_WATCHDOG_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
