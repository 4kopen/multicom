/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_CPU_H
#define _ICS_CPU_H

/*
 * Routines to start and reset companions CPUs
 */
/*  specific api kernel 3.10 remote proc */
ICS_EXPORT ICS_ERROR   ics_cpu_start_fw (const ICS_CHAR *fname, ICS_UINT cpuNum);
ICS_EXPORT ICS_ERROR   ics_cpu_start (ICS_OFFSET entryAddr, ICS_UINT cpuNum, ICS_UINT flags);
ICS_EXPORT ICS_ERROR   ics_cpu_reset (ICS_UINT cpuNum, ICS_UINT flags);
ICS_EXPORT void   ics_cpu_reset_all (void);

/*
 * Routines to query the BSP
 */
ICS_EXPORT ICS_INT     ics_cpu_self (void);
ICS_EXPORT ICS_ULONG   ics_cpu_mask (void);

/*
 * Routines to query the BSP, translating between logical CPU numbers and BSP CPU names
 */
ICS_EXPORT int         ics_cpu_lookup (const ICS_CHAR *cpuName);
ICS_EXPORT const char *ics_cpu_name (ICS_UINT cpuNum);
ICS_EXPORT const char *ics_cpu_type (ICS_UINT cpuNum);
ICS_EXPORT int         ics_cpu_index (ICS_UINT cpuNum);

#endif /* _ICS_CPU_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
