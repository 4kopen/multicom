/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_REGION_H
#define _ICS_REGION_H

typedef ICS_HANDLE   ICS_REGION;

/*
 * Address translation & Region management
 */
ICS_EXPORT ICS_ERROR ICS_region_add (ICS_VOID *map, ICS_OFFSET paddr, ICS_SIZE size, ICS_MEM_FLAGS flags,
				     ICS_ULONG cpuMask, ICS_REGION *regionp);
ICS_EXPORT ICS_ERROR ICS_region_remove (ICS_REGION region, ICS_UINT flags);

/* Convert virtual addresses to and from physical ones based on the registered regions */
ICS_EXPORT ICS_ERROR ICS_region_virt2phys (ICS_VOID *address, ICS_OFFSET *paddrp, ICS_MEM_FLAGS *mflagsp);
ICS_EXPORT ICS_ERROR ICS_region_phys2virt (ICS_OFFSET paddr, ICS_SIZE size, ICS_MEM_FLAGS mflags, ICS_VOID **addressp);
#if defined (__arm__) && defined __KERNEL__
ICS_ERROR ICS_region_phys2virt_mflags (ICS_OFFSET paddr, ICS_SIZE size, ICS_MEM_FLAGS *mflags, ICS_VOID **addressp);
ICS_EXPORT ICS_ERROR ics_region_mappeable (ICS_UINT cpuNum, ICS_OFFSET paddr, ICS_SIZE size, ICS_MEM_FLAGS *mflags);
#endif

#endif /* _ICS_REGION_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
