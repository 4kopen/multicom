/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_INIT_H
#define _ICS_INIT_H

/*
 * ICS_cpu_init flags
 */
typedef enum
{
  ICS_INIT_CONNECT_ALL       = 0x1,		/* Connect to all CPUs during init */
  ICS_INIT_WATCHDOG          = 0x2,		/* Install a CPU watchdog */

} ICS_INIT_FLAGS;

ICS_EXPORT ICS_ERROR        ICS_cpu_init (ICS_UINT flags);
ICS_EXPORT void             ICS_cpu_term (ICS_UINT flags);

ICS_EXPORT ICS_ERROR        ics_cpu_init (ICS_UINT cpuNum, ICS_ULONG cpuMask, ICS_UINT flags);

ICS_EXPORT ICS_ERROR        ICS_cpu_info (ICS_UINT *cpuNump, ICS_ULONG *cpuMaskp);

ICS_EXPORT const ICS_CHAR * ics_cpu_version (void);

ICS_EXPORT ICS_ERROR        ICS_cpu_version (ICS_UINT cpuNum, ICS_ULONG *versionp);

ICS_EXPORT ICS_ERROR        ICS_SetAffinity (ICS_UINT affinity_mask);

ICS_EXPORT ICS_UINT         ICS_GetAffinity (void);

#endif /* _ICS_INIT_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
