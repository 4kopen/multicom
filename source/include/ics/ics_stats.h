/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_STATS_H
#define _ICS_STATS_H

#define ICS_STATS_MAX_ITEMS		16	/* Maximum number of stats monitored per CPU */

#define ICS_STATS_MAX_NAME		31	/* Max stat name string len (not including the \0) */

typedef ICS_HANDLE ICS_STATS_HANDLE;
typedef ICS_UINT64 ICS_STATS_VALUE;
typedef ICS_UINT64 ICS_STATS_TIME;

/*
 * Statistic item types
 * Used to display them correctly
 */
typedef enum
{
  ICS_STATS_COUNTER		= 0x1,		/* Display value as a simple count (with delta) */
  ICS_STATS_RATE		= 0x2,		/* Display value as rate per second */
  ICS_STATS_PERCENT		= 0x4,		/* Display value as percentage of time */
  ICS_STATS_PERCENT100		= 0x8,		/* Display value at 100 - percentage of time */

} ICS_STATS_TYPE;

typedef void (*ICS_STATS_CALLBACK)(ICS_STATS_HANDLE handle, ICS_VOID *param);

/*
 * Array item returned by stats sample
 */
typedef struct ics_stats_item
{
  ICS_STATS_VALUE	value[2];
  ICS_STATS_TIME	timestamp[2];
  ICS_STATS_TYPE	type;
  ICS_CHAR              name[ICS_STATS_MAX_NAME+1];
} ICS_STATS_ITEM;

ICS_ERROR ICS_stats_sample (ICS_UINT cpuNum, ICS_STATS_ITEM *stats, ICS_UINT *nstatsp);
ICS_ERROR ICS_stats_update (ICS_STATS_HANDLE handle, ICS_STATS_VALUE value, ICS_STATS_TIME timestamp);
ICS_ERROR ICS_stats_add (const ICS_CHAR *name, ICS_STATS_TYPE type, 
			 ICS_STATS_CALLBACK callback, ICS_VOID *param, ICS_STATS_HANDLE *handlep);
ICS_ERROR ICS_stats_remove (ICS_STATS_HANDLE handle);

#endif /* _ICS_STATS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

