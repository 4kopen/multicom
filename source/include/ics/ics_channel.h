/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_CHANNEL_H
#define _ICS_CHANNEL_H

typedef ICS_HANDLE   ICS_CHANNEL;
typedef ICS_VOID *   ICS_CHANNEL_SEND;

/* Callback function for Channels */
typedef ICS_ERROR (*ICS_CHANNEL_CALLBACK) (ICS_CHANNEL channel, ICS_VOID *param, void *buf);

ICS_EXPORT ICS_ERROR ICS_channel_alloc (ICS_CHANNEL_CALLBACK callback,
					ICS_VOID *param,
					ICS_VOID *base, ICS_UINT nslots, ICS_UINT ssize, ICS_UINT flags,
					ICS_CHANNEL *channelp);

ICS_EXPORT ICS_ERROR ICS_channel_free (ICS_CHANNEL channel, ICS_UINT flags);

ICS_EXPORT ICS_ERROR ICS_channel_open (ICS_CHANNEL channel, ICS_UINT flags, ICS_CHANNEL_SEND *schannelp);
ICS_EXPORT ICS_ERROR ICS_channel_close (ICS_CHANNEL_SEND schannel);

ICS_EXPORT ICS_ERROR ICS_channel_send (ICS_CHANNEL_SEND channel, ICS_VOID *buf, ICS_SIZE size, ICS_UINT flags);
ICS_EXPORT ICS_ERROR ICS_channel_send_lock (ICS_CHANNEL_SEND channel, ICS_VOID *buf, ICS_SIZE size, ICS_UINT flags, int lock);
ICS_EXPORT ICS_ERROR ICS_channel_recv (ICS_CHANNEL channel, ICS_VOID **bufp, ICS_LONG timeout);
ICS_EXPORT ICS_ERROR ICS_channel_release (ICS_CHANNEL channel, ICS_VOID *buf);
ICS_EXPORT ICS_ERROR ICS_channel_release_locked (ICS_CHANNEL channel, ICS_VOID *buf);

ICS_EXPORT ICS_ERROR ICS_channel_unblock (ICS_CHANNEL channel);

#endif /* _ICS_CHANNEL_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
