/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_DYN_H
#define _ICS_DYN_H

typedef ICS_HANDLE ICS_DYN;

ICS_EXPORT ICS_ERROR ICS_dyn_load_file (ICS_UINT cpuNum, ICS_CHAR *fname, ICS_UINT flags,
					ICS_DYN parent, ICS_DYN *handlep);
ICS_EXPORT ICS_ERROR ICS_dyn_load_image (ICS_UINT cpuNum, ICS_CHAR *image, ICS_SIZE imageSize, ICS_UINT flags,
					 ICS_DYN parent, ICS_DYN *handlep);

ICS_EXPORT ICS_ERROR ICS_dyn_unload (ICS_DYN handle);

#endif /* _ICS_DYN_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
