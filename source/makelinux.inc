#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

#*******************************************************************************
#
# ST40 Linux userspace Multicom4 specific makefile.
#
#*******************************************************************************
#*******************************************************************************

# bz 46079
LIBDIR ?= lib/$(OS)/$(CPU)$(MULTILIB)
OBJDIR ?= obj/$(OS)/$(CPU)$(MULTILIB)
################################################################################

ics_OBJS = 							\
	$(OBJDIR)/ics/linux/ics_user_lib$(OBJSFX).$(O) 	\
	$(OBJDIR)/ics/debug/debug$(OBJSFX).$(O) 		\

mme_OBJS = 							\
	$(OBJDIR)/mme/linux/mme_user_lib$(OBJSFX).$(O) 	\
	$(OBJDIR)/mme/debug/mme_debug$(OBJSFX).$(O) 		\
	$(OBJDIR)/ics/linux/linux_user_task$(OBJSFX).$(O) 	\

ifeq ($(findstring arm, $(CROSS_COMPILE)),arm) 
mme_OBJS +=  $(OBJDIR)/mme_user/stub_posix_wrapper$(OBJSFX).$(O) $(OBJDIR)/mme_user/stub_user_lib$(OBJSFX).$(O)  
endif

$(LIBDIR)/$(LIBPFX)mme$(LIBSFX).$(SO): LDADD:=-lpthread
$(LIBDIR)/$(LIBPFX)ics$(LIBSFX).$(SO): LDADD:=-lpthread


################################################################################

CCUSERFLAGS += \
	-Iinclude    \
	-Isrc/ics/include \
	-Isrc/mme/include \


################################################################################
# Library names

ALLLIBS = mme ics

################################################################################
# Main targets

all : libs

################################################################################
# Libraries

-include makelib.inc

# Compile both a '.a' and '.so' library
# 
$(foreach lib,$(ALLLIBS),$(eval $(call LIB_template,$(lib))))

$(foreach lib,$(ALLLIBS),$(eval $(call LIBSO_template,$(lib))))

libs : $(LIBS) $(LIBS_SO)

################################################################################

$(OBJDIR)/%$(OBJSFX).$(O): src/%.c
	-@$(MKTREE) $(call DOSCMD,$(dir $@))
	$(CCBUILD) $<

################################################################################

clean:
	-$(RM) $(call DOSCMD, $(OBJS) $(LIBS) $(LIBS_SO))
	-$(RM) $(call DOSCMD, $(patsubst %.$(O),%.d,$(OBJS)))

################################################################################

-include $(patsubst %.$(O),%.d,$(OBJS))

################################################################################
