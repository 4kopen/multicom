/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <bsp/_bsp.h>

/*
 * Dummy BSP for EMBXSHM emulation library
 */

const char * bsp_cpu_name;

struct bsp_cpu_info bsp_cpus[1];

unsigned int bsp_cpu_count = 0;

struct bsp_mbox_regs bsp_mailboxes[1];

unsigned int bsp_mailbox_count = 0;

/* 
 * ST40 only
 * reset.c dummies
 */
struct bsp_reg_mask bsp_sys_reset_bypass[1];
unsigned int bsp_sys_reset_bypass_count = 0;

struct bsp_boot_address_reg bsp_sys_boot_registers[1];
struct bsp_reg_mask bsp_sys_reset_registers[1];
struct bsp_reg_mask bsp_sys_boot_enable[1];

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
