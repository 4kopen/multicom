/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

#ifdef __st200__
#include <stdio.h>
#include <stdlib.h>
#include <ics.h>
#include <os21.h>
#include <os21/st200.h>

#define ILC_MAX_INPUT_REGS (32)
/*  offset for ILC base to reach enable array */
#define ILC_ENABLE_OFFSET (32+32+64+32+32+64)*4
#define _TD_WEAK(x)	x __attribute__ ((weak))

_TD_WEAK (extern void * bsp_ilc_base_address);

typedef struct
{
       unsigned int enable             [ILC_MAX_INPUT_REGS];
}ilc_enable_t;

/*  register mask are saved in this structure */
ilc_enable_t _ilc_save;
int _ilc_saved;
/*
 * mask the ILC
 */
void _st200_ilc_mask (void)
{
	if ((&bsp_ilc_base_address) && (_ilc_saved == 0))
	{
		if (bsp_ilc_base_address)
		{
			unsigned int i;
			volatile ilc_enable_t * ilc = (ilc_enable_t *) (bsp_ilc_base_address+ILC_ENABLE_OFFSET);
			/*
			 * Basically we reset into a state where all interrupts
			 * are disabled.
			 */
			for (i=0; i < ILC_MAX_INPUT_REGS; i++)
			{
				_ilc_save.enable[i] = ilc->enable[i];
				ilc->enable[i] = 0x00000000;
			}
		}
		_ilc_saved = 1;
	}
}

/*
 * mask the ILC
 */
void _st200_ilc_unmask (void)
{
	if ((&bsp_ilc_base_address) && (_ilc_saved))
	{
		unsigned int i;
		volatile ilc_enable_t * ilc = (ilc_enable_t *) (bsp_ilc_base_address+ILC_ENABLE_OFFSET);
		/*
		 * Basically we reset into a state where all interrupts
		 * are disabled.
		 */
		for (i=0; i < ILC_MAX_INPUT_REGS; i++)
		{
			ilc->enable[i] = _ilc_save.enable[i];
		}
	}
	_ilc_saved = 0;
}

#define ST200_PERIPHERAL_BASE (* (volatile unsigned int *) 0xFFFFFFB0)

#define INTC_MASK_OFFSET 4*4

typedef struct _intc_reg_mask_s
{
  unsigned int group[4];

} _intc_mask_t;

_intc_mask_t _intc_save;
int _intc_saved;
/*
 * Reset the INTC
 */
void _st200_intc_mask (void)
{
	_intc_mask_t *_intcp = (_intc_mask_t *) (ST200_PERIPHERAL_BASE + INTC_MASK_OFFSET);

	if (_intc_saved == 0) {
	_intc_save.group[0] = _intcp->group[0];
    _intcp->group[0] = 0x00000000;
	_intc_save.group[2] = _intcp->group[2];
    _intcp->group[2] = 0x00000000;
    _intc_saved  =  1;
	}
}

void _st200_intc_unmask (void)
{
	_intc_mask_t *_intcp = (_intc_mask_t *) (ST200_PERIPHERAL_BASE + INTC_MASK_OFFSET);
	if (_intc_saved) {
     _intcp->group[0] = _intc_save.group[0];
    _intcp->group[2] = _intc_save.group[2];
    _intc_saved  =  0;
	}
}
#endif

