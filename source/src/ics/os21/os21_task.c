/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

static void ics_task_helper (void *param)
{
  _ICS_OS_TASK_INFO *t = (_ICS_OS_TASK_INFO *) param;

  /* Signal that we have started */
  _ICS_OS_EVENT_POST(&t->event);

  /* Run the actual task code */
  t->entry(t->param);
  
  /* Signal that we are stopping */
  _ICS_OS_EVENT_POST(&t->event);
 
  return;
}

ICS_ERROR ics_os_task_create (void (*entry)(void *), void *param, ICS_INT priority, const ICS_CHAR *name,
			      _ICS_OS_TASK_INFO *t)
{
  /* Fill out supplied task info struct */
  t->entry = entry;
  t->param = param;
  t->priority = priority;
  
  if (!_ICS_OS_EVENT_INIT(&t->event))
    return ICS_SYSTEM_ERROR;
  
  t->task = task_create(ics_task_helper, t, _ICS_OS_DEFAULT_STACK_SIZE, priority, name, 0);
  if (t->task == NULL)
    return ICS_SYSTEM_ERROR;
  
  /* Wait for task to start */
  _ICS_OS_EVENT_WAIT(&t->event, ICS_TIMEOUT_INFINITE, ICS_FALSE);
  
  return ICS_SUCCESS;
}

ICS_ERROR ics_os_task_destroy (_ICS_OS_TASK_INFO *t)
{
  int res = ICS_SUCCESS;
  
  if (t->task == NULL)
    return ICS_HANDLE_INVALID;

  /* Wait for task to signal exit */
  _ICS_OS_EVENT_WAIT(&t->event, ICS_TIMEOUT_INFINITE, ICS_FALSE);

  if (task_wait(&t->task, 1, TIMEOUT_INFINITY) == OS21_SUCCESS)
    task_delete(t->task);
  else
    res = ICS_SYSTEM_INTERRUPT;

  /* Mark as invalid */
  t->task = NULL;

  _ICS_OS_EVENT_DESTROY(&t->event);

  return res;
}

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
