/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

/*
 * Block forever waiting for EVENT to be signalled,
 * or timeout after timeout (ms) have expired
 * set timeout to ICS_TIMEOUT_INFINITE to wait forever
 *
 * Returns ICS_SUCCESS if signalled, ICS_SYSTEM_TIMEOUT otherwise
 */
ICS_ERROR ics_os_event_wait (_ICS_OS_EVENT ev, ICS_UINT timeout)
{
  int res;
  _ICS_OS_TIME time;
  
  ICS_ASSERT(ev);
    
  if (timeout != ICS_TIMEOUT_INFINITE)
  {
    /* OS21 semaphore timeout takes an absolute time value
     * so we calculate that here
     */
    time = time_plus(time_now(), _ICS_OS_TIME_MSEC2TICKS(timeout));
    res = semaphore_wait_timeout(ev, &time);
  }
  else
    /* Infinite wait */
    res = semaphore_wait(ev);

  if (res == OS21_FAILURE)
    return ICS_SYSTEM_TIMEOUT;
  
  return ICS_SUCCESS;
}

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
