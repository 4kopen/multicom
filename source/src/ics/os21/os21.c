/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

/* 
 * Translate a kernel virtual address to a phyiscal one 
 *
 * Sets the supplied mode flag accordingly to indicate CACHED or UNCACHED memory
 */
ICS_ERROR ics_os_virt2phys (ICS_VOID *vaddr, ICS_OFFSET *paddrp, ICS_MEM_FLAGS *flagp)
{
  unsigned int mode;

  ICS_MEM_FLAGS mflags;

  ICS_ASSERT(flagp);
  ICS_ASSERT(paddrp);

  /* OS21: This should work in both 29-bit and 32-bit modes */
  if (vmem_virt_to_phys(vaddr, (ICS_VOID *)paddrp))
    return ICS_HANDLE_INVALID;
  
  if (vmem_virt_mode(vaddr, &mode))
    return ICS_HANDLE_INVALID;

  /* Returned translated mode flags */
  mflags = 0;
  
  if ((mode & VMEM_CREATE_CACHED))
    mflags |= ICS_CACHED;
  
  if ((mode & VMEM_CREATE_UNCACHED))
    mflags |= ICS_UNCACHED;
  
  if ((mode & VMEM_CREATE_WRITE_BUFFER))
    mflags |= ICS_WRITE_BUFFER;

  *flagp = mflags;

  return ICS_SUCCESS;
}


#ifdef __st200__

/*
 * ST200 specific functions to enable and disable tbhe memory speculation feature
 */
ICS_ERROR ics_os_scu_enable (ICS_OFFSET paddr, ICS_SIZE size)
{
  int res;
  
  res = scu_enable_range((void *)paddr, size);
  
  return (res ? ICS_SYSTEM_ERROR : ICS_SUCCESS);
}

ICS_ERROR ics_os_scu_disable (ICS_OFFSET paddr)
{
  int res;
  
  res = scu_disable_range((void *)paddr);
  
  return (res ? ICS_SYSTEM_ERROR : ICS_SUCCESS);
}

/*  by default NULL pointers fixes sytem partition  */
partition_t *__shm_pp=NULL; ;
int ics_os_contig_create(void * pAddr, unsigned int size)
{
	void* vmemAddr = NULL;
	/*  mapped the region as cacheable */
	vmemAddr = vmem_create(pAddr, size,
			NULL, VMEM_CREATE_CACHED|VMEM_CREATE_WRITE|VMEM_CREATE_READ);
	if (!vmemAddr)
		return ICS_SYSTEM_ERROR;
	/* Purge any old cache mappings */
	_ICS_OS_CACHE_PURGE(vmemAddr, paddr, size);
	__shm_pp = partition_create_heap(vmemAddr, size);
	if (!__shm_pp) 
		return ICS_SYSTEM_ERROR;
	return ICS_SUCCESS;
}

/* directly extracted from osplus code */
void *osplus_align (void *addr, unsigned int align)
{
  ICS_ASSERT(addr != NULL);
  ICS_ASSERT((align > 0) && (align <(1<<16)));

  return (void *) (((unsigned int) addr + (align - 1)) & ~(align - 1));
}

void * ics_os_contig_alloc (ICS_SIZE size, ICS_SIZE align)
{/* from osplus allocator */

	char *memory;
	char *aligned = NULL;
	/* Work out the new size to allocate including all overhead */
    size += (sizeof (void *) + (align) - 1);

	memory = (char *) memory_allocate(__shm_pp, size);
	if (memory != NULL)
    {
      /* Align the memory and store the original pointer */
      aligned = osplus_align ((void *) ((unsigned int) memory + 4), align);
      *((unsigned int *) ((unsigned int) aligned - 4)) = (unsigned int) memory;
	}
	return (void*)aligned;
}

void _ics_os_contig_free (ICS_VOID *memory)
{
		memory = (void *) (*((unsigned int *) ((unsigned int) memory - 4)));
		memory_deallocate(__shm_pp, memory);
}

int ics_contig_status(int avail)
{
partition_status_t status;
	if (partition_status(__shm_pp, &status, 0) != OS21_SUCCESS)
		return -1;
	return (avail ? status.partition_status_free :
			status.partition_status_size -status.partition_status_free);
}

#endif /* __st200__ */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
