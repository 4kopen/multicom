/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#include <stdarg.h>

/* For some reason -std=c99 doesn't give us these definitions */
extern int snprintf(char *, size_t, const char *, ...);
extern int vsnprintf(char *, size_t, const char *, va_list);

/* 
 * ICS stdio interception 
 *
 * We use the linker --wrap _write_r() trick here. This changes all references
 * to FN() to __wrap_FN(). The original function can then be called as __real_FN()
 */

void __wrap__write_r (struct _reent *reent, int fd, const char *buf, unsigned int len)
{
  extern void __real__write_r (struct _reent *reent, int fd, const char *buf, unsigned int len);

  /* intercept any writes to stdout or stderr */
  if (1 == fd || 2 == fd) 
  {
    /* log the message to the cyclic debug buffer */
    ICS_DEBUG_MSG((ICS_CHAR *)buf, len);
  }

#ifdef __st200__  
  /* XXXX This crashes when in IRQ/exception context with the older ST200 compilers (6.2.1) */
  if (!_ICS_OS_TASK_INTERRUPT())
#endif
    /* Fall through and do the standard _write_r operation */
    __real__write_r(reent, fd, buf, len);
}

/*
 * With the older (ST200R4.1) compiler we need to intercept _write instead
 */
void __wrap__write (int fd, const char *buf, unsigned int len)
{
  extern void __real__write (int fd, const char *buf, unsigned int len);

  /* intercept any writes to stdout or stderr */
  if (1 == fd || 2 == fd) 
  {
    /* log the message to the cyclic debug buffer */
    ICS_DEBUG_MSG((ICS_CHAR *)buf, len);
  }

  /* Fall through and do the standard _write operation */
  __real__write(fd, buf, len);
}

#define STR_SIZE 512

void ics_os_printf (const char *fmt, ...)
{
  /* These APIs were changed in OS21 V2.4.X and later */
  extern int _os21_kernel_vsnprintf(char *start_ptr, size_t size, const char  *fmt, va_list ap) __attribute__ ((weak));
  extern int _kernel_vsnprintf(char *start_ptr, size_t size, const char  *fmt, va_list ap) __attribute__ ((weak));
  extern void _md_kernel_puts(const char*);
  
  task_t* task;
  int     interruptInfo;
  
  char    buf[STR_SIZE];
  
  va_list args;

  if (task_context(&task, &interruptInfo) == task_context_interrupt)
  {
    va_start(args, fmt);
  
    /* Test the weak OS21 V2.4.X symbol and use it if present */
    if (_os21_kernel_vsnprintf)
    {
      _os21_kernel_vsnprintf(buf, STR_SIZE, fmt, args);
    }	
    else
    {
      _kernel_vsnprintf(buf, STR_SIZE, fmt, args);
    }

    va_end(args);

    _md_kernel_puts("*");
    _md_kernel_puts(buf);
  }
  else 
  {
    /* Workaround for a bug on the ST200 where the interrupt context
     * debug messages seem to interfere and cause the LX to crash
     */
    interrupt_lock();
    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);
    fflush(stdout);      
    interrupt_unlock();
  }

}

/* Leave a spare char for '\0' termination */
#define BUF_SIZE (_ICS_DEBUG_LINE_SIZE-1)

/*
 * Write a prefix string to buffer of form 
 *
 *  <prefix>:fn:<num> 
 *
 * Returns number of characters written
 */
static
int prefix (char *buf, const char *prefix, const char *fn, unsigned int line)
{
  char *cp = buf;

  /* Copy in prefix string */
  while (*prefix != '\0')
    *cp++ = *prefix++;
  
  *cp++ = ':';

  /* Copy in fn string */
  while (*fn != '\0')
    *cp++ = *fn++;
  
  *cp++ = ':';
  
  /* Convert line number to decimal */
  do {
    const int b = 10;
    
    *cp++ = (line % b) + '0';
    line /= b;
  } while (line);
  
  *cp++ = ' ';

  return cp - buf;
}

/* ICS Debug printf routine
 * Prefixes the output and then displays it on stdout/stderr depending
 * on the setting of ics_debug_chan. Will log only to cyclic buffer if
 * called from Interrupt context
 */
void ics_debug_printf (const char *fmt, const char *fn, int line, ...)
{
  extern void _md_kernel_puts(const char*);

  int     size = 0;
  char    buf[BUF_SIZE+1];

  va_list args;

  ICS_BOOL inInterrupt = _ICS_OS_TASK_INTERRUPT();

  if (inInterrupt)
  {
    /* Prefix Interrupt messages (without using snprintf) */
    size = prefix(buf, "Interrupt", fn, line);

#if OS21_VERSION_MAJOR >= 3
    {
      extern int _os21_kernel_vsnprintf(char *start_ptr, size_t size, const char  *fmt, va_list ap) __attribute__ ((weak));
      
      va_start(args, line);
      size += _os21_kernel_vsnprintf(buf+size, BUF_SIZE-size, fmt, args);
      va_end(args);
    }
#else
    /* Unfortunately _kernel_vsnprintf() doesn't seem to work here */
    buf[size++] = '\n';
    buf[size++] = '\0';
#endif

    /* Always terminate with a newline (in case of truncation) */
    buf[BUF_SIZE-1] = '\n';
    buf[BUF_SIZE]   = '\0';
    
    if (_ics_debug_chan & (ICS_DBG_STDOUT|ICS_DBG_STDERR))
      /* Should be safe in IRQ context */
      /* kernel_printf("%s", buf); */
      _md_kernel_puts(buf);
    else
      /* log the msg into the cyclic buffer (only if ICS_DBG_LOG set) */
      ICS_DEBUG_MSG(buf, size);
  }
  else
  {
    /* Prefix debug msg with 'TaskName:FnName:Line' */
    size = snprintf(buf, BUF_SIZE, "%s:%s:%d ", _ICS_OS_TASK_NAME(), fn, line);

    /* Append the printf content */
    va_start(args, line);
    size += vsnprintf(buf+size, BUF_SIZE-size, fmt, args);
    va_end(args);
    
    /* Always terminate with a newline (in case of truncation) */
    buf[BUF_SIZE-1] = '\n';
    buf[BUF_SIZE]   = '\0';

    /* Log the message to stdout or stderr. These will be intercepted
     * by the _write_r wrapper and hence be logged into the cyclic buffer too
     */
    if (_ics_debug_chan & ICS_DBG_STDOUT)
      fwrite(buf, 1, size, stdout);
    else if (_ics_debug_chan & ICS_DBG_STDERR)
      fwrite(buf, 1, size, stderr);
    else
      /* log the msg into the cyclic buffer (only if ICS_DBG_LOG set) */
      ICS_DEBUG_MSG(buf, size);
  }
}

#ifdef MULITCOM_EXP_HANDLER

/*
 * This currently clashes with the Havana Post Mortem handler
 * Disbale for now until we can replace all that functionality in ICS
 */

/* Older OS21/st200 (V3.2.7) had a bug which would lead
 * to recursive exceptions happening. Trap them here
 */
void bsp_exp_handler (unsigned int exp_code)
{
  extern void _md_kernel_puts(const char*);

  static int recursion = 0;
  
  /* kernel_printf("OS21 took unexpected exception: 0x%x\n", exp_code); */
  if (recursion++)
  {
    _md_kernel_puts("ICS: RECURSIVE exception detected\n");
    while (1); 
  }
}

#endif /* MULITCOM_EXP_HANDLER */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
