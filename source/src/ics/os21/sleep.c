/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 ******************************************************************************/


#ifdef __st200__
#include <stdio.h>
#include <stdlib.h>
#include <ics.h>
#include <os21.h>
#include <os21/st200.h>
#define TASK_STACK_SIZE OS21_DEF_MIN_STACK_SIZE
#include <bsp/_bsp.h>

static task_t *_sleep_taskp;
static event_group_t *_sleep_events;
static event_group_t *_test_events;

/*  sleep_counter is set a 1 at boot */
/*  sleep_counter 0, sleep is possible */
/*  negative sleep_counter is not possible !!! */
int sleep_counter = 1;
/*  timer_suspended  */
static int timer_suspended = 0;
static interrupt_t *mailbox_handle_p;
 
/*  event triggering an attempt to sleep */
#define	SLEEP_NOW (0x00000001)
/*  event triggering a attempt to sleep later, unless a SLEEP_NOW is received */
#define SLEEP_LATER (0x00000002)
#define INSECONDS(A) (timer = time_plus(time_now(),time_ticks_per_sec()*A))
/*  guard timer to trigger sleep following reception of message , if sleep_counter
 *  is zero */
int InactivityLimit=5;
extern int ICS_sleep(void);
extern void _st200_ilc_mask (void);
extern void _st200_ilc_unmask(void);
extern void _st200_intc_mask(void);
extern void _st200_intc_unmask (void);

/*
 * sleep task
 * responsible for sleeping the st200
 */
static void _sleep_task(void *ptr __attribute__((unused)))
{
	int save;
	osclock_t *wake_time=TIMEOUT_INFINITY;
	int ret;
	/*  local va */
	osclock_t timer;
	/*  received event */
/* bz 73452
	ICS_debug_printf("_sleep_task started\n");
*/
	unsigned int event;
	while (1)
	{
		/* bz 73452
		if (wake_time==TIMEOUT_INFINITY) ICS_debug_printf("sleep wait event\n");
		else ICS_debug_printf("sleep wait timeout or event\n");
		*/
		ret = event_wait_all(_sleep_events, SLEEP_LATER, &event, wake_time);
		/*  by default no timer */
/* bz 73452
		ICS_debug_printf("_sleep_task ret %d event = %d timer_suspended %d\n",
				ret, event, timer_suspended);
*/
		//if (timer_suspended) while(1);
		wake_time = TIMEOUT_INFINITY;
		/*  timeout has occured */
		if (ret != OS21_SUCCESS) event = SLEEP_NOW;
/* bz 73452
		ICS_debug_printf("_sleep_task ret %d event = %d timer_suspended %d\n",
				ret, event, timer_suspended);
*/
		save = interrupt_mask_all();
		switch(event) {
		case SLEEP_LATER:
			if (sleep_counter > 0) break;
sleep_later:
			INSECONDS(InactivityLimit);
			wake_time = &timer;
			break;
		case SLEEP_NOW:
		case SLEEP_NOW | SLEEP_LATER:
			if (sleep_counter) break;
			/*  call ics service, that send message if sleep possible
			 *  return 1 sleep possible 
			 *  return 0 sleep not possible :
			 *  host not connected, or host sent
			 *  additional message since .
			 */
			ret = ICS_sleep();
/* bz 73452
			ICS_debug_printf("ICS_sleep ret %d\n",ret);
*/
			if (ret!=ICS_SUCCESS)
				goto sleep_later;
/* bz 73452			
			ICS_debug_printf("sleep asked time %u(s) %u\n", 
			time_now() / time_ticks_per_sec(), time_now());
*/
			/*  disable ilc and intc controler  */
			interrupt_disable(mailbox_handle_p);
			_st200_ilc_mask();
			_st200_intc_mask();
			/*  re-enable the mailbox interrupt (only this IT remains active,
			 *  this will wake up the system */
			interrupt_enable(mailbox_handle_p);

			timer_suspended = 1;
			break;
		}
		interrupt_unmask(save);
	}
}


/*
 * sleep_initialize
 * */

int sleep_initialize(void)
{
	int rc = -1;
	int i;

	_sleep_events = event_group_create(event_auto_clear);

	_test_events =  event_group_create(event_auto_clear);

	if (_sleep_events)
	{
		_sleep_taskp = task_create(_sleep_task, NULL, TASK_STACK_SIZE, OS21_MIN_USER_PRIORITY, "sleeping task", 0);
		if (_sleep_taskp)
			rc = 0;
		else
			event_group_delete(_sleep_events);
	}
	/*  parse bsp ressource to find mailboc interruption */
	for(i=0; i< bsp_mailbox_count; i++) {
		if (bsp_mailboxes[i].interrupt) {
			mailbox_handle_p = interrupt_handle(bsp_mailboxes[i].interrupt);
			break;
		}
	}

	return rc;
}

/*  called  */
void host_it_wake(void)
{
	int save = interrupt_mask_all();
	if (timer_suspended) {
/* bz 73452 
			ICS_debug_printf("wake up time %u(s) %u tick/s=%ld\n",
				time_now() / time_ticks_per_sec(), time_now(), time_ticks_per_sec());
*/
	_st200_ilc_unmask();
	_st200_intc_unmask();
	interrupt_enable(mailbox_handle_p);

		timer_suspended = 0;
/* bz 73452 
		ICS_debug_printf("wake up time %u(s) %u tick/s=%ld\n",
				time_now() / time_ticks_per_sec(), time_now(), time_ticks_per_sec());
		ICS_debug_printf("host_it wake up\n");
*/
		event_post(_sleep_events, SLEEP_LATER );
	}
	interrupt_unmask(save);
}


void sleeping(int number)
{
	int copy;
	interrupt_lock();
	sleep_counter += number;
	copy= sleep_counter;
	interrupt_unlock();
	if (copy == 0) {
		/*  possible SLEEP_NOW instead */
		event_post(_sleep_events, SLEEP_LATER );
/* bz 73452
		ICS_debug_printf("post event SLEEP_LATER\n");
*/
	}

}


void sleep_enable(void)
{
	sleeping(-1);
}

void sleep_disable(void)
{
	sleeping(1);
}

void sleep_fix_time(int limit)
{
	InactivityLimit=limit;
}
#endif

