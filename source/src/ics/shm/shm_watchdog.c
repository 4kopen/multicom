/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */


#include "_ics_shm.h"	/* SHM transport specific headers */

/*
 * Update our own watchdog timestamp in SHM
 */
void ics_shm_watchdog (ICS_ULONG timestamp)
{
  ICS_assert(ics_shm_state);
  ICS_assert(ics_shm_state->shm);

  /* Update our own watchdog timestamp in the SHM segment */
  ics_shm_state->shm->watchdogTs = timestamp;

  /* Write new value to memory */
  _ICS_OS_CACHE_PURGE(&ics_shm_state->shm->watchdogTs, 
		      ics_shm_state->paddr + offsetof(ics_shm_seg_t, watchdogTs),
		      sizeof(ics_shm_state->shm->watchdogTs));
}

/*
 * Return the last watchdog timestamp for a given cpu
 */
ICS_UINT ics_shm_watchdog_query (ICS_UINT cpuNum)
{
  ics_shm_cpu_t *cpu;

  ICS_assert(ics_shm_state);
  
  cpu = &ics_shm_state->cpu[cpuNum];

  ICS_assert(cpu->shm);

  /* About to read from SHM, so purge first */
  _ICS_OS_CACHE_PURGE(&cpu->shm->watchdogTs,
		      cpu->paddr + offsetof(ics_shm_seg_t, watchdogTs),
		      sizeof(cpu->shm->watchdogTs));

  return cpu->shm->watchdogTs;
}

#ifdef __arm__
/*  clear remote watchog  */
/*  function is called after remote cpu has been un-clocked */
void ics_shm_watchdog_clear(ICS_UINT cpuNum)
{
  ics_shm_cpu_t *cpu;

  ICS_assert(ics_shm_state);
  
  cpu = &ics_shm_state->cpu[cpuNum];

  ICS_assert(cpu->shm);
	cpu->shm->watchdogTs = 0;
	_ICS_OS_CACHE_PURGE(&cpu->shm->watchdogTs,
		      cpu->paddr + offsetof(ics_shm_seg_t, watchdogTs),
		      sizeof(cpu->shm->watchdogTs));
}
#endif 
/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

/* vim:ts=2:sw=2: */

