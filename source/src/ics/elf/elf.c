/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* Define this so we can find the ".boot" section */
/* #define SECTION_DRIVEN */

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#include "elf_types.h"


#define FOREACH_PHDR(elfh) { \
  const Elf32_Ehdr *ELFH = (elfh); \
  int PHNUM = Elf32Half(ELFH->e_phnum);	\
  int PHOFF = Elf32Off(ELFH->e_phoff);	\
  int PHENTSIZE = Elf32Half(ELFH->e_phentsize);	\
  int NUM; \
  for (NUM = 0; NUM < PHNUM; NUM++) { \
    const Elf32_Phdr *PHDR = (const Elf32_Phdr *)((const char *)ELFH+PHOFF+NUM*PHENTSIZE); \
    {

#define ENDEACH_PHDR \
    } \
  } \
}

#define FOREACH_SHDR(elfh) { \
  const Elf32_Ehdr *ELFH = (elfh); \
  int SHNUM = Elf32Half(ELFH->e_shnum);		\
  int SHOFF = Elf32Off(ELFH->e_shoff);		\
  int SHENTSIZE = Elf32Half(ELFH->e_shentsize);	\
  int NUM; \
  for (NUM = 0; NUM < SHNUM; NUM++) { \
    const Elf32_Shdr *SHDR = (const Elf32_Shdr *)((const char *)ELFH+SHOFF+NUM*SHENTSIZE); \
    {

#define ENDEACH_SHDR \
    } \
  } \
}

#define FOREACH_FILE_PHDR(file,elfh) { \
  const Elf32_Ehdr *ELFH = (elfh); \
  int PHNUM = Elf32Half(ELFH->e_phnum);	\
  int PHOFF = Elf32Off(ELFH->e_phoff);	\
  int PHENTSIZE = Elf32Half(ELFH->e_phentsize);	\
  int NUM;  \
  ICS_SIZE   sz;      \
  Elf32_Phdr *PHDR_SPACE = _ICS_OS_MALLOC((PHNUM * PHENTSIZE));    \
  _ICS_OS_FSEEK((file),PHOFF); \
  sz  = (ICS_SIZE)(_ICS_OS_FREAD((ICS_CHAR *)PHDR_SPACE,(file),(PHNUM *PHENTSIZE))); \
  if (sz != (PHNUM * PHENTSIZE))  { \
    ICS_EPRINTF(ICS_DBG_LOAD, "Failed to read PHDR of size:'%d' read:'%d'\n", (PHNUM *PHENTSIZE),sz);  \
    return ICS_SYSTEM_ERROR;  \
  }  \
  for (NUM = 0; NUM < PHNUM; NUM++) { \
    Elf32_Phdr *PHDR = PHDR_SPACE + NUM;   


#define ENDEACH_FILE_PHDR \
  } \
  _ICS_OS_FREE(PHDR_SPACE); \
}

#define FOREACH_FILE_SHDR(file,elfh) { \
  const Elf32_Ehdr *ELFH = (elfh); \
  int SHNUM = Elf32Half(ELFH->e_shnum);		\
  int SHOFF = Elf32Off(ELFH->e_shoff);		\
  int SHENTSIZE = Elf32Half(ELFH->e_shentsize);	\
  int NUM; \
  ICS_SIZE   sz;      \
  Elf32_Shdr *SHDR_SPACE = _ICS_OS_MALLOC((SHNUM * SHENTSIZE));    \
  _ICS_OS_FSEEK((file),SHOFF); \
  sz  = (ICS_SIZE)(_ICS_OS_FREAD((ICS_CHAR *)SHDR_SPACE,(file),(SHNUM *SHENTSIZE))); \
  if (sz != (SHNUM * SHENTSIZE))  { \
    ICS_EPRINTF(ICS_DBG_LOAD, "Failed to read SHDR if size:'%d' read:'%d'\n",(SHNUM *SHENTSIZE),sz);  \
    return ICS_SYSTEM_ERROR;  \
  }  \
  for (NUM = 0; NUM < SHNUM; NUM++) { \
    Elf32_Shdr *SHDR = SHDR_SPACE + NUM;  \
     

#define ENDEACH_FILE_SHDR \
  } \
  _ICS_OS_FREE(SHDR_SPACE); \
}


#define IMAGE_LOAD_CHUNK_SIZE (8 * 1024)

#ifdef ICS_DEBUG
/*
 * Dump out the Elf header 
 */
static void 
dumpElfHdr (Elf32_Ehdr *ehdr)
{
  ICS_PRINTF(ICS_DBG_LOAD, "Elf header %p\n", ehdr);
  ICS_PRINTF(ICS_DBG_LOAD, "  Class:\t%s (%d)\n",
	     (ehdr->e_ident[EI_CLASS] == ELFCLASS32) ? "ELF32" : "Not supported",
	     ehdr->e_ident[EI_CLASS]);
  
  ICS_PRINTF(ICS_DBG_LOAD, "  Data: \t%s (%d)\n",
	     (ehdr->e_ident[EI_DATA] == ELFDATA2LSB) ? "Little Endian" : "Not supported",
	     ehdr->e_ident[EI_DATA]);

  ICS_PRINTF(ICS_DBG_LOAD, "  Machine:\t%s (%d)\n",
	     ((Elf32Half(ehdr->e_machine) == EM_SH) ? "ST/SuperH SH" :
	      ((Elf32Half(ehdr->e_machine) == EM_ST200) ? "ST ST200" : "Not supported")),
	     ehdr->e_machine);
  
  ICS_PRINTF(ICS_DBG_LOAD, "  Type: \t%s (%d)\n",
	     (Elf32Word(ehdr->e_type) == ET_EXEC) ? "Executable" : "Not supported",
	     Elf32Word(ehdr->e_type));
  
  ICS_PRINTF(ICS_DBG_LOAD, "  Entry point:\t0x%x\n",
	     Elf32Addr(ehdr->e_entry));

  ICS_PRINTF(ICS_DBG_LOAD, "  Flags:\t0x%x\n",
	     Elf32Word(ehdr->e_flags));
  
  ICS_PRINTF(ICS_DBG_LOAD, "  Num phdrs:\t%d Num shdrs:\t%d\n",
	     Elf32Half(ehdr->e_phnum), Elf32Half(ehdr->e_shnum));
}
#endif /* DEBUG */

ICS_ERROR ics_elf_check_magic (ICS_CHAR *hdr)
{
  if (hdr[EI_MAG0] != ELFMAG0 ||
      hdr[EI_MAG1] != ELFMAG1 ||
      hdr[EI_MAG2] != ELFMAG2 ||
      hdr[EI_MAG3] != ELFMAG3) {
    return ICS_INVALID_ARGUMENT;
  }

  return ICS_SUCCESS;
}

/* Validate some of the Elf header values */
ICS_ERROR ics_elf_check_hdr (ICS_CHAR *image)
{
  Elf32_Ehdr *ehdr;

  ehdr = (Elf32_Ehdr *) image;

  if (ics_elf_check_magic(image) != ICS_SUCCESS)
  { goto error_magic; }

#ifdef ICS_DEBUG
  dumpElfHdr(ehdr);
#endif

  /* 32-bit binaries only */
  if (ehdr->e_ident[EI_CLASS] != ELFCLASS32)
  { goto error_class; }

  /* Check we have a little endian file */
  if (ehdr->e_ident[EI_DATA] != ELFDATA2LSB)
  { goto error_endian; }

  /* Check ELF version */
  if (ehdr->e_ident[EI_VERSION] != EV_CURRENT)
  { goto error_version; }

  /* Check OS/ABI */
  if (ehdr->e_ident[EI_OSABI] != ELFOSABI_SYSV)
  { goto error_osabi; }

  /* Check for support machine types */
  if ((Elf32Half(ehdr->e_machine) != EM_SH) && (Elf32Half(ehdr->e_machine) != EM_ST200))
  { goto error_machine; }

  /* We only support exectutables files */
  if (Elf32Half(ehdr->e_type) != ET_EXEC)
  { goto error_type; }

  return ICS_SUCCESS;

error_magic:
error_class:
error_endian:
error_machine:
error_type:
error_version:
error_osabi:
  
  return ICS_INVALID_ARGUMENT;
}

ICS_OFFSET ics_elf_entry (ICS_CHAR *image)
{
  Elf32_Ehdr *ehdr;

  ehdr = (Elf32_Ehdr *) image;

  return Elf32Addr(ehdr->e_entry);
}


/* Lookup a string from the section header string table ".shstrtab" */
ICS_SIZE ics_elffile_shstr_size (_ICS_OS_FHANDLE file,Elf32_Ehdr *ehdr)
{
  int        shstrndx    = Elf32Half(ehdr->e_shstrndx);		/* shstrtab section number */
  Elf32_Off  shoff       = Elf32Off(ehdr->e_shoff);		/* section table offset */
  Elf32_Word shentsize   = Elf32Half(ehdr->e_shentsize);	/* size of each shdr entry */

  const Elf32_Shdr shstrsec;

  /* Calculate the base of the shstrtab section header */
  _ICS_OS_FSEEK((file),(shoff + (shstrndx * shentsize))); 
  _ICS_OS_FREAD((ICS_CHAR *)&shstrsec,(file),shentsize); 

  /* Return the requested string from within the table */
  return shstrsec.sh_size;
}

/* Load a string table from the section header string table ".shstrtab" */
ICS_ERROR ics_elffile_shstr_load (_ICS_OS_FHANDLE file,Elf32_Ehdr *ehdr,const char * shdrstr)
{
  int        shstrndx    = Elf32Half(ehdr->e_shstrndx);		/* shstrtab section number */
  Elf32_Off  shoff       = Elf32Off(ehdr->e_shoff);		/* section table offset */
  Elf32_Word shentsize   = Elf32Half(ehdr->e_shentsize);	/* size of each shdr entry */
  ICS_SIZE   sz;

  const Elf32_Shdr shstrsec;

  /* Calculate the base of the shstrtab section header */
  _ICS_OS_FSEEK((file),(shoff + (shstrndx * shentsize))); 
  sz = (ICS_SIZE)(_ICS_OS_FREAD((ICS_CHAR *)&shstrsec,(file),shentsize)); 
  if (sz != shentsize)  { 
    ICS_EPRINTF(ICS_DBG_LOAD, "Failed to read file of size:'%d' read:'%d'\n",shentsize,sz); 
    return ICS_SYSTEM_ERROR;  
  }  

  /* Copy the base of the ststrtab string table */
  _ICS_OS_FSEEK((file),Elf32Off(shstrsec.sh_offset)); 
  sz = (ICS_SIZE)(_ICS_OS_FREAD((ICS_CHAR *)shdrstr,(file),shstrsec.sh_size)); 
  if (sz != shstrsec.sh_size)  { 
    ICS_EPRINTF(ICS_DBG_LOAD, "Failed to read file of size:'%d' read:'%d'\n",shstrsec.sh_size,sz); 
    return ICS_SYSTEM_ERROR;  
  }  
 
  return ICS_SUCCESS;  

}


/* Lookup a string from the section header string table ".shstrtab" */
const char *ics_elffile_shstr( Elf32_Word off,const char * shdrstr)
{
  /* Return the requested string from within the table */
  return shdrstr + off;

}

/* Lookup a string from the section header string table ".shstrtab" */
const char *ics_elf_shstr (Elf32_Ehdr *ehdr, Elf32_Word off)
{
  int        shstrndx    = Elf32Half(ehdr->e_shstrndx);		/* shstrtab section number */
  Elf32_Off  shoff       = Elf32Off(ehdr->e_shoff);		/* section table offset */
  Elf32_Word shentsize   = Elf32Half(ehdr->e_shentsize);	/* size of each shdr entry */

  const Elf32_Shdr *shstrsec;
  const char       *shstrtab;

  /* Calculate the base of the shstrtab section header */
  shstrsec        = (const Elf32_Shdr *)((const char *)ehdr + shoff + (shstrndx * shentsize));

  /* Calculate the base of the ststrtab string table */
  shstrtab        = (const char *)ehdr + Elf32Off(shstrsec->sh_offset);
  
  /* Return the requested string from within the table */
  return shstrtab + off;
}

ICS_ERROR ics_elfimage_load_size (ICS_CHAR *image, ICS_OFFSET *basep, ICS_SIZE *sizep, ICS_SIZE *alignp, ICS_OFFSET *bootp)
{
  Elf32_Ehdr *ehdr;
  ICS_OFFSET  baseAddr = -1, topAddr = 0, bootAddr = -1;
  
  ICS_SIZE    align = 0;

  ehdr = (Elf32_Ehdr *) image;

#ifdef SECTION_DRIVEN

  FOREACH_SHDR(ehdr) {
    Elf32_Addr sh_addr  = Elf32Addr(SHDR->sh_addr);
    Elf32_Word sh_size  = Elf32Word(SHDR->sh_size);
    Elf32_Word sh_align = Elf32Word(SHDR->sh_addralign);

    Elf32_Word sh_addr_align;

    if (!(Elf32Word(SHDR->sh_flags) & SHF_ALLOC) || Elf32Word(sh_size) == 0)
      /* Only consider non-zero SHF_ALLOC sections */
    { continue; }
    
    /* Find the lowest (aligned) section address */
    sh_addr_align = sh_align ? sh_addr & ~(sh_align-1) : sh_addr;
    
    if (sh_addr_align < baseAddr)
    { baseAddr = sh_addr_align; }
    
    /* Calculate the highest section end address */
    if (sh_addr + sh_size > topAddr)
    { topAddr = sh_addr + sh_size; }
    
    /* Find the largest alignment constraint */
    if (sh_align > align)
    { align = sh_align; }

    /* Hack to support older toolkits where the ELF start
     * address is invalid and instead we need to jump to the
     * ".boot" section start address
     */
    if (strcmp(".boot", ics_elf_shstr(ehdr, Elf32Off(SHDR->sh_name))) == 0)
	{ bootAddr = sh_addr; }

  } ENDEACH_SHDR;
  
#else

  FOREACH_PHDR(ehdr) {
    Elf32_Addr ph_paddr = Elf32Addr(PHDR->p_paddr);
    Elf32_Word ph_memsz = Elf32Word(PHDR->p_memsz);
    Elf32_Word ph_align = Elf32Word(PHDR->p_align); 
    
    if (Elf32Word(PHDR->p_type) != PT_LOAD || ph_memsz == 0)
      /* Only consider non-zero sized PT_LOAD segments */
    { continue; }

    /* Find the lowest section address */
    if (ph_paddr < baseAddr)
    { baseAddr = ph_paddr; }
    
    /* Calculate the highest section end address */
    if (ph_paddr + ph_memsz > topAddr)
    { topAddr = ph_paddr + ph_memsz; }
    
    /* Find the largest alignment constraint */
    if (ph_align > align)
    { align = ph_align; }
    
  } ENDEACH_PHDR;

#endif /* SECTION_DRIVEN */

  ICS_assert(baseAddr != -1);
  ICS_assert(topAddr > baseAddr);

  *sizep  = topAddr - baseAddr;
  *alignp = align;

  /* On 29-bit sh4 we need to convert P1 addresses
   * to be true physical ones
   */
#if defined(__sh__) && defined(SECTION_DRIVEN)
  if (Elf32Half(ehdr->e_machine) == EM_SH)
  {
    ICS_ERROR     err;
    ICS_MEM_FLAGS mflags;
    ICS_OFFSET    paddr;
    
    err = _ICS_OS_VIRT2PHYS((void *)baseAddr, &paddr, &mflags);
    if (err != ICS_SUCCESS)
    {
      ICS_EPRINTF(ICS_DBG_LOAD, "Failed to convert addr 0x%lx to physical\n",
		  baseAddr);
      return err;
    }

    baseAddr = paddr;
  }
#endif /* __sh__   && SECTION_DRIVEN */

  *basep  = baseAddr;

  /* Return bootAddr from ".boot" section (may be -1) */
  *bootp  = bootAddr;

  ICS_PRINTF(ICS_DBG_LOAD, "image %p : base 0x%lx size %d align %d boot 0x%lx\n",
	     image, *basep, *sizep, *alignp, *bootp);

  return ICS_SUCCESS;
}

ICS_ERROR ics_elfimage_load_image (ICS_CHAR *image, ICS_VOID *mem, ICS_SIZE memSize,
			      ICS_OFFSET baseAddr)
{
  Elf32_Ehdr *ehdr;

  ICS_PRINTF(ICS_DBG_LOAD, "image %p mem %p memSize %d baseAddr 0x%lx\n",
	     image, mem, memSize, baseAddr);
  
  if (image == NULL || mem == NULL || memSize == 0)
  { return ICS_INVALID_ARGUMENT; }
  
  ehdr = (Elf32_Ehdr *) image;

  /*
   * Drive the load from the program header table
   */
  FOREACH_PHDR(ehdr) {
    Elf32_Addr ph_paddr  = Elf32Addr(PHDR->p_paddr);
    Elf32_Addr ph_offset = Elf32Addr(PHDR->p_offset);
    Elf32_Word ph_memsz  = Elf32Word(PHDR->p_memsz);
    Elf32_Word ph_filesz = Elf32Word(PHDR->p_filesz); 
    
    ICS_OFFSET offset;

    if (Elf32Word(PHDR->p_type) != PT_LOAD || ph_memsz == 0)
      /* Only consider non-zero sized PT_LOAD segments */
    { continue; }

    /* On 29-bit sh4 we need to convert P1 addresses
     * to true physical ones
     */
#if defined(__sh__) && defined(SECTION_DRIVEN)
    if (Elf32Half(ehdr->e_machine) == EM_SH)
    {
      ICS_ERROR     err;
      ICS_MEM_FLAGS mflags;
      ICS_OFFSET    paddr;
      
      err = _ICS_OS_VIRT2PHYS((void *)ph_paddr, &paddr, &mflags);
      if (err != ICS_SUCCESS)
      {
	ICS_EPRINTF(ICS_DBG_LOAD, "Failed to convert addr 0x%lx to physical\n",
		    ph_paddr);
	return err;
      }
      
      ph_paddr = paddr;
    }
#endif /* __sh__   && SECTION_DRIVEN */

    /* Now generate the memory load offset */
    offset = ph_paddr - baseAddr;

    ICS_PRINTF(ICS_DBG_LOAD, "Code: off 0x%lx ph_offset 0x%x filesz %d\n", offset, ph_offset, ph_filesz);
    
    ICS_assert(offset < memSize);

    /* Copy the code from the image to the memory region */
    Elf32Copy((void *)((ICS_OFFSET)mem + offset), (void *)((ICS_OFFSET)image + ph_offset), ph_filesz);

#ifdef ZERO_BSS
    /* XXXX Do we need to do this or does crt0 do it for us ? */
    /* Zero any BSS regions */
    if (ph_filesz < ph_memsz)
    {
      Elf32_Word bss = ph_memsz - ph_filesz;

      /* Skip over the code bit */
      offset += ph_filesz;

      ICS_assert(offset+bss <= memSize);
      
      ICS_PRINTF(ICS_DBG_LOAD, "BSS section to %p-%p\n", mem + offset, mem + offset + bss);
      _ICS_OS_MEMSET((void *)((ICS_OFFSET)mem + offset), 0, bss);
    }
#endif
    
  }  ENDEACH_PHDR;

  return ICS_SUCCESS;
}

ICS_ERROR ics_elffile_load_size (_ICS_OS_FHANDLE file,ICS_CHAR *image, ICS_OFFSET *basep, ICS_SIZE *sizep, ICS_SIZE *alignp, ICS_OFFSET *bootp)
{
  Elf32_Ehdr *ehdr;
  ICS_OFFSET  baseAddr = -1, topAddr = 0, bootAddr = -1;
  ICS_SIZE    align = 0;

#ifdef SECTION_DRIVEN
  ICS_SIZE    strSize = 0;
  ICS_CHAR    *shdrStr;
#endif /* SECTION_DRIVEN */

  ehdr = (Elf32_Ehdr *) image;

#ifdef SECTION_DRIVEN
  
  /* Load the string table from the section header string table ".shstrtab" */ 
  strSize = ics_elffile_shstr_size(file,ehdr);
  shdrStr = _ICS_OS_MALLOC(strSize);
  if (shdrStr == NULL)
  {
    ICS_EPRINTF(ICS_DBG_LOAD,"Failed to allocate string table memory %d bytes\n",strSize);
    return ICS_ENOMEM;
  }

  if (ICS_SUCCESS != ics_elffile_shstr_load(file,ehdr,shdrStr))
  {
    _ICS_OS_FREE(shdrStr);
    ICS_EPRINTF(ICS_DBG_LOAD,"Failed to load string table to memory \n");
    return ICS_SYSTEM_ERROR;  
  }
  
  /* Now determine the base physical address and memory size to load this image file */
  FOREACH_FILE_SHDR(file,ehdr) {
    Elf32_Addr sh_addr  = Elf32Addr(SHDR->sh_addr);
    Elf32_Word sh_size  = Elf32Word(SHDR->sh_size);
    Elf32_Word sh_align = Elf32Word(SHDR->sh_addralign);

    Elf32_Word sh_addr_align;

    if (!(Elf32Word(SHDR->sh_flags) & SHF_ALLOC) || Elf32Word(sh_size) == 0)
      /* Only consider non-zero SHF_ALLOC sections */
    { continue; }
    
    /* Find the lowest (aligned) section address */
    sh_addr_align = sh_align ? sh_addr & ~(sh_align-1) : sh_addr;
    
    if (sh_addr_align < baseAddr)
    { baseAddr = sh_addr_align; }
    
    /* Calculate the highest section end address */
    if (sh_addr + sh_size > topAddr)
    { topAddr = sh_addr + sh_size; }
    
    /* Find the largest alignment constraint */
    if (sh_align > align)
    { align = sh_align; }

    /* Hack to support older toolkits where the ELF start
     * address is invalid and instead we need to jump to the
     * ".boot" section start address
     */
    ICS_PRINTF(ICS_DBG_LOAD, "Looking into details of section[%d] :'%s' '\n",NUM,ics_elffile_shstr(Elf32Off(SHDR->sh_name),shdrStr)); 

    if (strcmp(".boot", ics_elffile_shstr(Elf32Off(SHDR->sh_name),shdrStr)) == 0)
    { bootAddr = sh_addr; }

  } ENDEACH_FILE_SHDR;
  
  _ICS_OS_FREE(shdrStr);
  
#else

  FOREACH_FILE_PHDR(file,ehdr) {
    Elf32_Addr ph_paddr = Elf32Addr(PHDR->p_paddr);
    Elf32_Word ph_memsz = Elf32Word(PHDR->p_memsz);
    Elf32_Word ph_align = Elf32Word(PHDR->p_align); 
    
    if (Elf32Word(PHDR->p_type) != PT_LOAD || ph_memsz == 0)
      /* Only consider non-zero sized PT_LOAD segments */
    { continue; }

    /* Find the lowest section address */
    if (ph_paddr < baseAddr)
    { baseAddr = ph_paddr; }
    
    /* Calculate the highest section end address */
    if (ph_paddr + ph_memsz > topAddr)
    { topAddr = ph_paddr + ph_memsz; }
    
    /* Find the largest alignment constraint */
    if (ph_align > align)
    { align = ph_align; }
    
  } ENDEACH_FILE_PHDR;

#endif /* SECTION_DRIVEN */

  ICS_assert(baseAddr != -1);
  ICS_assert(topAddr > baseAddr);

  *sizep  = topAddr - baseAddr;
  *alignp = align;

  /* On 29-bit sh4 we need to convert P1 addresses
   * to be true physical ones
   */
#if defined(__sh__) && defined(SECTION_DRIVEN)
  if (Elf32Half(ehdr->e_machine) == EM_SH)
  {
    ICS_ERROR     err;
    ICS_MEM_FLAGS mflags;
    ICS_OFFSET    paddr;
    
    err = _ICS_OS_VIRT2PHYS((void *)baseAddr, &paddr, &mflags);
    if (err != ICS_SUCCESS)
    {
      ICS_EPRINTF(ICS_DBG_LOAD, "Failed to convert addr 0x%lx to physical\n",
		  baseAddr);
      return err;
    }

    baseAddr = paddr;
  }
#endif /* __sh__   && SECTION_DRIVEN */

  *basep  = baseAddr;

  /* Return bootAddr from ".boot" section (may be -1) */
  *bootp  = bootAddr;

  ICS_PRINTF(ICS_DBG_LOAD, "image %p : base 0x%lx size %d align %d boot 0x%lx\n",
	     image, *basep, *sizep, *alignp, *bootp);

  return ICS_SUCCESS;
}

ICS_ERROR ics_elffile_load_image (_ICS_OS_FHANDLE file,ICS_CHAR *image, ICS_VOID *mem, ICS_SIZE memSize,
			      ICS_OFFSET baseAddr)
{
  Elf32_Ehdr *ehdr;
  ICS_CHAR *loadImage;
  ICS_INT i;
  ICS_SIZE fileSize;
  ICS_SIZE loadSize;

  ICS_PRINTF(ICS_DBG_LOAD, "image %p mem %p memSize %d baseAddr 0x%lx\n",
	     image, mem, memSize, baseAddr);
  
  if (image == NULL || mem == NULL || memSize == 0)
    return ICS_INVALID_ARGUMENT;
  
  /* Alloc image load buffer */ 
  loadImage = _ICS_OS_MALLOC (IMAGE_LOAD_CHUNK_SIZE);
  if (loadImage == NULL)
  {
    ICS_EPRINTF(ICS_DBG_LOAD,"Failed to allocate image memory %d bytes\n",IMAGE_LOAD_CHUNK_SIZE);
    return ICS_ENOMEM;
  }

  ehdr = (Elf32_Ehdr *) image;

  /*
   * Drive the load from the program header table
   */
  FOREACH_FILE_PHDR(file,ehdr) {
    Elf32_Addr ph_paddr  = Elf32Addr(PHDR->p_paddr);
    Elf32_Addr ph_offset = Elf32Addr(PHDR->p_offset);
    Elf32_Word ph_memsz  = Elf32Word(PHDR->p_memsz);
    Elf32_Word ph_filesz = Elf32Word(PHDR->p_filesz); 
    
    ICS_OFFSET offset;

    if (Elf32Word(PHDR->p_type) != PT_LOAD || ph_memsz == 0)
      /* Only consider non-zero sized PT_LOAD segments */
      continue;

    /* On 29-bit sh4 we need to convert P1 addresses
     * to true physical ones
     */
#if defined(__sh__) && defined(SECTION_DRIVEN)
    if (Elf32Half(ehdr->e_machine) == EM_SH)
    {
      ICS_ERROR     err;
      ICS_MEM_FLAGS mflags;
      ICS_OFFSET    paddr;
      
      err = _ICS_OS_VIRT2PHYS((void *)ph_paddr, &paddr, &mflags);
      if (err != ICS_SUCCESS)
      {
	ICS_EPRINTF(ICS_DBG_LOAD, "Failed to convert addr 0x%lx to physical\n",
		    ph_paddr);
	return err;
      }
      
      ph_paddr = paddr;
    }
#endif /* __sh__   && SECTION_DRIVEN */

    /* Now generate the memory load offset */
    offset = ph_paddr - baseAddr;

    ICS_PRINTF(ICS_DBG_LOAD, "Code: off 0x%lx ph_offset 0x%x filesz %d\n", offset, ph_offset, ph_filesz);
    
    ICS_assert(offset < memSize);
  
    /* Set the file pointer to ph_offset */   
    _ICS_OS_FSEEK((file),ph_offset);
   
    for(i=0,fileSize = ph_filesz,loadSize =0; (0 != fileSize); i++) 
    {
      if(IMAGE_LOAD_CHUNK_SIZE < fileSize) 
        loadSize = IMAGE_LOAD_CHUNK_SIZE;
      else
        loadSize = fileSize;
      
      /* Update the remaining PH size*/
      fileSize -= loadSize;

      /* Set the file pointer to correct offset */   
      _ICS_OS_FSEEK((file),(ph_offset + (i * IMAGE_LOAD_CHUNK_SIZE)));

      /* load the image to buffer */ 
      _ICS_OS_FREAD((ICS_CHAR *)loadImage,(file),loadSize); 

      /* Copy the code from the loadimage to the memory region */
      Elf32Copy((void *)((ICS_OFFSET)mem + (offset + i * IMAGE_LOAD_CHUNK_SIZE)),(void *)loadImage,loadSize);

      ICS_PRINTF(ICS_DBG_LOAD, "Loaded %d of ph_filesz=%d\n", ((i * IMAGE_LOAD_CHUNK_SIZE) + loadSize),ph_filesz );
    }

#ifdef ZERO_BSS
    /* XXXX Do we need to do this or does crt0 do it for us ? */
    /* Zero any BSS regions */
    if (ph_filesz < ph_memsz)
    {
      Elf32_Word bss = ph_memsz - ph_filesz;

      /* Skip over the code bit */
      offset += ph_filesz;

      ICS_assert(offset+bss <= memSize);
      
      ICS_PRINTF(ICS_DBG_LOAD, "BSS section to %p-%p\n", mem + offset, mem + offset + bss);
      _ICS_OS_MEMSET((void *)((ICS_OFFSET)mem + offset), 0, bss);
    }
#endif
    
  }  ENDEACH_FILE_PHDR;
  
  /* Free image load buffer */ 
  _ICS_OS_FREE (loadImage);

  return ICS_SUCCESS;
}

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

