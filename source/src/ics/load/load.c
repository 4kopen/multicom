/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

/*
 * Host side code for loading Elf firmware
 */

#define ELF_HEADER (128)   /* This is not the accurate size of Elf32_Ehdr, however much bigger than Elf32_Ehdr.  */

#ifdef __arm__
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
#define __MULTICOM_RPROC__
#endif
#endif

#ifdef __MULTICOM_RPROC__
/*
 * These API are stubbed, the elf is loaded by remoteproc
 */
ICS_ERROR ics_load_elf_file (const ICS_CHAR *fname, ICS_UINT flags, ICS_OFFSET *entryAddrp)
{
	printk("remoteproc load elf file not supported\n");
	return ICS_INVALID_ARGUMENT;
}
ICS_ERROR ics_load_elf_image (ICS_CHAR *image, ICS_UINT flags, ICS_OFFSET *entryAddrp)
{
	printk("remoteproc load elf not supported\n");
	return ICS_INVALID_ARGUMENT;
}

#else /* #ifdef __MULTICOM_RPROC__ */

/*
 * Take an Elf image in memory and unpack it according to the Elf header info
 */
static
ICS_ERROR loadElfFile ( _ICS_OS_FHANDLE file,const ICS_CHAR *fname,ICS_UINT flags, ICS_OFFSET *entryAddrp)
{
  ICS_ERROR  err;

  ICS_OFFSET baseAddr, bootAddr, entryAddr;
  ICS_SIZE   loadSize, loadAlign;
  ICS_VOID  *loadBase = NULL;
  ICS_CHAR  *image = NULL;
  ICS_SIZE        sz;
  ICS_SIZE        fsize;    

  /* Determine the total file size */
  _ICS_OS_FSIZE(file, &fsize);

  /* Allocate memory to hold the Elf file image */
  image = _ICS_OS_MALLOC(ELF_HEADER);
  if (image == NULL)
  {
    ICS_EPRINTF(ICS_DBG_LOAD,
		"Failed to allocate image memory %d bytes\n",ELF_HEADER);
    err = ICS_ENOMEM;
    goto error;
  }

  ICS_PRINTF(ICS_DBG_LOAD, "Loading fh 0x%x size %d into %p\n", (int) file, fsize, image);
  
  sz = _ICS_OS_FREAD(image,file,ELF_HEADER);
  if (sz != ELF_HEADER)
  {
    ICS_EPRINTF(ICS_DBG_LOAD,
		"Failed to read file '%s' : %d\n", fname, sz);
    err = ICS_SYSTEM_ERROR;
    goto error_free;
  }
  /* Check the validity of the Elf image */
  if ((err = ics_elf_check_magic(image)) != ICS_SUCCESS)
  {
    goto error_free;
  }

  /* Is the Elf header valid and compatible? */
  if ((err = ics_elf_check_hdr(image)) != ICS_SUCCESS)
  {
    goto error_free;
  }

  /* Determine the base physical address and memory size to load this image file */
  if ((err = ics_elffile_load_size(file,image, &baseAddr, &loadSize, &loadAlign, &bootAddr)) != ICS_SUCCESS)
  {
    goto error_free;
  }

  /* Map memory (cached) at the correct address for loading the image */
  loadBase = _ICS_OS_MMAP(baseAddr, loadSize, ICS_TRUE);
  if (loadBase == NULL)
  {
    ICS_EPRINTF(ICS_DBG_LOAD, "OS_MMAP(0x%lx, %d) failed\n",
		baseAddr, loadSize);
    err = ICS_SYSTEM_ERROR;
    goto error_free;
  }

  /* Purge any old cache entries for this newly mapped memory */
  _ICS_OS_CACHE_PURGE(loadBase, baseAddr, loadSize);

  /* Load the Elf image into the target memory */
  if ((err = ics_elffile_load_image(file,image, loadBase, loadSize, baseAddr)) != ICS_SUCCESS)
  {
    goto error_unmap;
  }

  /* Purge the cache of the loaded code image */
  _ICS_OS_CACHE_PURGE(loadBase, baseAddr, loadSize);

  /* Unmap the target memory */
  _ICS_OS_MUNMAP(loadBase);

  /* Grab the Elf entry address */
  if (bootAddr != -1)
    /* Hack to support older compilers which have an invalid ELF entry address */
    entryAddr = bootAddr;
  else
    entryAddr = ics_elf_entry(image);

  ICS_assert(entryAddr);

  ICS_PRINTF(ICS_DBG_LOAD, "loadBase %p loadSize %d baseAddr 0x%lx entryAddr 0x%lx\n",
	     loadBase, loadSize, baseAddr, entryAddr);

  /* Pass back the entry address to caller */
  *entryAddrp = entryAddr;

  /*  Release elf image header space */  
  _ICS_OS_FREE(image);

  return ICS_SUCCESS;

error_unmap:
  _ICS_OS_MUNMAP(loadBase);

error_free:
  /*  Release elf image header space */  
  _ICS_OS_FREE(image);

error:
  ICS_EPRINTF(ICS_DBG_LOAD, "Failed : %s(%d)\n",
	      ics_err_str(err), err);

  return err;
}

/*
 * Take an Elf image in memory and unpack it according to the Elf header info
 */
static
ICS_ERROR loadElfImage (ICS_CHAR *image, ICS_UINT flags, ICS_OFFSET *entryAddrp)
{
  ICS_ERROR  err;

  ICS_OFFSET baseAddr, bootAddr, entryAddr;
  ICS_SIZE   loadSize, loadAlign;
  ICS_VOID  *loadBase = NULL;

  ICS_assert(image);

  /* Check the validity of the Elf image */
  if ((err = ics_elf_check_magic(image)) != ICS_SUCCESS)
  {
    goto error;
  }

  /* Is the Elf header valid and compatible? */
  if ((err = ics_elf_check_hdr(image)) != ICS_SUCCESS)
  {
    goto error;
  }

  /* Determine the base physical address and memory size to load this image file */
  if ((err = ics_elfimage_load_size(image, &baseAddr, &loadSize, &loadAlign, &bootAddr)) != ICS_SUCCESS)
  {
    goto error;
  }

  /* Map memory (cached) at the correct address for loading the image */
  loadBase = _ICS_OS_MMAP(baseAddr, loadSize, ICS_TRUE);
  if (loadBase == NULL)
  {
    ICS_EPRINTF(ICS_DBG_LOAD, "OS_MMAP(0x%lx, %d) failed\n",
		baseAddr, loadSize);
    err = ICS_SYSTEM_ERROR;
    goto error;
  }

  /* Purge any old cache entries for this newly mapped memory */
  _ICS_OS_CACHE_PURGE(loadBase, baseAddr, loadSize);

  /* Load the Elf image into the target memory */
  if ((err = ics_elfimage_load_image(image, loadBase, loadSize, baseAddr)) != ICS_SUCCESS)
  {
    goto error_unmap;
  }

  /* Purge the cache of the loaded code image */
  _ICS_OS_CACHE_PURGE(loadBase, baseAddr, loadSize);

  /* Unmap the target memory */
  _ICS_OS_MUNMAP(loadBase);

  /* Grab the Elf entry address */
  if (bootAddr != -1)
    /* Hack to support older compilers which have an invalid ELF entry address */
    entryAddr = bootAddr;
  else
    entryAddr = ics_elf_entry(image);

  ICS_assert(entryAddr);

  ICS_PRINTF(ICS_DBG_LOAD, "loadBase %p loadSize %d baseAddr 0x%lx entryAddr 0x%lx\n",
	     loadBase, loadSize, baseAddr, entryAddr);

  /* Pass back the entry address to caller */
  *entryAddrp = entryAddr;
  
  return ICS_SUCCESS;

error_unmap:
  _ICS_OS_MUNMAP(loadBase);
  
error:
  ICS_EPRINTF(ICS_DBG_LOAD, "Failed : %s(%d)\n",
	      ics_err_str(err), err);

  return err;
}

/*
 * Load an Elf image from a file
 */
ICS_ERROR ics_load_elf_file (const ICS_CHAR *fname, ICS_UINT flags, ICS_OFFSET *entryAddrp)
{
  ICS_ERROR       err = ICS_INVALID_ARGUMENT;
  ICS_UINT        validFlags = 0;
  ICS_SIZE        fsize;    
  int             res;
  _ICS_OS_FHANDLE file;

  if (fname == NULL || entryAddrp == NULL || (flags & ~validFlags))
    return ICS_INVALID_ARGUMENT;

  ICS_PRINTF(ICS_DBG_LOAD, "fname '%s'\n", fname);
    
  ICS_PRINTF(ICS_DBG_LOAD, "flags 0x%x\n", flags);
  
  /* Open the file on the local OS */
  if ((res = _ICS_OS_FOPEN(fname, &file)) != 0)
  {
    ICS_EPRINTF(ICS_DBG_LOAD,
		"Failed to open file '%s' : %d\n", fname, res);

    return ICS_NAME_NOT_FOUND;
  }

  /* Determine the total file size */
  _ICS_OS_FSIZE(file, &fsize);
  if (fsize == 0)
  {
    ICS_EPRINTF(ICS_DBG_LOAD,
		"Failed to find file size of '%s'\n",
		fname);

    err = ICS_INVALID_ARGUMENT;
    goto error_close;
  }

  ICS_PRINTF(ICS_DBG_LOAD, "Filesize is %d\n", fsize);


  /* Now load the Elf image from the file to memory, returning the entry address */
  err = loadElfFile(file,fname,flags, entryAddrp);

error_close:

  _ICS_OS_FCLOSE(file);
  
  return err;
}

/*
 * Load an Elf image from a memory region
 */
ICS_ERROR ics_load_elf_image (ICS_CHAR *image, ICS_UINT flags, ICS_OFFSET *entryAddrp)
{
  ICS_UINT validFlags = 0;

  if (image == NULL)
    return ICS_INVALID_ARGUMENT;
  
  if (entryAddrp == NULL || (flags & ~validFlags))
    return ICS_INVALID_ARGUMENT;

  /* Load the Elf image from a memory image, returning the entry address */
  return loadElfImage(image, flags, entryAddrp);
}

#endif /* #ifdef __MULTICOM_RPROC__ */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

