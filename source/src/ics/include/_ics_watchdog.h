/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_WATCHDOG_SYS_H
#define _ICS_WATCHDOG_SYS_H

typedef struct ics_watchdog_callback 
{
  ICS_ULONG		 cpuMask;			/* Bitmask of watched cpus */

  ICS_WATCHDOG_CALLBACK	 callback;			/* User callback function */
  ICS_VOID		*param;				/* User's callback parameter */

  struct list_head       list;				/* Doubly linked list */
  struct list_head       tlist;				/* Doubly linked trigger list */

} ics_watchdog_callback_t;

/* Internal APIs */
void ics_watchdog_task (void *param);
void ics_watchdog_run (void);
ICS_ERROR ics_watchdog_init (void);
void ics_watchdog_term (void);

#endif /* _ICS_WATCHDOG_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

