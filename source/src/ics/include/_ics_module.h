/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

/*
 * Linux Kernel module header file
 */

#ifndef _ICS_LINUX_MODULE_H
#define _ICS_LINUX_MODULE_H

#define MODULE_NAME	"ics"

/* Procfs utilities */
extern int ics_create_procfs (ICS_ULONG cpuMask);
extern void ics_remove_procfs (ICS_ULONG cpuMask);
extern struct bpa2_part *ics_bpa2_part;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
#define SET_PROCFS_OWNER(D)	(D)->owner = THIS_MODULE
#else
#define SET_PROCFS_OWNER(D)
#endif

#endif /* _ICS_LINUX_MODULE_H */

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
