/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _HPY_STUB_H
#define _HPY_STUB_H

typedef void           			HPY_VOID;
typedef int            			HPY_INT;
typedef unsigned int   			HPY_UINT32;
typedef unsigned int   			HPY_UINT;
typedef unsigned long long __attribute__ ((aligned(8))) HPY_UINT64;
typedef void (*HPY_MAILBOX_FN)(HPY_UINT64 status);


enum 
{
  /* absolute hw mbox offsets */
  _ICS_MBOX_OFFSET		= 0x004 / 4,	/* Offset of 1st mbox register */
  _ICS_MBOX_LOCKS		= 0x200 / 4,	/* Offset of 1st lock register */
  
  /* deltas between types of register */
  _ICS_MBOX_SIZEOF		= 0x004 / 4,
  _ICS_MBOX_LOCK_SIZEOF		= 0x004 / 4,

  /* offsets of various registers from an ics_mailbox_t * */
  _ICS_MBOX_STATUS		= 0x000 / 4,
  _ICS_MBOX_STATUS_SET		= 0x020 / 4,
  _ICS_MBOX_STATUS_CLEAR  	= 0x040 / 4,
  _ICS_MBOX_ENABLE        	= 0x060 / 4,
  _ICS_MBOX_ENABLE_SET		= 0x080 / 4,
  _ICS_MBOX_ENABLE_CLEAR	= 0x0a0 / 4
};

enum 
{
  /* offsets of various registers from an ics_mailbox_t * */
  _ICS_MBOX_ST20_OFFESET		 = 0x000 / 4,
  _ICS_MBOX_ST40_OFFESET		 = 0x100 / 4,
  _ICS_MBOX_LOW32_STATUS		      = 0x000 / 4,
  _ICS_MBOX_LOW32_STATUS_SET	   	= 0x020 / 4,
  _ICS_MBOX_LOW32_STATUS_CLEAR  	= 0x040 / 4,
  _ICS_MBOX_HIGH32_STATUS   	= 0x060 / 4,
  _ICS_MBOX_HIGH32_ENABLE_SET	 	  = 0x080 / 4,
  _ICS_MBOX_HIGH32_ENABLE_CLEAR	  = 0x0a0 / 4
};

#define _hpy_assert(expr)	do { if (!(expr)) {							\
					assert(expr); }							\
				} while (0)

#if defined __os21__
#include <os21.h>
/* #include <os21/st40/sti7108.h> */
extern interrupt_name_t OS21_INTERRUPT_MAILBOX_0;
#define MBOX0_IRQ	((unsigned int) &OS21_INTERRUPT_MAILBOX_0)
#elif defined __KERNEL__
/* Based on a ILC3 base of 65 + offsets in ADCS 8183887 (Table 80) */
#define MBOX6_IRQ	(65 + 14)
#endif


/* Encode the MBOX Handle <cpu,compartment> into the 32-bit value
 * [31-24]                     ( Reserved unused)
 * [23-16] Handle Mark         ( 8-bit)
 * [15-08] CPU number          ( 8-bit)
 * [07-00] Compartment number  ( 8-bit)
 *
 */
#define _BSP_MAILBOX_HANDLE_MARK_MASK	 (0xff)
#define _BSP_MAILBOX_HANDLE_CPU_MASK	  (0xff)
#define _BSP_MAILBOX_HANDLE_PART_MASK	 (0xff)
#define _BSP_MAILBOX_HANDLE_MARK_SHIFT (16)
#define _BSP_MAILBOX_HANDLE_CPU_SHIFT	 (8)
#define _BSP_MAILBOX_HANDLE_PART_SHIFT (0)
#define _BSP_MAILBOX_HANDLE_MAGIC      (0xAA)

#define _BSP_MAILBOX_HANDLE(CPUNUM,PART)	    ((((CPUNUM) & _BSP_MAILBOX_HANDLE_CPU_MASK) << _BSP_MAILBOX_HANDLE_CPU_SHIFT) | \
                                              (((PART) & _BSP_MAILBOX_HANDLE_PART_MASK) << _BSP_MAILBOX_HANDLE_PART_SHIFT) | \
                                              ((_BSP_MAILBOX_HANDLE_MAGIC & _BSP_MAILBOX_HANDLE_MARK_MASK) << _BSP_MAILBOX_HANDLE_MARK_SHIFT))
#define _BSP_MAILBOX_HANDLE2CPU(HANDLE)	     (((HANDLE) >> _BSP_MAILBOX_HANDLE_CPU_SHIFT) & _BSP_MAILBOX_HANDLE_CPU_MASK) 
#define _BSP_MAILBOX_HANDLE2COMPART(HANDLE)	 (((HANDLE) >> _BSP_MAILBOX_HANDLE_PART_SHIFT) & _BSP_MAILBOX_HANDLE_PART_MASK) 





extern HPY_INT   hpy_mailbox_init(HPY_UINT mboxHandle,HPY_MAILBOX_FN mboxHandler);
extern HPY_UINT32 hpy_mailbox_install_handler(HPY_MAILBOX_FN mboxHandler);
extern HPY_VOID  hpy_mailbox_interrupt_enable (HPY_UINT mboxHandle, HPY_UINT bit);
extern HPY_VOID  hpy_mailbox_interrupt_disable(HPY_UINT mboxHandle,HPY_UINT32 bit);
extern HPY_VOID  hpy_mailbox_interrupt_clear (HPY_UINT mboxHandle,HPY_UINT32 bit);
extern HPY_VOID  hpy_mailbox_interrupt_raise (HPY_UINT mboxHandle,HPY_UINT32 bit);
extern HPY_UINT  hpy_mailbox_status_get  (HPY_UINT mboxHandle);
extern HPY_VOID  hpy_mailbox_status_set  (HPY_UINT mboxHandle, HPY_UINT value);
extern HPY_VOID  hpy_mailbox_status_mask (HPY_UINT mboxHandle,HPY_UINT set, HPY_UINT clear);
extern HPY_VOID  hpy_mailbox_enable_set  (HPY_UINT mboxHandle, HPY_UINT value);
extern HPY_VOID  hpy_mailbox_enable_mask (HPY_UINT mboxHandle, HPY_UINT set, HPY_UINT clear);
extern HPY_UINT  hpy_mailbox_enable_get  (HPY_UINT mboxHandle,HPY_UINT32 bit);
extern HPY_VOID* hpy_mailbox_shared_pool (void);

extern unsigned int bsp_cpu_count;
extern unsigned int bsp_vcpu_count;
extern unsigned int bsp_ics_page_size;
extern unsigned int bsp_ics_pool_size;
extern unsigned int bsp_pool_start;
extern unsigned int bsp_pool_size;




#endif /* _HPY_STUB_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
