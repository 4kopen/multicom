/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_USER_SYS_H
#define _ICS_USER_SYS_H

#define ICS_MAJOR_NUM 		0
#define ICS_DEV_NAME  		"ics"
#define ICS_DEV_COUNT 		1

/* Control desc hung off file pointer */
typedef struct ics_user
{
	_ICS_OS_MUTEX		ulock;			/* Lock to protect this structure */

} ics_user_t;

#endif /* _ICS_USER_SYS_H */

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
