/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_SYS_STATS_H
#define _ICS_SYS_STATS_H

/*
 * Internal stats state
 */
typedef struct
{ 
  ICS_STATS_HANDLE	 handle;			/* Transport handle */

  ICS_STATS_CALLBACK	 callback;			/* User callback & param */
  ICS_VOID		*param;

  struct list_head       list;				/* Doubly linked list */
  struct list_head       tlist;				/* Doubly linked trigger list */

} ics_stats_t;

typedef ICS_STATS_ITEM __attribute__ ((aligned(8))) ics_stats_item_t;

/* 
 * Internal APIs
 */
ICS_ERROR ics_stats_init (void);
void ics_stats_term (void);
void ics_stats_trigger (void);

void ics_stats_mem (ICS_STATS_HANDLE handle, void *param);
void ics_stats_mem_sys (ICS_STATS_HANDLE handle, void *param);
void ics_stats_cpu (ICS_STATS_HANDLE handle, void *param);

#endif /* _ICS_SYS_STATS_H */
 
/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
