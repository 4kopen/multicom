/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

/*
 * OS21 Wrapper macros
 */

#ifndef _ICS_OS21_SYS_H
#define _ICS_OS21_SYS_H

#include <os21.h>

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>

/* For some reason -std=c99 doesn't give us these definitions */
extern int snprintf(char *, size_t, const char *, ...);
extern int vsnprintf(char *, size_t, const char *, va_list);

#ifdef __sh__
#include <os21/st40.h>
#endif

#ifdef __st200__
#include <os21/st200.h>

#ifndef cache_flush_data
#define cache_flush_data	cache_purge_data
#endif

#endif

#define _ICS_OS_EXPORT 			extern
#define _ICS_OS_INLINE_FUNC_PREFIX 	static __inline__

#define _ICS_OS_MAX_TASK_NAME		16	/* OS specific maximum length of a task name */

_ICS_OS_EXPORT ICS_ERROR ics_os_virt2phys (ICS_VOID *vaddr, ICS_OFFSET *paddrp, ICS_MEM_FLAGS *mflagsp);
_ICS_OS_EXPORT ICS_ERROR ics_os_scu_enable (ICS_OFFSET paddr, ICS_SIZE size);
_ICS_OS_EXPORT ICS_ERROR ics_os_scu_disable (ICS_OFFSET paddr);
_ICS_OS_EXPORT void      ics_os_printf (const char *fmt, ...);

#if ( OS21_VERSION_MAJOR >= 3 ) || ( OS21_VERSION_MAJOR >= 2 && OS21_VERSION_MINOR >= 4 )
#define task_context_interrupt 		task_context_system
#endif

#if OS21_VERSION_MAJOR < 3
#include "../os21/vmemadapt.h"		/* vmem API translation layer */
#endif

#if OS21_VERSION_MAJOR < 3
#define kernel_printf			_kernel_printf
#endif

/* Returns ICS_TRUE if caller is running in interrupt context */
_ICS_OS_INLINE_FUNC_PREFIX
ICS_BOOL ics_os_in_interrupt (void)
{
  task_t* task;
  int     interruptInfo;

  return (task_context(&task, &interruptInfo) == task_context_interrupt);
}

/* Console/tty output function */
#define _ICS_OS_PRINTF(FMT, ...)	(ics_os_in_interrupt() ? (void) kernel_printf(FMT, ## __VA_ARGS__) : (void) printf(FMT, ## __VA_ARGS__))

/* 
 * Memory allocation
 *
 * Debug wrappers of these are defined in _ics_debug.h
 */
#define _ICS_OS_PAGESIZE		vmem_min_page_size()

#define _ics_os_malloc(S)		malloc((S))
#define _ics_os_zalloc(P, S)		do { if (((P) = _ics_os_malloc((S)))) _ICS_OS_MEMSET((P), 0, (S)); } while (0)
#define _ics_os_free(P)			free((P))

/*
 * Physically contiguous memory allocation
 *
 * Debug wrappers of these are defined in _ics_debug.h
 */

#if defined(__st200__)
int ics_os_contig_create(void * pAddr, unsigned int size);
int ics_contig_status(int avail);
void * ics_os_contig_alloc (ICS_SIZE size, ICS_SIZE align);
#define _ics_os_contig_alloc(S, A, F) ics_os_contig_alloc((S), (A))
void _ics_os_contig_free (ICS_VOID *ptr);
#else
#define _ics_os_contig_alloc(S, A, F)	memalign((A), (S))
#define _ics_os_contig_free(P)		free((P))
#define ics_contig_status(A) _ics_os_mem_status((A))
#endif

_ICS_OS_INLINE_FUNC_PREFIX
ICS_ULONG _ics_os_mem_status (int avail)
{
  partition_status_t status;

  /* Get statistics of the runtime/malloc heap */
  if (partition_status(NULL, &status, 0) != OS21_SUCCESS)
    return 0;

  return (avail ? status.partition_status_free : status.partition_status_used);
}

#define _ICS_OS_MEM_AVAIL()		_ics_os_mem_status(1)
#define _ICS_OS_MEM_USED()		_ics_os_mem_status(0)


/* Data movement */
#define _ICS_OS_MEMSET(P, V, S)		memset((P), (V), (S))
#define _ICS_OS_MEMCPY(D, S, N)		memcpy((D), (S), (N))

/* 
 * Cache management
 */
#define _ICS_OS_CACHE_PURGE(V, P, S) 		cache_purge_data((void *)(V), (S))
#define _ICS_OS_CACHE_FLUSH(V, P, S) 		cache_flush_data((void *)(V), (S))
#define _ICS_OS_CACHE_INVALIDATE(V, P, S) 	cache_invalidate_data((void *)(V), (S))

/*
 * Virtual address mapping
 */
#define _ICS_OS_VIRT2PHYS(V, PP, MFP)	ics_os_virt2phys((V), (PP), (MFP))
#define _ics_os_mmap(P, S, C)		vmem_create((void *)(P), (S), NULL, (C) ? VMEM_CREATE_CACHED : (VMEM_CREATE_UNCACHED|VMEM_CREATE_WRITE|VMEM_CREATE_READ))
#define _ics_os_munmap(V)		vmem_delete((V))

#ifdef __st200__
/*
 * ST200 speculation unit 
 */
#define _ICS_OS_SCU_ENABLE(A, S)	ics_os_scu_enable((A), (S))
#define _ICS_OS_SCU_DISABLE(A, S)	ics_os_scu_enable((A), (S))

#else

/* Phew no speculation unit! */
#define _ICS_OS_SCU_ENABLE(A, S)	ICS_SUCCESS
#define _ICS_OS_SCU_DISABLE(A, S)	ICS_SUCCESS

#endif

#define _ICS_OS_TASK_ADMIN_PRIO			_ICS_OS_TASK_DEFAULT_PRIO
#define _ICS_OS_TASK_NSRV_PRIO			_ICS_OS_TASK_DEFAULT_PRIO
#define _ICS_OS_TASK_STATS_PRIO			_ICS_OS_TASK_DEFAULT_PRIO
#define _ICS_OS_TASK_WATCHDOG_PRIO		_ICS_OS_TASK_DEFAULT_PRIO

/*
 * Mutual exclusion
 */
typedef semaphore_t* _ICS_OS_MUTEX;

#define _ICS_OS_MUTEX_INIT(pMutex)	((*(pMutex) = semaphore_create_fifo(1)) != NULL)
#define _ICS_OS_MUTEX_DESTROY(pMutex)	semaphore_delete(*(pMutex))
#define _ICS_OS_MUTEX_TAKE(pMutex)	semaphore_wait(*(pMutex))
#define _ICS_OS_MUTEX_RELEASE(pMutex)	do { ICS_ASSERT(_ICS_OS_MUTEX_HELD(pMutex)); \
    					     semaphore_signal(*(pMutex)); } while(0)
#define _ICS_OS_MUTEX_HELD(pMutex)	(semaphore_value(*(pMutex)) == 0)

/*
 * Blocking Events
 */

typedef semaphore_t* _ICS_OS_EVENT;

ICS_ERROR ics_os_event_wait (_ICS_OS_EVENT ev, ICS_UINT timeout);

#define _ICS_OS_EVENT_INIT(pEvent)	((*(pEvent) = semaphore_create_fifo(0)) != NULL)
#define _ICS_OS_EVENT_DESTROY(pEvent)	semaphore_delete(*(pEvent))
#define _ICS_OS_EVENT_WAIT(pEvent,t,i)	ics_os_event_wait(*(pEvent), (t))
#define _ICS_OS_EVENT_POST(pEvent)	semaphore_signal(*(pEvent))
#define _ICS_OS_EVENT_COUNT(pEvent)	semaphore_value(*(pEvent))
#define _ICS_OS_EVENT_READY(pEvent)	(semaphore_value(*(pEvent)) != 0)

/*
 * Spinlocks (IRQ and SMP safe)
 */
typedef int _ICS_OS_SPINLOCK;

/* OS21 doesn't yet have spinlocks so just block all IRQs */
#define _ICS_OS_SPINLOCK_INIT(pLock)		(*(pLock) = 0)
#define _ICS_OS_SPINLOCK_ENTER(pLock,FLAGS)	((FLAGS) = interrupt_mask_all())
#define _ICS_OS_SPINLOCK_EXIT(pLock,FLAGS)	interrupt_unmask(FLAGS)

/*
 * Clock functions
 */
#define _ICS_OS_TIME			osclock_t
#define _ICS_OS_TIME_NOW()		time_now()
#define _ICS_OS_TIME_TICKS2MSEC(T)	((_ICS_OS_TIME)((T)*1000)/time_ticks_per_sec())
#define _ICS_OS_TIME_MSEC2TICKS(M)	(((M)*time_ticks_per_sec())/1000)
#define _ICS_OS_TIME_AFTER(A,B)		time_after((A),(B))
#define _ICS_OS_TIME_EXPIRED(S,T)	time_after(time_now(), time_plus((S), _ICS_OS_TIME_MSEC2TICKS((T))))
#define _ICS_OS_TIME_IDLE()		kernel_idle()
#define _ICS_OS_HZ			time_ticks_per_sec()

/* 
 * Thread/Task functions
 */
typedef task_t _ICS_OS_TASK;

typedef struct ics_task_info
{
  _ICS_OS_TASK	     *task;
  void              (*entry)(void *);
  void               *param;
  ICS_INT             priority;
  _ICS_OS_EVENT       event;     
} _ICS_OS_TASK_INFO;

_ICS_OS_INLINE_FUNC_PREFIX
_ICS_OS_TIME _ics_os_task_cpu (_ICS_OS_TASK_INFO *task)
{
  task_status_t status = { 0 };
  
  if (task_status(task->task, &status, 0) == OS21_SUCCESS)
    /* Return CPU time used */
    return status.task_time;
  else
    return 0;
}

#define _ICS_OS_DEFAULT_STACK_SIZE	(OS21_DEF_MIN_STACK_SIZE)
#define _ICS_OS_TASK_DEFAULT_PRIO	(MAX_USER_PRIORITY)

ICS_ERROR ics_os_task_create (void (*entry)(void *), void *param, ICS_INT priority, const ICS_CHAR *name,
			      _ICS_OS_TASK_INFO *task);
ICS_ERROR ics_os_task_destroy (_ICS_OS_TASK_INFO *task);

#define _ICS_OS_TASK_CREATE(FUNC, PARAM, PRIO, NAME, T)	ics_os_task_create((FUNC), (PARAM), (PRIO), (NAME), (T))
#define _ICS_OS_TASK_DESTROY(T)		ics_os_task_destroy((T))

#define _ICS_OS_TASK_NAME()		task_name(NULL)
#define _ICS_OS_TASK_SELF()		task_id()
#define _ICS_OS_TASK_PRIORITY_SET(PRIO)	task_priority_set(NULL, (PRIO))
#define _ICS_OS_TASK_DELAY(M)		task_delay(_ICS_OS_TIME_MSEC2TICKS((M)))
#define _ICS_OS_TASK_YIELD()		task_yield()
#define _ICS_OS_TASK_INTERRUPT()	ics_os_in_interrupt()
#define _ICS_OS_TASK_LOAD(T)		_ics_os_task_cpu((T))

/*
 * File/Firmware loading
 */
#define _ICS_OS_FHANDLE			FILE *
#define _ICS_OS_FOPEN(N, FHP)		(*(FHP) = fopen((N), "rb"), *(FHP) == NULL)
#define _ICS_OS_FREAD(M, FH, S)		fread((M), 1, (S), (FH))
#define _ICS_OS_FSEEK(FH, POS)		fseek((FH),(POS),(SEEK_SET))
#define _ICS_OS_FCLOSE(FH)		fclose((FH))
#define _ICS_OS_FSIZE(FH, SP)		do {*(SP) = (fseek((FH), (off_t)0, SEEK_END)) ? 0 : ftell((FH)); rewind(FH);} while (0)

/*
 * OS heap info
 */

#endif /* _ICS_OS21_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
