/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_ELF_H
#define _ICS_ELF_H

ICS_ERROR ics_elf_check_magic (ICS_CHAR *hdr);
ICS_ERROR ics_elf_check_hdr (ICS_CHAR *image);

ICS_OFFSET ics_elf_entry (ICS_CHAR *image);
ICS_ERROR ics_elfimage_load_size (ICS_CHAR *image, ICS_OFFSET *basep, ICS_SIZE *sizep, ICS_SIZE *alignp, ICS_OFFSET *bootp);
ICS_ERROR ics_elfimage_load_image (ICS_CHAR *image, ICS_VOID *mem, ICS_SIZE memSize, ICS_OFFSET baseAddr);
ICS_ERROR ics_elffile_load_size (_ICS_OS_FHANDLE file,ICS_CHAR *image, ICS_OFFSET *basep, ICS_SIZE *sizep, ICS_SIZE *alignp, ICS_OFFSET *bootp);
ICS_ERROR ics_elffile_load_image (_ICS_OS_FHANDLE file,ICS_CHAR *image, ICS_VOID *mem, ICS_SIZE memSize, ICS_OFFSET baseAddr);

#endif /* _ICS_ELF_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

