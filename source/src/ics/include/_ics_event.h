/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_EVENT_SYS_H
#define _ICS_EVENT_SYS_H

typedef enum ics_event_state
{
  _ICS_EVENT_FREE = 0,
  _ICS_EVENT_ALLOC,
  _ICS_EVENT_ABORTED,
} ics_event_state_t;

typedef struct ics_event
{
  ics_event_state_t	 state;		/* Event state */

  struct list_head	 list;		/* Doubly linked list entry */

  _ICS_OS_EVENT		 event;		/* OS Event for blocking on */

  void 			*data;		/* Event specific data */

} ics_event_t;

ICS_EXPORT ICS_ERROR ics_event_alloc (ics_event_t **event);
ICS_EXPORT void      ics_event_free (ics_event_t *event);
ICS_EXPORT ICS_ERROR ics_event_wait (ics_event_t *event, ICS_UINT timeout);
ICS_EXPORT ICS_ERROR ics_event_test (ics_event_t *event, ICS_BOOL *readyp);

ICS_EXPORT ICS_ERROR ics_event_init (void);
ICS_EXPORT void      ics_event_term (void);

#ifdef __arm__
void ics_event_lock(void);
void ics_event_unlock(void);
#endif
/* 
 * Signal an event
 */
_ICS_OS_INLINE_FUNC_PREFIX 
void ics_event_post (ics_event_t *event)
{
  ICS_ASSERT(event);
  
  _ICS_OS_EVENT_POST(&event->event);
}


#endif /* _ICS_EVENT_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
