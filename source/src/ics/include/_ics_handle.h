/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_HANDLE_H
#define _ICS_HANDLE_H

/* Encode <type,cpu,ver,idx> into the 32-bit ICS handle
 *
 * [31-29] Handle type (3-bit)
 * [28-24] Cpu         (5-bit)
 * [19-12] Version     (12-bit)
 * [11-00] Index       (12-bit)
 *
 */
#define _ICS_HANDLE_TYPE_SHIFT 		(29)
#define _ICS_HANDLE_TYPE_MASK 		(0x7)
#define _ICS_HANDLE_CPU_SHIFT 		(24)
#define _ICS_HANDLE_CPU_MASK 		(0x1f)
#define _ICS_HANDLE_VER_SHIFT 		(12)
#define _ICS_HANDLE_VER_MASK 		(0xfff)
#define _ICS_HANDLE_IDX_SHIFT 		(0)
#define _ICS_HANDLE_IDX_MASK 		(0xfff)

/* Generate a handle from the TYPE, CPU, VER and IDX
 * We assume that TYPE, CPU & IDX don't overflow their bitmask widths
 * but for VER we need to mask and wrap it
 */
#define _ICS_HANDLE(TYPE, CPU, VER, IDX) ((TYPE) << _ICS_HANDLE_TYPE_SHIFT | ((CPU) << _ICS_HANDLE_CPU_SHIFT) | ((VER) & _ICS_HANDLE_VER_MASK) << _ICS_HANDLE_VER_SHIFT | ((IDX) & _ICS_HANDLE_IDX_MASK) << _ICS_HANDLE_IDX_SHIFT)

/* Extract the Type from a handle */
#if defined (__arm__) && defined __KERNEL__
/* Extract the CPU number from a handle */
#define __ICS_HDL2CPU(HDL)	(((HDL) >> _ICS_HANDLE_CPU_SHIFT) & _ICS_HANDLE_CPU_MASK)
/* Extract the CPU number from a handle cpu 31 is reserved for userspace
 * transformer which uses cpu 0 ics shm.  */
#define _ICS_HDL2CPU(HDL) ((__ICS_HDL2CPU(HDL)==ICS_USERSPACE_CPU)?0:(__ICS_HDL2CPU(HDL)))
#else
#define   _ICS_HDL2CPU(HDL)	(((HDL) >> _ICS_HANDLE_CPU_SHIFT) & _ICS_HANDLE_CPU_MASK)
#endif
/* Extract the type number from a handle */
#define _ICS_HDL2TYPE(HDL)	    	(((HDL) >> _ICS_HANDLE_TYPE_SHIFT) & _ICS_HANDLE_TYPE_MASK)

/* Extract the Version number from a handle */
#define _ICS_HDL2VER(HDL)	    	(((HDL) >> _ICS_HANDLE_VER_SHIFT) & _ICS_HANDLE_VER_MASK)
  
/* Extract the Index from a handle */
#define _ICS_HDL2IDX(HDL)	    	(((HDL) >> _ICS_HANDLE_IDX_SHIFT) & _ICS_HANDLE_IDX_MASK)

/* Split a handle into its component parts */
#define _ICS_DECODE_HDL(HDL, TYPE, CPU, VER, IDX) do { \
    (TYPE) = _ICS_HDL2TYPE(HDL);		       \
    (CPU)  = _ICS_HDL2CPU(HDL);			       \
    (VER)  = _ICS_HDL2VER(HDL);			       \
    (IDX)  = _ICS_HDL2IDX(HDL); } while (0)
			       
/*
 * ICS Possible handle types (3-bit field)
 */
#define _ICS_TYPE_PORT		(0x1)	/* PORT handle */
#define _ICS_TYPE_CHANNEL	(0x2)	/* CHANNEL handle */
#define _ICS_TYPE_DYN		(0x3)	/* DYNAMIC module handle */
#define _ICS_TYPE_REGION	(0x4)	/* REGION handle */
#define _ICS_TYPE_NSRV		(0x5)	/* NSRV entry handle */
#define _ICS_TYPE_MNGT      (0x6)   /* NOT A HANDLE, fills port value of message like _ICS_TYPE_PORT */ 

#endif /* _ICS_HANDLE_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
