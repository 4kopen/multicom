/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_UTIL_SYS_H
#define _ICS_UTIL_SYS_H

/*
 * A collection of useful macros
 */

#define ALIGNUP(x,a)    (((unsigned long)(x) + ((a)-1UL)) & ~((a)-1UL)) /* 'a' must be a power of 2 */
#define ALIGNED(x,a)	(!((unsigned long)(x) & ((a)-1UL)))		/* 'a' must be a power of 2 */

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)	(sizeof(a)/sizeof((a)[0]))
#endif

/* Determine whether a positive integer is a power of 2 or not */
#define powerof2(N)	((N) && ((N-1) & (N)) == 0)

#ifndef min
#define min(_a, _b) 	(((_a) < (_b)) ? (_a) : (_b))
#endif

#ifndef max
#define max(_a, _b) 	(((_a) > (_b)) ? (_a) : (_b))
#endif

#ifdef __GNUC__
#define log2i(X)	__builtin_ctz(X)
#else
static inline 
unsigned int log2i(unsigned int x)
{
  unsigned int level = 0;

  while (x)
  {
    x >>= 1;
    level++;
  }

  return level;
}
#endif /* __builtin_ctz */

#ifdef __GNUC__
#define RETURN_ADDRESS(N)	__builtin_return_address((N))
#else
#define RETURN_ADDRESS(N)	NULL
#endif

#endif /* _ICS_UTIL_SYS_H */
