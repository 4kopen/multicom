/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_LIMITS_SYS_H
#define _ICS_LIMITS_SYS_H

/*
 * ICS Compile time tuneables
 */

#define _ICS_MAX_CPUS			8	/* Maximum # of CPUs supported (see also _ICS_HANDLE_CPU_MASK) */
#define _ICS_MAX_CHANNELS		32	/* Maximum # of channels per CPU */

#define _ICS_MAX_MAILBOXES		4	/* Maximum mailboxes per CPU */

#define _ICS_INIT_PORTS			16	/* Initial # of local Ports per CPU */
#define _ICS_MAX_PORTS			256	/* Maximum # of local Ports per CPU */
#define _ICS_MAX_REGIONS		96	/* Maximum # of mapped regions per CPU */
#define _ICS_MAX_DYN_MOD		16	/* Maximum # of dynamic modules per CPU */

#define _ICS_CONNECT_TIMEOUT		30000	/* Inter-cpu connection timeout (in ms) */
#ifdef ICS_DEBUG
#define _ICS_COMMS_TIMEOUT		10000	/* Inter-cpu communications timeout (in ms) */
#else
#define _ICS_COMMS_TIMEOUT		1000	/* Inter-cpu communications timeout (in ms) */
#endif

#define _ICS_MSG_NSLOTS			256	/* Number of msg slots per CPU channel FIFO */

#define _ICS_NSRV_NUM_ENTRIES		128	/* Default initial name table size */

#define _ICS_WATCHDOG_INTERVAL		500	/* Watchdog timeout/timer interval in ms  */ /* BZ 46714*/
#define _ICS_WATCHDOG_FAILURES		3	  /* Number of timeout intervals before failure report */

#define _ICS_MAX_PATHNAME_LEN	       	63	/* Maximum pathname length (see also ICS_MSG_INLINE_DATA) */

#define _ICS_STATS_INTERVAL		1000	/* Stats helper timeout/timer interval in ms */

#endif /* _ICS_LIMITS_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
