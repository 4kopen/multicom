/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_TRANSPORT_H
#define _ICS_TRANSPORT_H

ICS_EXPORT ICS_ERROR ics_transport_init (ICS_UINT cpuNum, ICS_ULONG cpuMask);
ICS_EXPORT void      ics_transport_term (void);
ICS_EXPORT ICS_ERROR ics_transport_start (void);
ICS_EXPORT void      ics_transport_stop (void);

ICS_EXPORT ICS_ERROR ics_transport_connect (ICS_UINT cpuNum, ICS_UINT flags, ICS_UINT timeout); 	/* for bz33688 */
ICS_EXPORT ICS_ERROR ics_transport_disconnect (ICS_UINT cpuNum, ICS_UINT flags);

ICS_EXPORT void      ics_transport_watchdog (ICS_ULONG timestamp);
ICS_EXPORT ICS_ULONG ics_transport_watchdog_query (ICS_UINT cpuNum);

ICS_EXPORT void      ics_transport_debug_msg (ICS_CHAR *msg, ICS_UINT len);
ICS_EXPORT ICS_ERROR ics_transport_debug_dump (ICS_UINT cpuNum);
ICS_EXPORT ICS_ERROR ics_transport_debug_copy (ICS_UINT cpuNum, ICS_CHAR *buf, ICS_SIZE bufSize, ICS_INT *bytesp);

ICS_EXPORT ICS_ERROR ics_transport_stats_add (const ICS_CHAR *name, ICS_STATS_TYPE type, ICS_STATS_HANDLE *handlep);
ICS_EXPORT ICS_ERROR ics_transport_stats_remove (ICS_STATS_HANDLE handle);
ICS_EXPORT ICS_ERROR ics_transport_stats_update (ICS_STATS_HANDLE handle, ICS_STATS_VALUE value, ICS_STATS_TIME timestamp);
ICS_EXPORT ICS_ERROR ics_transport_stats_sample (ICS_UINT cpuNum, ICS_STATS_ITEM *stats, ICS_UINT *nstatsp);

ICS_EXPORT ICS_ERROR ics_transport_cpu_version (ICS_UINT cpuNum, ICS_ULONG *versionp);

#endif /* _ICS_TRANSPORT_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
