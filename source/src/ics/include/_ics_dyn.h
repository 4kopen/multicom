/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_DYN_SYS_H
#define _ICS_DYN_SYS_H

#ifdef __sh__
/* The sh linker prefixes symbol names with an '_' */
#define _ICS_DYN_SYM_PREFIX "_"
#else
#define _ICS_DYN_SYM_PREFIX ""
#endif

/* These are the names of the functions which will be called
 * (if present) as each module is loaded or unloaded
 */
#define _ICS_DYN_INIT_NAME  _ICS_DYN_SYM_PREFIX "module_init"
#define _ICS_DYN_TERM_NAME  _ICS_DYN_SYM_PREFIX "module_term"

typedef struct ics_dyn_mod
{
  ICS_CHAR	 name[_ICS_MAX_PATHNAME_LEN+1];	/* Name of module (for debugging purposes) */

  void		*handle;			/* Relocation system handle */
  ICS_DYN  	 parent;			/* Module's parent handle */
  
} ics_dyn_mod_t;

/* Exported private APIs */
ICS_ERROR ics_dyn_load_image (ICS_CHAR *name, ICS_OFFSET paddr, ICS_SIZE imageSize, ICS_UINT flags,
			      ICS_DYN handle, ICS_DYN *handlep);
ICS_ERROR ics_dyn_unload (ICS_DYN handle);

#endif /* _ICS_DYN_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

