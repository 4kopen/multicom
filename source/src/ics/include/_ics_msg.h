/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _ICS_MSG_SYS_H
#define _ICS_MSG_SYS_H

#define _ICS_MSG_SSIZE			(sizeof(ics_msg_t))	/* Size of a message channel slot */

typedef struct ics_msg 
{
  ICS_UINT		srcCpu;		/* Sending cpu # */
  ICS_PORT       	port;		/* Target port # */
  ICS_OFFSET		data;		/* 'address' of data (if not ICS_INLINE) */
  ICS_SIZE		size;		/* Size of message */
  ICS_MEM_FLAGS		mflags;		/* Memory/Buffer flags */
  ICS_UINT		seqNo;		/* Message seqNo to aid debugging */    
  
  /* Pad to next cacheline boundary */
  ICS_CHAR		pad[_ICS_CACHELINE_PAD(4,2)];
  
  /* CACHELINE ALIGNED */
  ICS_CHAR		payload[ICS_MSG_INLINE_DATA];	/* Defined in ics_msg.h */
  
} ics_msg_t;

/* Per Channel message handler callback */
ICS_ERROR ics_message_handler (ICS_CHANNEL handle, void *p, void *b);

/* Exported internal APIs */
ICS_ERROR _ics_msg_send (ICS_PORT port, ICS_VOID *buf, ICS_MEM_FLAGS mflags, ICS_UINT size, ICS_UINT flags, 
			 ICS_UINT cpuNum);

#endif /* _ICS_MSG_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
