/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#include <bsp/_bsp.h>
#if defined(__arm__)
extern void enable_slave_clock(int cpu_num);
#endif

/*
 * Routines to manage the logical to BSP translation of cpu names and CPU numbers
 * as well as SH4 specific code to load and reset the CPUs via BSP lookup tables
 *
 * The reset code is a direct copy of that found in the sh4 toolkit slaveboot.c
 * example code
 */

#ifdef __arm__
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
#define __MULTICOM_RPROC__
#endif

#ifndef __MULTICOM_RPROC__

static unsigned int *bsp_lx_vid_register_map;
static unsigned int *bsp_lx_aud_register_map;
static unsigned int *bsp_lx_gp0_register_map;
static unsigned int *bsp_lx_gp2_register_map;

ICS_ERROR ics_cpu_start_fw (const ICS_CHAR *fname, ICS_UINT cpuNum)
{
  return ICS_INVALID_ARGUMENT;
}

/*
 * Set the boot address of a CPU *without* resetting the CPU.
 */
static
ICS_ERROR setBootAddress (ICS_UINT index, ICS_OFFSET startAddress)
{
  ICS_OFFSET bootRegORVal;

  if (bsp_sys_boot_registers[index].address == NULL)
    return -1;

  bootRegORVal = (startAddress << bsp_sys_boot_registers[index].leftshift) & bsp_sys_boot_registers[index].mask;
  if (bootRegORVal != ((startAddress << bsp_sys_boot_registers[index].leftshift)))
  {
    /* Boot address was invalid - would not fit in register without losing information */
    ICS_EPRINTF(ICS_DBG_LOAD, "Invalid sys boot info: startAddr 0x%x lsh %d mask 0x%x != 0x%x\n",
		startAddress, 
		bsp_sys_boot_registers[index].leftshift, 
		bsp_sys_boot_registers[index].mask,
		bootRegORVal);
    
    return ICS_INVALID_ARGUMENT;
  }

  *bsp_lx_vid_register_map = 0x0E400000;
  *bsp_lx_aud_register_map = 0x1A200000;
  *bsp_lx_gp0_register_map = 0x1A100000;
  *bsp_lx_gp2_register_map = 0x1A000000;
  
  ICS_PRINTF(ICS_DBG_LOAD, "writing to boot register %p = 0x%x\n",
	     bsp_sys_boot_registers[index].address,
	     (*(bsp_sys_boot_registers[index].address) & ~(bsp_sys_boot_registers[index].mask)) | bootRegORVal);
  
  *(bsp_sys_boot_registers[index].address) =
    (*(bsp_sys_boot_registers[index].address) & ~(bsp_sys_boot_registers[index].mask)) | bootRegORVal;

  ICS_PRINTF(ICS_DBG_LOAD, "boot register %p = 0x%x\n",
	     bsp_sys_boot_registers[index].address,
	     *(bsp_sys_boot_registers[index].address));

  return ICS_SUCCESS;
}

/* 
 * Companion reset/boot sequence
 *
 * This code is based on slaveboot.c code from the sh4 toolkit examples tree
 */
static
ICS_ERROR slaveBoot (ICS_UINT index, ICS_OFFSET startAddress)
{
  ICS_ERROR res;
#ifdef __arm__
  enable_slave_clock(index);
#endif
  /* Unset the CPU's "allow boot" - needed for STi7141 - older SoCs don't mind */
  if (bsp_sys_boot_enable[index].address) 
  {
    ICS_PRINTF(ICS_DBG_LOAD, "clearing boot enable %p (0x%x): mask 0x%x\n",
	       (void *)bsp_sys_boot_enable[index].address,
	       *(bsp_sys_boot_enable[index].address),
	       bsp_sys_boot_enable[index].mask);
    *(bsp_sys_boot_enable[index].address) &= bsp_sys_boot_enable[index].mask;
  }

  /* Bypass CPU core reset out signals */
  {
    unsigned int i;
    for (i = 0; i < bsp_sys_reset_bypass_count; i++)
    {
      if (bsp_sys_reset_bypass[i].address)
      {
	ICS_PRINTF(ICS_DBG_LOAD, "mask & set reset bypass %p (0x%x): value 0x%x mask 0x%x\n",
		   (void *)bsp_sys_reset_bypass[i].address,
		   *(bsp_sys_reset_bypass[i].address),
		   bsp_sys_reset_bypass[i].value,
		   bsp_sys_reset_bypass[i].mask);
		   
        *(bsp_sys_reset_bypass[i].address) = bsp_sys_reset_bypass[i].value | 
	  ((*(bsp_sys_reset_bypass[i].address)) & bsp_sys_reset_bypass[i].mask);
      }
    }
  }

  /* Configure the boot address */
  res = setBootAddress(index, startAddress);
  if (res != ICS_SUCCESS)
  {
    /* Failed to set boot address to startAddress */
    return res;
  }

  /* Assert and de-assert reset */
  if (bsp_sys_reset_registers[index].value)
  {
    ICS_PRINTF(ICS_DBG_LOAD, "toggle AH sys reset register %p (0x%x) value 0x%x mask 0x%x\n",
	       bsp_sys_reset_registers[index].address,
	       *(bsp_sys_reset_registers[index].address),
	       bsp_sys_reset_registers[index].value,
	       bsp_sys_reset_registers[index].mask);
    
    /* Reset is active high */
    *(bsp_sys_reset_registers[index].address) |= bsp_sys_reset_registers[index].value;
    *(bsp_sys_reset_registers[index].address) &= bsp_sys_reset_registers[index].mask;
  }
  else
  {
    ICS_PRINTF(ICS_DBG_LOAD, "toggle AL sys reset register %p (0x%x) mask : 0x%x\n",
	       bsp_sys_reset_registers[index].address,
	       *(bsp_sys_reset_registers[index].address),
	       bsp_sys_reset_registers[index].mask);

    /* Reset is active low - just use mask and ~mask */
    *(bsp_sys_reset_registers[index].address) &= bsp_sys_reset_registers[index].mask;
    *(bsp_sys_reset_registers[index].address) |= ~bsp_sys_reset_registers[index].mask;   
  }

  /* Set the CPU's "allow boot"
   * This must be *after* the reset cycle on sti7141 - older SoCs don't mind.
   */
  if (bsp_sys_boot_enable[index].address)
  {
    ICS_PRINTF(ICS_DBG_LOAD, "setting boot enable %p (0x%x): value 0x%x mask 0x%x\n",
	       (void *)bsp_sys_boot_enable[index].address,
	       *(bsp_sys_boot_enable[index].address),
	       bsp_sys_boot_enable[index].value,
	       bsp_sys_boot_enable[index].mask);
	       
    *(bsp_sys_boot_enable[index].address) = bsp_sys_boot_enable[index].value |
      ((*(bsp_sys_boot_enable[index].address)) & bsp_sys_boot_enable[index].mask);
  }
  
  return ICS_SUCCESS;
}

#if defined(__arm__)
extern unsigned int           bsp_sys_boot_registers_count;
extern unsigned int           bsp_sys_reset_registers_count;
extern unsigned int           bsp_debug_cpu_registers_count;
extern struct bsp_reg_mask    bsp_debug_cpu_registers[];
extern unsigned int           ics_bsp_cpu_debug_flag[];
extern struct bsp_reg_mask    bsp_debug_unlock_registers[];

void slaveResetRegMap (void)
{
		static int ResetRegMapped = ICS_FALSE;
    unsigned int i;

    /* Check if register already mapped.*/
				if (ICS_TRUE == ResetRegMapped)
						return;

    bsp_lx_vid_register_map = _ICS_OS_MMAP((unsigned long)0x0F130000,_ICS_OS_PAGESIZE, ICS_FALSE);
    bsp_lx_aud_register_map = _ICS_OS_MMAP((unsigned long)0x1B130040,_ICS_OS_PAGESIZE, ICS_FALSE);
    bsp_lx_gp0_register_map = _ICS_OS_MMAP((unsigned long)0x1B130020,_ICS_OS_PAGESIZE, ICS_FALSE);
    bsp_lx_gp2_register_map = _ICS_OS_MMAP((unsigned long)0x1B130000,_ICS_OS_PAGESIZE, ICS_FALSE);

    /* Map reset and boot register into kernel space */  
    for (i = 0; i < bsp_sys_reset_bypass_count; i++)
    {
      if (bsp_sys_reset_bypass[i].address)
      {
        /* Must map the physical bsp_sys_reset_bypass register addresses into (uncached) kernel space */
        bsp_sys_reset_bypass[i].address = _ICS_OS_MMAP((unsigned long)bsp_sys_reset_bypass[i].address,_ICS_OS_PAGESIZE, ICS_FALSE);
      }
    }
    for (i = 0; i < bsp_sys_boot_registers_count; i++)
    {
      if (bsp_sys_boot_registers[i].address)
      {
        /* Must map the physical bsp_sys_boot register addresses into (uncached) kernel space */
        bsp_sys_boot_registers[i].address = _ICS_OS_MMAP((unsigned long)bsp_sys_boot_registers[i].address,_ICS_OS_PAGESIZE, ICS_FALSE);
      }
    }
    for (i = 0; i < bsp_sys_reset_registers_count; i++)
    {
      if (bsp_sys_reset_registers[i].address)
      {
        /* Must map the physical bsp_sys_reset register addresses into (uncached) kernel space */
        bsp_sys_reset_registers[i].address = _ICS_OS_MMAP((unsigned long)bsp_sys_reset_registers[i].address,_ICS_OS_PAGESIZE, ICS_FALSE);
      }
    }

    /* Map debug control register */
    {
      volatile unsigned int *bsp_debug_register_map;
      
      if(bsp_debug_cpu_registers[0].address)
        bsp_debug_register_map = _ICS_OS_MMAP((unsigned long)bsp_debug_cpu_registers[0].address,_ICS_OS_PAGESIZE, ICS_FALSE);
      else
        bsp_debug_register_map = NULL;
          
      for (i = 0; i < bsp_debug_cpu_registers_count; i++)
        /* Must map the physical bsp_sys_reset register addresses into (uncached) kernel space */
        bsp_debug_cpu_registers[i].address =  bsp_debug_register_map;

      /* Map CPU debug control register access unlock */
      if(bsp_debug_unlock_registers[0].address)
        bsp_debug_unlock_registers[0].address = _ICS_OS_MMAP((unsigned long)bsp_debug_unlock_registers[0].address,_ICS_OS_PAGESIZE, ICS_FALSE);

    }

    /* Set register mapped flag.*/
				ResetRegMapped = ICS_TRUE;
}

void slaveUnmapRegMap (void)
{
    unsigned int i;

    /* Unmap reset and boot register into kernel space */
    for (i = 0; i < bsp_sys_reset_bypass_count; i++)
    {
      if (bsp_sys_reset_bypass[i].address)
      {
        /* Must unmap the physical bsp_sys_reset_bypass register addresses into (uncached) kernel space */
        _ICS_OS_MUNMAP((unsigned long)bsp_sys_reset_bypass[i].address);
		bsp_sys_reset_bypass[i].address = NULL;
      }
    }
    for (i = 0; i < bsp_sys_boot_registers_count; i++)
    {
      if (bsp_sys_boot_registers[i].address)
      {
        /* Must unmap the physical bsp_sys_boot register addresses into (uncached) kernel space */
        _ICS_OS_MUNMAP((unsigned long)bsp_sys_boot_registers[i].address);
		bsp_sys_boot_registers[i].address = NULL;
      }
    }
    for (i = 0; i < bsp_sys_reset_registers_count; i++)
    {
      if (bsp_sys_reset_registers[i].address)
      {
        /* Must unmap the physical bsp_sys_reset register addresses into (uncached) kernel space */
        _ICS_OS_MUNMAP((unsigned long)bsp_sys_reset_registers[i].address);
		bsp_sys_reset_registers[i].address = NULL;
	  }
    }

    /* Unmap debug control register */
    {
      volatile unsigned int *bsp_debug_register_map;

      if(bsp_debug_cpu_registers[0].address)
	  {
        _ICS_OS_MUNMAP((unsigned long)bsp_debug_cpu_registers[0].address);
        bsp_debug_cpu_registers[0].address = NULL;
	  }

      /* Unmap CPU debug control register access unlock */
      if(bsp_debug_unlock_registers[0].address)
	  {
        _ICS_OS_MUNMAP((unsigned long)bsp_debug_unlock_registers[0].address);
		bsp_debug_unlock_registers[0].address = NULL;
	  }
    }
}

static ICS_ERROR ics_cpu_debug_disable(ICS_UINT cpuNum)
{
  int index;
  
  /* Translate cpuNum into a BSP table index */
  index = ics_cpu_index(cpuNum);

  if(bsp_debug_cpu_registers[index].address == NULL)
    return ICS_SYSTEM_ERROR;
  
  /* Read debug flag */
  ics_bsp_cpu_debug_flag[index] = *bsp_debug_cpu_registers[index].address & bsp_debug_cpu_registers[index].value;

  /* unlock stbus to write to CPU debug control registers */
  *bsp_debug_unlock_registers[0].address = bsp_debug_unlock_registers[0].value;

  /* Disable flag    */
  *bsp_debug_cpu_registers[index].address = *bsp_debug_cpu_registers[index].address & ~bsp_debug_cpu_registers[index].value;

  return ICS_SUCCESS;

}

static ICS_ERROR ics_cpu_debug_restore(ICS_UINT cpuNum)
{
  int index;
  
  /* Translate cpuNum into a BSP table index */
  index = ics_cpu_index(cpuNum);

  if(bsp_debug_cpu_registers[index].address == NULL)
    return ICS_SYSTEM_ERROR;

  /* unlock stbus to write to CPU debug control registers */
  *bsp_debug_unlock_registers[0].address = bsp_debug_unlock_registers[0].value;

  /* Restore debug flag */
/*  *bsp_debug_cpu_registers[index].address = *bsp_debug_cpu_registers[index].address | ics_bsp_cpu_debug_flag[index];*/

  return ICS_SUCCESS;
}
#endif /* __arm__*/

static void slaveReset (ICS_UINT index)
{
  /* Bypass CPU core reset out signals */
  {
    unsigned int i;
	#if defined(__arm__)
	printk("slaveReset %d\n", index);
	enable_slave_clock(index);
	printk("Reseting cpu %d\n", index);
	#endif
    for (i = 0; i < bsp_sys_reset_bypass_count; i++)
    {
      if (bsp_sys_reset_bypass[i].address)
      {
	ICS_PRINTF(ICS_DBG_LOAD, "mask & set reset bypass %p (0x%x): value 0x%x mask 0x%x\n",
		   (void *)bsp_sys_reset_bypass[i].address,
		   *(bsp_sys_reset_bypass[i].address),
		   bsp_sys_reset_bypass[i].value,
		   bsp_sys_reset_bypass[i].mask);

        *(bsp_sys_reset_bypass[i].address) = bsp_sys_reset_bypass[i].value | 
	  ((*(bsp_sys_reset_bypass[i].address)) & bsp_sys_reset_bypass[i].mask);
      }
    }
  }

  /* Assert and hold reset */
  if (bsp_sys_reset_registers[index].value)
  {
    ICS_PRINTF(ICS_DBG_LOAD, "toggle AH sys reset register %p value 0x%x mask 0x%x\n",
	       bsp_sys_reset_registers[index].address,
	       bsp_sys_reset_registers[index].value,
	       bsp_sys_reset_registers[index].mask);
    
    /* Reset is active high - just use value */
    *(bsp_sys_reset_registers[index].address) |= bsp_sys_reset_registers[index].value;
  }
  else
  {
    ICS_PRINTF(ICS_DBG_LOAD, "toggle AL sys reset register %p mask : 0x%x\n",
	       bsp_sys_reset_registers[index].address,
	       bsp_sys_reset_registers[index].mask);

    /* Reset is active low - just use mask */
    *(bsp_sys_reset_registers[index].address) &= bsp_sys_reset_registers[index].mask;
  }
}

ICS_ERROR ics_cpu_start (ICS_OFFSET entryAddr, ICS_UINT cpuNum, ICS_UINT flags)
{
  ICS_ERROR res;
  ICS_UINT validFlags = ICS_CPU_AFTER_DEATH;	/* for bz33688 */
  
  int index;
  
  if (cpuNum >= _ICS_MAX_CPUS || entryAddr == ICS_BAD_OFFSET || (flags & ~validFlags))
    return ICS_INVALID_ARGUMENT;
  
  /* Translate cpuNum into a BSP table index */
  index = ics_cpu_index(cpuNum);
  
  if (index < 0)
    return ICS_NOT_INITIALIZED;

  ICS_PRINTF(ICS_DBG_LOAD, "Starting cpu %d index %d entryAddr 0x%x\n",
	     cpuNum, index, entryAddr);

  res = slaveBoot(index, entryAddr);

#if defined(__arm__)
  /* Restore debug control for target cpu */
  ics_cpu_debug_restore(cpuNum);
#endif

  return res;
}



ICS_ERROR ics_cpu_reset (ICS_UINT cpuNum, ICS_UINT flags)
{
  ICS_UINT validFlags = 0;

  int index;

  if (cpuNum >= _ICS_MAX_CPUS || (flags & ~validFlags))
    return ICS_INVALID_ARGUMENT;

  /* Translate cpuNum into a BSP table index */
  index = ics_cpu_index(cpuNum);

  if (index < 0)
    return ICS_NOT_INITIALIZED;

  ICS_PRINTF(ICS_DBG_LOAD, "resetting cpu %d index %d\n",
	     cpuNum, index);

#if defined(__arm__)
  /*  Map the reset registers physical addresses into (uncached) kernel space */
  slaveResetRegMap();
#endif /* __arm__ */

#if defined(__arm__)
  /* Disable debug control for target cpu */
  ics_cpu_debug_disable(cpuNum);
#endif /* __arm__ */

  /* Reset the corresponding CPU */
  slaveReset(index);

  return ICS_SUCCESS;
}
void ics_cpu_reset_all (void) 
{
	int i;
#if defined(__arm__)
	slaveResetRegMap();
#endif /* __arm__ */
	for (i = 0; i < bsp_cpu_count; i++)
	{
		if (bsp_cpus[i].num != 0)
			slaveReset(i);
	}
#if defined(__arm__)
	printk("all cpu reseted\n");
#endif 
}

#endif /* #ifndef __MULTICOM_RPROC__ */

#endif /* #ifdef __arm__ */

/* Find a CPU in the BSP table and return its logical cpuNum */
int ics_cpu_lookup (const ICS_CHAR *cpuName)
{ 
  int i;

  /* Search BSP CPU table for a match */
  for (i = 0; i < bsp_cpu_count; i++)
  {
    if (!strcmp(cpuName, bsp_cpus[i].name))
    {
      /* Match found - return logical CPU number */
      return bsp_cpus[i].num;
    }
  }
  
  /* No match */
  return -1;
}

/* Return local CPU's logical number */
ICS_INT ics_cpu_self (void)
{
  return ics_cpu_lookup(bsp_cpu_name);
}

/* Return a bitmask of each logical CPU present */
ICS_ULONG ics_cpu_mask (void)
{
  int i;
  ICS_ULONG cpuMask = 0;

  for (i = 0; i < bsp_cpu_count; i++)
  {
    /* -ve num tells us to skip this one */
    if (bsp_cpus[i].num >= 0)
      cpuMask |= (1UL << bsp_cpus[i].num);
  }
  
  return cpuMask;
}

/* Return the CPU name of a given cpuNum */
const char *ics_cpu_name (ICS_UINT cpuNum)
{
  int i;

  if (cpuNum >= _ICS_MAX_CPUS)
    return "invalid";

  for (i = 0; i < bsp_cpu_count; i++)
  {
    if (bsp_cpus[i].num == cpuNum)
      return bsp_cpus[i].name;
  }

  return "unknown";
}

/* Return the CPU type of a given cpuNum */
const char *ics_cpu_type (ICS_UINT cpuNum)
{
  int i;

  if (cpuNum >= _ICS_MAX_CPUS)
    return "invalid";

  for (i = 0; i < bsp_cpu_count; i++)
  {
    if (bsp_cpus[i].num == cpuNum)
      return bsp_cpus[i].type;
  }
  
  return "unknown";

}

/* Return the BSP index of a given cpuNum */
int ics_cpu_index (ICS_UINT cpuNum)
{
  int i;

  if (cpuNum >= _ICS_MAX_CPUS)
    return -1;

  for (i = 0; i < bsp_cpu_count; i++)
  {
    if (bsp_cpus[i].num == cpuNum)
      return i;
  }

  return -1;
}

/*
 * Return a pointer to the ICS version string
 *
 * This string takes the form: 
 *
 *   {major number}.{minor number}.{patch number} : [text]
 *
 * That is, a major, minor and release number, separated by
 * decimal points, and optionally followed by a colon and a printable text string.
 */
const ICS_CHAR *ics_cpu_version (void)
{
  static const ICS_CHAR * version_string = __ICS_VERSION__" ("__TIME__" "__DATE__")";
  
  return version_string;
}


/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

