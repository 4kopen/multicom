/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/**************************************************************
 *
 * File: ics_msg_send.c
 *
 * Description
 *    Routines to implement the ICS message passing API
 *
 **************************************************************/

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#include "_ics_shm.h"

/*
 * Internal msg send code
 * This is made available so internal routines can send messages whilst
 * holding the state lock.
 * If the target cpu is not mapped it will do an ics_cpu_connect() which will connect
 * to the target but not do the region table update as that can deadlock the admin tasks
 *
 * MULTITHREAD SAFE: Called holding the state lock
 */
ICS_ERROR _ics_msg_send (ICS_PORT port, ICS_VOID *buf, ICS_MEM_FLAGS mflags, ICS_UINT size, ICS_UINT flags, 
			 ICS_UINT cpuNum)
{
  ICS_ERROR     err = ICS_SUCCESS;

  size_t        msgSize = 0;
  ics_msg_t     msg;

  ics_cpu_t    *cpu;

  cpu = &ics_state->cpu[cpuNum];

  /* Check that the target cpu is at least mapped */
  if (cpu->state != _ICS_CPU_CONNECTED && cpu->state != _ICS_CPU_MAPPED)
  {
    if (flags & ICS_MSG_CONNECT)
    {
      /* Connect to target cpu, but don't do the update regions */
#ifdef __arm__
	_ICS_OS_UP_READ(&ics_state->semLock);
   _ICS_OS_DOWN_WRITE(&ics_state->semLock);
#endif
      err = _ics_cpu_connect(cpuNum, 0, _ICS_CONNECT_TIMEOUT);
#ifdef __arm__
	_ICS_OS_UP_WRITE(&ics_state->semLock);
   _ICS_OS_DOWN_READ(&ics_state->semLock);
#endif

      if (err != ICS_SUCCESS)
	return ICS_NOT_CONNECTED;
    }
    else
      return ICS_NOT_CONNECTED;
  }


  msg.data = ICS_BAD_OFFSET;
  
  /* If the payload is inline, copy it to msg buffer */
  if (mflags & ICS_INLINE)
  {
    if (size > ICS_MSG_INLINE_DATA)
    {


      /* Inline messages must fit within a single FIFO slot */
      return ICS_INVALID_ARGUMENT;
    }

    /* Marshall data onto the stack */
    _ICS_OS_MEMCPY(msg.payload, buf, size);

    msgSize = offsetof(ics_msg_t, payload[size]);	/* msgSize includes hdr + payload */
  }
  else
  {
    if (size)
    {
      ICS_MEM_FLAGS buf_mflags;

      /* Convert a region buf ptr to a physical address 
       * The paddr will then be converted back to a local pointer
       * in the target cpu. Only works if the buffer has been allocated
       * from the local heaps or registered regions
       */
      /* Holding state lock call internal API */
      err = _ics_region_virt2phys(buf, &msg.data, &buf_mflags);
      if (err != ICS_SUCCESS)
      {
	ICS_EPRINTF(ICS_DBG_MSG, "Failed to convert %s addr %p to paddr : %d\n",
		    ((buf_mflags & ICS_CACHED) ? "CACHED" : "UNCACHED"), buf, err);
	return err;
      }

      ICS_PRINTF(ICS_DBG_MSG, "Converted %s addr %p to paddr 0x%x\n",
		 ((buf_mflags & ICS_CACHED) ? "CACHED" : "UNCACHED"), buf, msg.data);
      
      if (buf_mflags & ICS_CACHED)
	/* Need to flush the local buffer out of our cache */
	/* XXXX Use PURGE here to be L2 safe on SH4 ?*/
	_ICS_OS_CACHE_PURGE(buf, msg.data, size);
    }

    msgSize = offsetof(ics_msg_t, payload);		/* msgSize is just the msg hdr */
  }

  ICS_ASSERT(cpu->state == _ICS_CPU_CONNECTED || cpu->state == _ICS_CPU_MAPPED);
  ICS_ASSERT(cpu->sendChannel != ICS_INVALID_HANDLE_VALUE);
  
  /* Fill out the FIFO message structure */
  msg.srcCpu   = ics_state->cpuNum;
  msg.port     = port;
  msg.mflags   = mflags;	/* Remote buffer memory flags */
  msg.size     = size;
#ifdef __arm__
 _ICS_OS_MUTEX_TAKE(&(((ics_shm_channel_send_t*)(cpu->sendChannel))->chanLock));
#endif
  msg.seqNo    = cpu->txSeqNo;	/* Debugging */


  /* 
   * Send the message via the ICS send channel
   *
   * Hold the mutex across this call to ensure the txSeqNo arrive
   * in order at the target Port
   */
   err = ICS_channel_send_lock(cpu->sendChannel, &msg, msgSize, 0 /* flags */,0);

  /* Only increment seqNo on a successful send */
  if (err == ICS_SUCCESS)
    cpu->txSeqNo++;
#ifdef __arm__
 _ICS_OS_MUTEX_RELEASE(&(((ics_shm_channel_send_t*)(cpu->sendChannel))->chanLock));
#endif 
  if (err != ICS_SUCCESS)
    /* Failed to send to channel */
    return err;

  return ICS_SUCCESS;
}


/*
 * Send a message to the target port.
 *
 * Messages can either be sent inline (mflags & ICS_INLINE) via a copy
 * into the inter cpu FIFOs or via zero copy by simply translating the buf address.
 *
 * For inline message they must fit within a FIFO slot which has the
 * limit ICS_MSG_INLINE_DATA
 *
 * For non inline messages the mflags determine what translation is presented
 * to the receiver, i.e. a CACHED or UNCACHED virtual address
 *
 * For the zero copy path to work, the buffer must have been allocated 
 * from an ICS registered heap/region
 */
ICS_ERROR ICS_msg_send (ICS_PORT port, ICS_VOID *buf, ICS_MEM_FLAGS mflags, ICS_UINT size, ICS_UINT flags)
{
  ICS_ERROR     err = ICS_SUCCESS;

  ICS_MEM_FLAGS validMFlags = (ICS_INLINE|ICS_CACHED|ICS_UNCACHED|ICS_WRITE_BUFFER|ICS_PHYSICAL);

  ICS_UINT      validFlags  = (ICS_MSG_CONNECT);

  int           type, cpuNum, ver, idx;

  ics_cpu_t    *cpu;

  if (ics_state == NULL)
    return ICS_NOT_INITIALIZED;

  ICS_PRINTF(ICS_DBG_MSG, "port 0x%x buf %p size %d mflags 0x%x flags 0x%x\n",
	     port, buf, size, mflags, flags);

  if (mflags == 0 || (mflags & ~validMFlags) || (flags & ~validFlags) || (size && (buf == NULL)))
  {
    err = ICS_INVALID_ARGUMENT;
    goto error;
  }

  /* Decode the target port handle */
  _ICS_DECODE_HDL(port, type, cpuNum, ver, idx);

  /* Check the target port */
  if (((type != _ICS_TYPE_PORT) && (type != _ICS_TYPE_MNGT))  || cpuNum >= _ICS_MAX_CPUS || idx >= _ICS_MAX_PORTS )
  {
    ICS_EPRINTF(ICS_DBG_MSG, "Invalid port 0x%x -> type 0x%x cpuNum %d ver %d idx %d\n",
		port, type, cpuNum, ver, idx);

    err = ICS_HANDLE_INVALID;
    goto error;
  }

  ICS_PRINTF(ICS_DBG_MSG, "port 0x%x -> type 0x%x cpuNum %d ver %d idx %d\n",
	     port, type, cpuNum, ver, idx);

  cpu = &ics_state->cpu[cpuNum];
 
  /* We must be connected before we send a message or else
   * the region tables may not have been exchanged
   */
  /* Check that the target cpu is connected (NB: not holding state lock) */
  cpu = &ics_state->cpu[cpuNum];
  if (cpu->state != _ICS_CPU_CONNECTED)
  {
    err = ICS_NOT_CONNECTED;
    
    if (flags & ICS_MSG_CONNECT)
    {
      /* Connect to CPU if it is currently not connected 
       * This call will also do the region table exchange
       */
      err = ICS_cpu_connect(cpuNum, 0 /* flags */, _ICS_CONNECT_TIMEOUT);
    }
    
    if (err != ICS_SUCCESS)
      goto error;
  }

  /* Protect the cpu table */
#ifndef __arm__
  _ICS_OS_MUTEX_TAKE(&ics_state->lock);
#else
  _ICS_OS_DOWN_READ(&ics_state->semLock);
#endif
  /* Call internal msg send fn (whilst holding the cpu lock) */
  err = _ics_msg_send(port, buf, mflags, size, flags, cpuNum);
  if (err != ICS_SUCCESS)
    goto error_release;
#ifndef __arm__
  _ICS_OS_MUTEX_RELEASE(&ics_state->lock);
#else
  _ICS_OS_UP_READ(&ics_state->semLock);
#endif
  return ICS_SUCCESS;

error_release:
#ifndef __arm__
  _ICS_OS_MUTEX_RELEASE(&ics_state->lock);
#else
  _ICS_OS_UP_READ(&ics_state->semLock);
#endif


error:
  ICS_EPRINTF(ICS_DBG_MSG, "Port 0x%x buf %p size %d mflags 0x%x flags 0x%x : Failed %s (%d)\n",
	      port, buf, size, mflags, flags,
	      ics_err_str(err), err);

  return err;
}

#ifdef __st200__
int ICS_sleep(void)
{
	ICS_PORT mngt = _ICS_HANDLE(_ICS_TYPE_MNGT, 0, 0, 0);
	ICS_UINT fptr;	
	/*  called with interupt disable */
	/*  retrieve host chn pointer (host chan 0) */
	ics_chn_t *chn = &ics_shm_state->shm->chn[0];
	/*  read intentionally, without invalidate cache */
	/*  cache is updated on interruption handling */
/* bz 73452
	ICS_debug_printf("fptr %d bptr %d\n",chn->fifo.fptr, chn->fifo.bptr);
*/
	if (chn->fifo.fptr == chn->fifo.bptr) {
		fptr = chn->fifo.fptr;
		return	(ICS_msg_send(mngt,&fptr,ICS_INLINE,sizeof(ICS_UINT),0));
	} else {
		return ICS_SYSTEM_INTERRUPT;
	}
}
#endif
/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
