/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/**************************************************************
 *
 * File: ics_event.c
 *
 * Description
 *    Routines to allocate and initialise ICS event descs
 *
 **************************************************************/

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */


/*
 * Allocate a new event desc
 *
 * MULTITHREAD SAFE: Called holding the ics state lock
 */
ICS_ERROR ics_event_alloc (ics_event_t **eventp)
{
  ICS_ERROR    err = ICS_SYSTEM_ERROR;
  ics_event_t *event;
  
  ICS_assert(ics_state);
#ifdef __arm__
  spin_lock(&ics_state->eventLock);
#endif
   /* Attempt to grab a desc from the freelist */
  if (list_empty(&ics_state->events))
  {
    /* XXXX Do we need to limit numbers or use a fixed pool size ? */
#ifdef __arm__
  spin_unlock(&ics_state->eventLock);
#endif

    _ICS_OS_ZALLOC(event, sizeof(ics_event_t));

    /* Failed, allocate new event desc from free memory */
    if (event == NULL)
    {
      /* Failed to allocate local memory */
      err = ICS_ENOMEM;
      goto error;
    }
    
    /* Must initialise the linked list member */
    INIT_LIST_HEAD(&event->list);

    /* Initialise the OS blocking event (NB this can be an expensive operation) */
    if (!(_ICS_OS_EVENT_INIT(&event->event)))
    {
      /* Failed for some reason */
      err = ICS_SYSTEM_ERROR;
      goto error;
    }

    /* Keep a running count of how many allocated */
    ics_state->eventCount++;

    ICS_PRINTF(ICS_DBG_MSG, "Allocated new event %p count %d\n",
	       event, ics_state->eventCount);
  }
  else
  {
    /* Remove EVENT from freelist */
    event = list_first_entry(&ics_state->events, ics_event_t, list);
    list_del_init(&event->list);
#ifdef __arm__
  spin_unlock(&ics_state->eventLock);
#endif

  }

  /* Event should still be free */
  ICS_ASSERT(event->state == _ICS_EVENT_FREE);

  /* Event must not be on any lists */
  ICS_ASSERT(list_empty(&event->list));

  /* Event must be idle */
  ICS_ASSERT(_ICS_OS_EVENT_COUNT(&event->event) == 0);

  /* Mark event as being allocated */
  event->state = _ICS_EVENT_ALLOC;

  /* Return the allocated event pointer */
  *eventp = event;

  return ICS_SUCCESS;

error:

  return err;
}

/*
 * Release an event desc back to the free list
 *
 * MULTITHREAD SAFE: Called holding the ics state lock
 */
void ics_event_free (ics_event_t *event)
{
  ICS_ASSERT(ics_state);
  ICS_ASSERT(event);

  ICS_PRINTF(ICS_DBG_MSG, "Free event %p state %d\n", event, event->state);

  /* Event should not be already free */
  ICS_ASSERT(event->state != _ICS_EVENT_FREE);

  /* Event must not be on any lists */
  ICS_ASSERT(list_empty(&event->list));

  /* Event must be idle */
  ICS_ASSERT(_ICS_OS_EVENT_COUNT(&event->event) == 0);

  /* Mark state as being free */
  event->state = _ICS_EVENT_FREE;

  /* Release to head of freelist */
#ifdef __arm__
 spin_lock(&ics_state->eventLock);
#endif
  list_add(&event->list, &ics_state->events);
#ifdef __arm__
  spin_unlock(&ics_state->eventLock);
#endif
  return;
}
#ifdef __arm__
static spinlock_t event_lock;
#endif
ICS_ERROR ics_event_init (void)
{
  /* Initialise events list */
  INIT_LIST_HEAD(&ics_state->events);
#ifdef __arm__
  	spin_lock_init(&ics_state->eventLock);
	spin_lock_init(&event_lock);
#endif
  return ICS_SUCCESS;
}

/*
 * Free off all events (called during ICS_term)
 */
void ics_event_term (void)
{
  ics_event_t *event, *tmp;

  list_for_each_entry_safe(event, tmp, &ics_state->events, list)
  {
    list_del(&event->list);
    
    ICS_ASSERT(_ICS_OS_EVENT_COUNT(&event->event) == 0);

    _ICS_OS_EVENT_DESTROY(&event->event);

    _ICS_OS_FREE(event);
  }
}


/*
 * Block on an event 
 */
ICS_ERROR ics_event_wait (ics_event_t *event, ICS_UINT timeout)
{
  ICS_ERROR err;

  ICS_ASSERT(event);

  ICS_ASSERT(event->state != _ICS_EVENT_FREE);

  /* Blocking wait on event desc - may timeout */
  err = _ICS_OS_EVENT_WAIT(&event->event, timeout, ICS_FALSE);

  return err;
}

/*
 * Test an event for being ready
 */
ICS_ERROR ics_event_test (ics_event_t *event, ICS_BOOL *readyp)
{
  ICS_ASSERT(event);
  ICS_ASSERT(readyp);

  /* Event should not be already free */
  ICS_ASSERT(event->state != _ICS_EVENT_FREE);

  /* Probe the current event state */
  *readyp = _ICS_OS_EVENT_READY(&event->event);

  return ICS_SUCCESS;
}

#ifdef __arm__
void ics_event_lock(void)
{
	spin_lock(&event_lock);

}
void ics_event_unlock(void)
{
	spin_unlock(&event_lock);
}
#endif

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
