/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#if defined(__KERNEL__)

#include <linux/sched.h>

#define _ICS_RT_PRIO(X)	(-(X+20))

/*
 * Default tuneable array.
 * Order must match that of the ICS_Tuneable_t enum
 */
static ICS_UINT _ics_tuneables[ICS_TUNEABLE_MAX] = {
_ICS_OS_TASK_DEFAULT_ADMIN_PRIO,
_ICS_OS_TASK_DEFAULT_NSRV_PRIO,
_ICS_OS_TASK_DEFAULT_STATS_PRIO,
_ICS_OS_TASK_DEFAULT_WATCHDOG_PRIO,
};

ICS_ERROR ICS_ModifyTuneable (ICS_Tuneable_t key, ICS_UINT value)
{
  _ics_tuneables[key] = value;
  return ICS_SUCCESS;
}

ICS_UINT ICS_GetTuneable (ICS_Tuneable_t key)
{
  if (key < 0 || key >= ICS_TUNEABLE_MAX)
    return ICS_INVALID_ARGUMENT;

  return _ics_tuneables[key];
}

static int setPriority (struct task_struct *kt, int policy, struct sched_param *sp)
{
  int res = -1;
  
  res = sched_setscheduler(kt, policy, sp); 	/* GPL only function */

  if (res)
    ICS_EPRINTF(ICS_DBG_INIT, "sched_setscheduler(%p, %d) failed : %d\n",
		kt, sp->sched_priority, res);
  
  return res;
}

/* Set the priority/class of a task (NULL means current) */
int ics_os_set_priority (struct task_struct *kt, ICS_INT prio)
{
  int res = 0;
  
  if (kt == NULL)
    kt = current;

  /* priorities beyond -20 provoke a different scheduling policy */
  if (prio < -20)
  {
    struct sched_param sp;
    
    /* Set the RT task priority (1 .. 99) and RT scheduler class */
    sp.sched_priority = -(prio + 20);
    
    res = setPriority(kt, SCHED_RR, &sp);
  }
  else
  {
    /* Priorities >= -20 we assume its a nice level */
    set_user_nice(kt, prio);
  }
  
  return res;
}

static int ics_task_helper (void *param)
{
  _ICS_OS_TASK_INFO *t = (_ICS_OS_TASK_INFO *) param;

  /* Set the desired task priority/class */
  ics_os_set_priority(t->task, t->priority);
  
  /* Signal that we have started */
  _ICS_OS_EVENT_POST(&t->event);

  /* Run the actual task code */
  t->entry(t->param);

  /* Signal that we are stopping */
  _ICS_OS_EVENT_POST(&t->event);
  
  /* wait for the user to call kthread_stop() */
  set_current_state(TASK_INTERRUPTIBLE);
  while (!kthread_should_stop())
  {
    schedule();
    set_current_state(TASK_INTERRUPTIBLE);
  }

  __set_current_state(TASK_RUNNING);

  return 0;
}

ICS_ERROR ics_os_task_create (void (*entry)(void *), void *param, ICS_INT priority, const ICS_CHAR *name,
			      _ICS_OS_TASK_INFO *t)
{
    uint affinity_mask = ICS_GetAffinity ();

    /* Fill out supplied task info struct */
    t->entry = entry;
    t->param = param;
    t->priority = priority;

    if (!_ICS_OS_EVENT_INIT(&t->event))
      return ICS_SYSTEM_ERROR;

    t->task = kthread_create(ics_task_helper, t, "%s", name);

    if (IS_ERR(t->task))
      return ICS_SYSTEM_ERROR;

    /* check if the name of the task contains MME-Tfm */
    if (!strcmp(name, "MME-Tfm")&&(affinity_mask))
    {
	cpumask_t mask;
        int i;

	cpus_clear(mask);

	for_each_possible_cpu(i) {
	if ((affinity_mask>>i)&0x1) cpu_set(i, mask);
	}

	/* Match found */
	/* Task to be_isolated */
	/* Set this task to run on CPUs */
        if (0 != set_cpus_allowed_ptr(t->task, &mask)) {
		ICS_EPRINTF(ICS_DBG_INIT, "Error: %s failed to set scheduling affinity %d\n", __func__, affinity_mask);
		return ICS_SYSTEM_ERROR;
	}
    }

    /* Now start the task */
    wake_up_process(t->task);

    /* Wait for task to start */
    _ICS_OS_EVENT_WAIT(&t->event, ICS_TIMEOUT_INFINITE, ICS_FALSE);

    return ICS_SUCCESS;
}

ICS_ERROR ics_os_task_destroy (_ICS_OS_TASK_INFO *t)
{
  int res = ICS_SUCCESS;
  
  if (t->task == NULL)
    return ICS_HANDLE_INVALID;

  /* Wait for task to exit */
  _ICS_OS_EVENT_WAIT(&t->event, ICS_TIMEOUT_INFINITE, ICS_FALSE);

  res = kthread_stop(t->task);
  if (res != 0)
  {
    ICS_EPRINTF(ICS_DBG_INIT, "kthread_stop t %p kthread %p(%s) returned %d\n",
		t, t->task, t->task->comm, res);

    res = ICS_SYSTEM_INTERRUPT;
    
    /* FALLTHRU */
  }

  /* Mark as invalid */
  t->task = NULL;

  _ICS_OS_EVENT_DESTROY(&t->event);

  return res;
}

#endif /* __KERNEL__ */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
