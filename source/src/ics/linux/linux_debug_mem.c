/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

#ifdef ICS_DEBUG_MEM

/* 
 * 
 */ 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

/*
 * Simple debug memory allocation wrapper to detect
 * memory leaks from within ICS
 */

static int initialised = 0;

static ICS_ULONG totalSize = 0;

static LIST_HEAD(_ics_debug_mem);
static _ICS_OS_MUTEX ics_debug_mem_lock;

typedef struct debug_mem
{
  struct list_head 	 list;		/* Doubly linked list */
  
  const char		*file;		/* Allocating file */
  const char		*caller;	/* Allocating function */
  int			 line;		/* Line number of function */
  _ICS_OS_TASK		*task;		/* Allocating task */
  
  size_t		 size;		/* Size of allocation */
  size_t		 align;		/* Alignment of allocation (contig only) */

  char			*buf;		/* Actual buffer returned */
} debug_mem_t;

void *_ics_debug_linux_malloc (size_t size, size_t align,int flags, const char *file, const char *caller, int line, _ICS_OS_TASK *task)
{
  debug_mem_t *mem;

  if (!initialised)
    _ics_debug_linux_mem_init();

  /* Allocate desc */
  mem = _ics_os_malloc(sizeof(debug_mem_t));
  if (mem == NULL)
    return NULL;
  
  /* If align is non zero then this is a request for contiguous memory */
  mem->buf = (align ? _ics_os_contig_alloc(size, align,flags) : _ics_os_malloc(size));
  if (mem->buf == NULL)
  {
    _ics_os_free(mem);
    return NULL;
  }

  mem->file   = strrchr(file, '/') ? strrchr(file, '/') + 1 : file; 
  mem->caller = caller;
  mem->line   = line;
  mem->task   = task;
  mem->size   = size;
  mem->align  = align;

  _ICS_OS_MUTEX_TAKE(&ics_debug_mem_lock);
  
  list_add(&mem->list, &_ics_debug_mem);

  totalSize += size;

  _ICS_OS_MUTEX_RELEASE(&ics_debug_mem_lock);

  if (align)
    return (void *) ALIGNUP(&mem->buf[0], align);
  else
    return &mem->buf[0];
  
  /* NOTREACHED */
  return NULL;
}
EXPORT_SYMBOL(_ics_debug_linux_malloc);

void *_ics_debug_linux_zalloc (size_t size, const char *file, const char *caller, int line, _ICS_OS_TASK *task)
{
  void *buf;

  if (!initialised)
    _ics_debug_linux_mem_init();

  buf = _ics_debug_linux_malloc(size, 0/*align*/,0, file, caller, line, task);

  if (buf == NULL)
    return NULL;
  
  _ICS_OS_MEMSET(buf, 0, size);

  return buf;
}
EXPORT_SYMBOL(_ics_debug_linux_zalloc);

void _ics_debug_linux_free (void *buf, const char *file, const char *caller, int line)
{
  debug_mem_t *mem, *tmp;

  if (!initialised)
    _ics_debug_linux_mem_init();

  _ICS_OS_MUTEX_TAKE(&ics_debug_mem_lock);

  list_for_each_entry_safe(mem, tmp, &_ics_debug_mem, list)
  {
    if (buf == mem->buf)
    {
      /* Found a match, remove from the list */
      list_del_init(&mem->list);

      totalSize -= mem->size;

      _ICS_OS_MUTEX_RELEASE(&ics_debug_mem_lock);

      /* Free of real buffer */
      if (mem->align)
	_ics_os_contig_free(buf);
      else
	_ics_os_free(buf);

      /* Now free mem desc */
      _ics_os_free(mem);
      
      return;
    }
  }

  ICS_PRINTF(ICS_DBG, "ERROR: Free buf %p not found caller %s:%s:%d\n", buf, file, caller, line);

  _ICS_OS_MUTEX_RELEASE(&ics_debug_mem_lock);

  return;
}
EXPORT_SYMBOL(_ics_debug_linux_free);

void _ics_debug_linux_mem_init (void)
{
  initialised = 1;

  (void) _ICS_OS_MUTEX_INIT(&ics_debug_mem_lock);
}
EXPORT_SYMBOL(_ics_debug_linux_mem_init);

size_t _ics_debug_linux_mem_total (void)
{
  return totalSize;
}
EXPORT_SYMBOL(_ics_debug_linux_mem_total);

/*
 * Dump out all the entries on the debug mem list
 */
ICS_UINT _ics_debug_linux_mem_dump (void)
{
  debug_mem_t *mem;
  ICS_UINT total;

  if (!initialised)
    _ics_debug_linux_mem_init();

  total = 0;

  _ICS_OS_MUTEX_TAKE(&ics_debug_mem_lock);

  if (!list_empty(&_ics_debug_mem))
  {
    ICS_PRINTF(ICS_DBG, "DUMPING memory descs %p [%p.%p]\n", 
		&_ics_debug_mem,
		_ics_debug_mem.next, _ics_debug_mem.prev);
    
    list_for_each_entry(mem, &_ics_debug_mem, list)
    {
      total++;
      
      ICS_PRINTF(ICS_DBG, "buf %p size %d caller %s:%s:%d task %p\n",
		  mem->buf,
		  mem->size,
		  mem->file,
		  mem->caller,
		  mem->line,
		  mem->task);
    }
    
    ICS_PRINTF(ICS_DBG, "total %d totalSize %ld\n", total, totalSize);
  }

  _ICS_OS_MUTEX_RELEASE(&ics_debug_mem_lock);
  
  return total;
}
EXPORT_SYMBOL(_ics_debug_linux_mem_dump);

#endif /* #ifdef ICS_DEBUG_MEM */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
