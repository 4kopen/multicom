/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 
 

#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#include "_ics_module.h"

#include <linux/proc_fs.h>
#include <linux/ctype.h>

#include <linux/suspend.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#ifndef CONFIG_ARCH_STI
 #include <linux/stm/platform.h>
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

#include <linux/clk-provider.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/spinlock.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/clocksource.h>
#include <linux/clockchips.h>

#include <linux/clk-provider.h>
#include <linux/clk/mvebu.h>
/* bz 63794 */
#include <linux/kern_levels.h>
#include <linux/mailbox_client.h>
#include <linux/mailbox_sti.h>
#include <linux/workqueue.h>
#include <asm/uaccess.h>
#include <linux/remoteproc.h>

#include "bsp/_bsp.h"

struct ics_mbox {
	struct mbox_client client;
	struct mbox_chan *chan;
	int cpu;
	/*  set to 1 , to contain blocking status */
	/*  atomic_add_unless(block, 1, 1), if 0 no add done already blocked else 
	 *  need to block the channel */
	/*  atomic_add_unless(block, 0, -1) */
	atomic_t block;
	struct work_struct unblock;
	atomic_t  use;
};

struct ics_driver_data {
	void __iomem *base;
	struct platform_device *pdev;
	spinlock_t mbx_lock;
	struct ics_mbox *mbx_instance[_ICS_MAX_CPUS];
	struct rproc *rprocs[_ICS_MAX_CPUS];
	/* lock  */
	spinlock_t rproc_lock;
	int suspended[_ICS_MAX_CPUS];
	/*  filename loaded */
	char *filename[_ICS_MAX_CPUS];
	/* register mailbox used for exporting shm address and scanning remote processor address */
	unsigned int nb_bsp_shm;
	struct bsp_mbox_regs bsp_shm[_ICS_MAX_CPUS];
};

static const struct of_device_id ics_of_match[] = {
	{ .compatible = "st,stih416-ics", },
	{ .compatible = "st,stih407-ics", },
	{ .compatible = "st,stih390-ics", },
	{},
};

MODULE_DEVICE_TABLE(of, ics_of_match);

#endif
/* Not yet defined in bpa2.h */
extern void bpa2_memory (struct bpa2_part *part, unsigned long *base, unsigned long *size);

#if defined(__arm__)

extern void slaveResetRegMap (void);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
#else
extern void slaveUnmapRegMap (void);
#endif

#endif /* __arm__ */

static uint  cpu_num;
static ulong cpu_mask;

/*
 * Tuneable parameters and their default values
 */
/* 
 * Set default parameters, flags and logging
 */
#ifndef ICS_DEBUG_TEST_MULTICOM
static int   debug_flags = 0;
static int   debug_chan  = ICS_DBG_STDOUT|ICS_DBG_LOG;
static int   init        = 1;
#else
static int   debug_flags = 0xffef;
static int   debug_chan  = 7;
static int   init        = 0;
#endif /* #ifdef ICS_DEBUG_TEST_MULTICOM */

static int   connect     = 0;
static int   watchdog    = 0;
static int   thread_ics_admin[2] = { 0xFFFFFFFF, 0 };
static int   thread_ics_nsrv[2] = { 0xFFFFFFFF, 0 };
static int   thread_ics_stats[2] = { 0xFFFFFFFF, 0 };
static int   thread_ics_watchdog[2] = { 0xFFFFFFFF, 0 };

/* BZ 49149 & BZ 48077*/
/* bpa2_part set to NULL to use bigphysarea API
 * => BPA2 region "bigphyarea" wihtout ioremap */
/* 
 * Set default parameters, flags and logging
 */
#ifndef ICS_DEBUG_TEST_MULTICOM
static char *bpa2_part   = 0;
#else
static char *bpa2_part   = "bpamc4";
#endif /* #ifdef ICS_DEBUG_TEST_MULTICOM */

#if defined(__KERNEL__) && defined(MODULE)

MODULE_DESCRIPTION("ICS: Inter CPU System");
MODULE_AUTHOR("STMicroelectronics Ltd");
MODULE_LICENSE("GPL");
MODULE_VERSION(__ICS_VERSION__);

module_param(cpu_num, uint, 0);
MODULE_PARM_DESC(cpu_num, "ICS CPU number");

module_param(cpu_mask, ulong, 0);
MODULE_PARM_DESC(cpu_mask, "Bitmask of ICS CPU configuration");

module_param(debug_flags, int, 0);
MODULE_PARM_DESC(debug_flags, "Bitmask of ICS debugging flags");

module_param(debug_chan, int, 0);
MODULE_PARM_DESC(debug_chan, "Bitmask of ICS debugging channels");

module_param(init, int, S_IRUGO);
MODULE_PARM_DESC(init, "Do not initialize ICS if set to 0");

module_param(connect, int, S_IRUGO);
MODULE_PARM_DESC (connect, "Connect to all CPUs on init");

module_param(watchdog, int, S_IRUGO);
MODULE_PARM_DESC (watchdog, "Enable CPU watchdog handlers");

module_param(bpa2_part, charp, S_IRUGO);
MODULE_PARM_DESC (bpa2_part, "Set BPA2 partition name for ICS contig memory allocations");

module_param_array_named(thread_ICS_Admin, thread_ics_admin, int, NULL, 0644);
MODULE_PARM_DESC(thread_ICS_Admin, "ICS-Admin thread:s(Mode),p(Priority)");

module_param_array_named(thread_ICS_Nsrv, thread_ics_nsrv, int, NULL, 0644);
MODULE_PARM_DESC(thread_ICS_Nsrv, "ICS-Nsrv thread:s(Mode),p(Priority)");

module_param_array_named(thread_ICS_Stats, thread_ics_stats, int, NULL, 0644);
MODULE_PARM_DESC(thread_ICS_Stats, "ICS-Stats thread:s(Mode),p(Priority)");

module_param_array_named(thread_ICS_Watchdog, thread_ics_watchdog, int, NULL, 0644);
MODULE_PARM_DESC(thread_ICS_Watchdog, "ICS-Watchdog thread:s(Mode),p(Priority)");

/*
 * Support for declaring ICS regions on module load
 * The syntax is
 *    
 *     regions = region[,region]
 *     region = <bpa2name>:<cacheflag>|<phys base>:<size>:<cacheflag>
 * 
 * Where bpa2name is the ASCII name of the bpa2 partition e.g. LMI_VID or LMI_SYS
 * Otherwise the physical base address and size of a region can be specified directly
 * cacheflag: can be 1/0 for CACHED / UNCACHED region mapping.
 *
 * Once configured these regions will be mapped into all CPUs as CACHED / UNCACHED 
 * translations
 */
static char *     regions = NULL;
module_param     (regions, charp, S_IRUGO);
MODULE_PARM_DESC (regions, "Memory region string of the form <bpa2name>:<cacheflag>|<base>:<size>:<cacheflag>[,<bpa2name>:<cacheflag>|<base>:<size>:<cacheflag>]");

/*
 * Support for declaring ICS companion firmware on module load
 * The syntax is
 *
 *      firmware = companion[,companion]
 *      companion = <cpu>:<filename>
 *
 *   where <cpu> can either be the logical cpu number (integer)
 *   or the cpu core name e.g. audio0 or video0
 
 *   <filename> must be an ASCII filename of an ELF file located in /lib/firmware
 */

static char *     firmware = NULL;
module_param     (firmware, charp, S_IRUGO);
MODULE_PARM_DESC (firmware, "Coprocessor firmware string in the form <cpu>:<filename>[,<cpu>:<filename>]");


/*
 * Export all the ICS module symbols
 */
EXPORT_SYMBOL(ics_cpu_init);

EXPORT_SYMBOL(ics_cpu_version);
EXPORT_SYMBOL(ics_cpu_self);
EXPORT_SYMBOL(ics_cpu_name);
EXPORT_SYMBOL(ics_cpu_type);
EXPORT_SYMBOL(ics_cpu_mask);
EXPORT_SYMBOL(ics_cpu_index);
EXPORT_SYMBOL(ics_cpu_lookup);

EXPORT_SYMBOL(ics_cpu_start);
EXPORT_SYMBOL(ics_cpu_start_fw);
EXPORT_SYMBOL(ics_cpu_reset);
EXPORT_SYMBOL(ics_cpu_reset_all);

EXPORT_SYMBOL(ics_err_str);
EXPORT_SYMBOL(ics_debug_flags);
EXPORT_SYMBOL(ics_debug_chan);
EXPORT_SYMBOL(ICS_debug_dump);
EXPORT_SYMBOL(ICS_debug_copy);

EXPORT_SYMBOL(ics_heap_create);
EXPORT_SYMBOL(ics_heap_destroy);
EXPORT_SYMBOL(ics_heap_pbase);
EXPORT_SYMBOL(ics_heap_base);
EXPORT_SYMBOL(ics_heap_size);
EXPORT_SYMBOL(ics_heap_alloc);
EXPORT_SYMBOL(ics_heap_free);

EXPORT_SYMBOL(ics_load_elf_file);
EXPORT_SYMBOL(ics_load_elf_image);

EXPORT_SYMBOL(ICS_channel_alloc);
EXPORT_SYMBOL(ICS_channel_free);
EXPORT_SYMBOL(ICS_channel_open);
EXPORT_SYMBOL(ICS_channel_close);
EXPORT_SYMBOL(ICS_channel_send);
EXPORT_SYMBOL(ICS_channel_recv);
EXPORT_SYMBOL(ICS_channel_release);
EXPORT_SYMBOL(ICS_channel_release_locked);
EXPORT_SYMBOL(ICS_channel_unblock);

EXPORT_SYMBOL(ICS_cpu_init);
EXPORT_SYMBOL(ICS_cpu_info);
EXPORT_SYMBOL(ICS_cpu_connect);
EXPORT_SYMBOL(ICS_cpu_disconnect);
EXPORT_SYMBOL(ICS_cpu_term);
EXPORT_SYMBOL(ICS_cpu_version);

EXPORT_SYMBOL(ICS_dyn_load_image);
EXPORT_SYMBOL(ICS_dyn_load_file);
EXPORT_SYMBOL(ICS_dyn_unload);

EXPORT_SYMBOL(ICS_msg_send);
EXPORT_SYMBOL(ICS_msg_recv);
EXPORT_SYMBOL(ICS_msg_post);
EXPORT_SYMBOL(ICS_msg_test);
EXPORT_SYMBOL(ICS_msg_wait);
EXPORT_SYMBOL(ICS_msg_cancel);

EXPORT_SYMBOL(ICS_nsrv_add);
EXPORT_SYMBOL(ICS_nsrv_remove);
EXPORT_SYMBOL(ICS_nsrv_lookup);

EXPORT_SYMBOL(ICS_port_alloc);
EXPORT_SYMBOL(ICS_port_free);
EXPORT_SYMBOL(ICS_port_lookup);
EXPORT_SYMBOL(ICS_port_cpu);

EXPORT_SYMBOL(ICS_region_add);
EXPORT_SYMBOL(ICS_region_remove);
EXPORT_SYMBOL(ICS_region_virt2phys);
EXPORT_SYMBOL(ICS_region_phys2virt);
EXPORT_SYMBOL(ICS_region_phys2virt_mflags);
EXPORT_SYMBOL(ics_region_mappeable);
EXPORT_SYMBOL(ICS_stats_add);
EXPORT_SYMBOL(ICS_stats_remove);
EXPORT_SYMBOL(ICS_stats_update);
EXPORT_SYMBOL(ICS_stats_sample);

EXPORT_SYMBOL(ICS_watchdog_add);
EXPORT_SYMBOL(ICS_watchdog_remove);
EXPORT_SYMBOL(ICS_watchdog_reprime);


/*
 * Internal APIs used by MME 
 */
EXPORT_SYMBOL(ICS_debug_printf);	/* Used in os_abstraction.h */
EXPORT_SYMBOL(ics_debug_printf);
EXPORT_SYMBOL(_ics_debug_flags);	/* referenced in ics_mq.h */

EXPORT_SYMBOL(ics_os_virt2phys);
EXPORT_SYMBOL(ics_os_task_create);
EXPORT_SYMBOL(ics_os_task_destroy);

#ifdef ICS_DEBUG_MEM
EXPORT_SYMBOL(_ics_debug_malloc);	/* Needed by MME when debugging malloc */
EXPORT_SYMBOL(_ics_debug_zalloc);
EXPORT_SYMBOL(_ics_debug_free);
#endif

/*
 * Internal APIs used by EMBX mailbox
 */
EXPORT_SYMBOL(ics_mailbox_init);
EXPORT_SYMBOL(ics_mailbox_term);
EXPORT_SYMBOL(ics_mailbox_interrupt_clear);
EXPORT_SYMBOL(ics_mailbox_interrupt_disable);
EXPORT_SYMBOL(ics_mailbox_interrupt_enable);
EXPORT_SYMBOL(ics_mailbox_interrupt_raise);
EXPORT_SYMBOL(ics_mailbox_update_interrupt_handler);
EXPORT_SYMBOL(ics_mailbox_status_get);
EXPORT_SYMBOL(ics_mailbox_status_set);
EXPORT_SYMBOL(ics_mailbox_status_mask);
EXPORT_SYMBOL(ics_mailbox_register);
EXPORT_SYMBOL(ics_mailbox_deregister);
EXPORT_SYMBOL(ics_mailbox_alloc);
EXPORT_SYMBOL(ics_mailbox_free);
EXPORT_SYMBOL(ics_mailbox_find);

EXPORT_SYMBOL(ICS_SetAffinity);
EXPORT_SYMBOL(ICS_GetAffinity);

static struct {
	ICS_ULONG  base;
	ICS_ULONG  size;
	ICS_REGION rgnCached;
	ICS_REGION rgnUncached;
	ICS_ULONG cacheflag;
} region[_ICS_MAX_REGIONS];

static ICS_UINT numRegions;

static struct {
	ICS_UINT    cpuNum;
	const char *filename;
} load[_ICS_MAX_CPUS];

static ICS_UINT numCpus;

static int addRegion(void)
{
	ICS_ERROR err = ICS_SUCCESS;
	ICS_REGION *rgn_p = NULL;
	ICS_MEM_FLAGS mflags = 0;
	bool add_region = false;

	int i;
	for (i = 0; i < numRegions; i++) {
		/*if region size is zero, no need to add this region*/
		if (region[i].size == 0)
			continue;

		printk(KERN_INFO "ICS Adding region[%d] 0x%lx 0x%lx cacheflag=0x%x\n",
				i,
				region[i].base,
				region[i].size,
				(ICS_UINT)region[i].cacheflag);

		/* Changes here since, unlike sh, arm support either
		 * cahced or uncached maping for same physical location.*/
		/* Add a cached translation */
		if((region[i].cacheflag == 2) || (region[i].cacheflag == 1)) {
			rgn_p = &region[i].rgnCached;
			mflags = ICS_CACHED;
			add_region = true;
		}

		/* Add an uncached translation */
		if((region[i].cacheflag == 2) || (region[i].cacheflag == 0)) {
			rgn_p = &region[i].rgnUncached;
			mflags = ICS_UNCACHED;
			add_region = true;
		}

		if(add_region) {
			err = ICS_region_add(NULL,
					region[i].base,
					region[i].size,
					mflags,
					ics_cpu_mask(),
					rgn_p);
        }

		/*reset the flag*/
		add_region = false;

		if (err != ICS_SUCCESS) {
			printk(KERN_ERR "ICS_region_add(0x%lx, %ld) failed : %s (%d)\n",
					region[i].base,
					region[i].size,
					ics_err_str(err),
					err);
			goto error;
		}
	}

	return 0;

error:
	ICS_cpu_term(0);
	return -ENODEV;
}


static int parseNumber (const char *str, ICS_ULONG *num)
{
	int   base;
	char *endp;

	/* lazy evaluation will prevent us illegally accessing str[1] */
	base = (str[0] == '0' && str[1] == 'x' ? str += 2, 16 : 10);
	*num = simple_strtoul(str, &endp, base);

	return ('\0' != *str && '\0' == *endp ? 0 : -EINVAL);
}

/* Parse a comma separated list of region declarations
   
 * Each element can be of the form <bpa2name>:<cacheflag>|<base>:<size>:<cacheflag>
 *
 */
static int parseRegionString (char *str)
{
	char *reg;
	
	if (!str || !*str)
		return -EINVAL;
	
	while ((reg = strsep(&str, ",")) != NULL) {
		ICS_OFFSET base;
		ICS_ULONG  size;
		ICS_ULONG  cacheflag=0xCF;

		char *token;

		/* Attempt to parse as <base>:<size>:<cacheflag> */
		token = strsep(&reg, ":");
		if (!token || !*token) {
			return -EINVAL;
		}
		
		/* Only call memparse if we have a numeric value to parse.
		 * Otherwise if bpa2name begins with a value modifier
		 * (g,G,m,M,k,K) it will be incorrectly consumed
		 * (i.e. Consider gfx-memory)
		 */
		if (isdigit( *token ))
			base = memparse(token, &token);

		if (!*token) {
			/* Success, now parse size */
			token = strsep(&reg, ":");
			if (!token || !*token) 
				/* No size parameter */
				return -EINVAL;

			size = memparse(token, &token);
			if (*token)
				/* Invalid size parameter */
				return -EINVAL;
		}
		else {
			/* Assume its a bpa2name */
			struct bpa2_part *part;
			
			/* Lookup the BPA2 partition */
			part = bpa2_find_part(token);
			if (part == NULL) {
				printk(KERN_ERR "Failed to find BPA2 region '%s'\n", token);
				return -EINVAL;
			}
			
			/* Query the base and size of partition */
			bpa2_memory(part, &base, &size);
			
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 32)
			/* Convert to a physical address */
			base = virt_to_phys((void *)base);
#endif
		}
		
#if 1
		printk(KERN_DEBUG "region[%d] base 0x%lx size 0x%lx\n",
		       numRegions, base, size);
#endif

		/* Now parse cacheflag */
			token = strsep(&reg, ":");
			if (!token || !*token) {
     	/* No cache flag parameter */
#if defined(__sh__)
      cacheflag = 2;   /* Set defaults to cached and uncahced */
#elif defined(__arm__)
     cacheflag  = 1;   /* Set defaults to cached */
#endif 	/* __sh__ */		
   }
   else { 
     /* read the user supplied cache flag */
     if (isdigit( *token ))
			    cacheflag = memparse(token, &token);
   } 

#if 1
		printk(KERN_DEBUG "region[%d] base 0x%lx size 0x%lx, cacheflag=%ld\n",
		       numRegions, base, size, cacheflag);
#endif
		/* Fill out region table */
		region[numRegions].base = base;
		region[numRegions].size = size;
		region[numRegions].cacheflag = cacheflag;
		
		numRegions++;
	}

	return 0;
}

/* Parse a comma separated list of firmware files 
 *
 * Each element can be of the form <cpu>:<filename>
 */

static int parseFirmwareString (char *str)
{
	char *firm;
        int res;

	if (!str || !*str)
		return -EINVAL;
	
	while ((firm = strsep(&str, ",")) != NULL) {
		char *token;
		
		ICS_ULONG cpuNum;
		
		token = strsep(&firm, ":");
		if (!token || !*token) {
			/* No cpu specified */
			return -EINVAL;
		}
		
		/* Parse the cpu as a number */
		if (parseNumber(token, &cpuNum)) {
			/* Failed: Assume its a cpuName instead */
			res = ics_cpu_lookup(token);
			if (res < 0)
			{
			  return -EINVAL;
			}
			else
			{
			  cpuNum = (ICS_ULONG)res;
			}
		}

		/* Don't allow cpuNum == 0 (i.e. us!) */
		if (cpuNum == 0)
			return -EINVAL;
		
	        token = strsep(&firm, ":");
		if (!token || !*token) {
			/* No filename given */
			return -EINVAL;
		}
		
		load[numCpus].cpuNum = cpuNum;
		load[numCpus].filename = token;

#if 1
		printk(KERN_DEBUG "load[%d] cpuNum %d (%s) filename '%s'\n",
		       numCpus, load[numCpus].cpuNum, ics_cpu_name(load[numCpus].cpuNum), load[numCpus].filename);
#endif

		numCpus++;
	}
	
	return 0;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

static 
struct rproc *ics_cpunum_to_rproc(int coproc_num)
{
	struct ics_driver_data *p;
	struct rproc *rproc;
	p = platform_get_drvdata(ics_pdev);
	if (!p)
		return ERR_PTR(-EINVAL);

	if ((coproc_num < 0)  || coproc_num >= (_ICS_MAX_CPUS-1))
		return ERR_PTR(-EINVAL);

	rproc = p->rprocs[coproc_num];

	return (p->rprocs[coproc_num] ?
		p->rprocs[coproc_num] : ERR_PTR(-EINVAL));
}

ICS_ERROR ics_cpu_start(ICS_OFFSET entryAddr, ICS_UINT cpuNum, ICS_UINT flags)
{
	return -EINVAL;
}

ICS_ERROR ics_cpu_start_fw(const char	*fname, ICS_UINT cpuNum)
{
	struct ics_driver_data *p;
	int coproc_num = cpuNum -1;
	struct rproc *rproc;
	int ret, len;
	char *tmp;
	if ((coproc_num < 0)  || coproc_num >= (_ICS_MAX_CPUS-1)) {
	dev_err(&ics_pdev->dev, "cpuNum %d is not valid\n",
			cpuNum);
	return ICS_INVALID_ARGUMENT;
	}

	p = platform_get_drvdata(ics_pdev);
	if (!p)
		return ICS_INVALID_ARGUMENT;

	rproc = ics_cpunum_to_rproc(coproc_num);
	if (IS_ERR(rproc))
		return ICS_INVALID_ARGUMENT;
	if (p->filename[coproc_num]) {
		dev_warn(&ics_pdev->dev, "cpuNum %d running reload without reset\n", cpuNum);
	}

	printk(KERN_DEBUG "ics_cpu_start_fw() start : cpuNum : %d coproc_num : %d\n", cpuNum, coproc_num);

        /* + Ticket bz 72352 -PIE: when ics module is unloaded, coproc should be stopped */
        rproc_shutdown(rproc);
        /* - Ticket bz 72352 -PIE: when ics module is unloaded, coproc should be stopped */ 

	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc_shutdown(rproc=%p)\n", rproc);

	len = strlen(fname);
	if (!len) {
	dev_err(&ics_pdev->dev,"fname null\n");
	return ICS_INVALID_ARGUMENT;
	}

	tmp = devm_kzalloc(&ics_pdev->dev, len+1, GFP_KERNEL);
	if (!tmp) {
	dev_err(&ics_pdev->dev,"alloc failed\n");
	return ICS_SYSTEM_ERROR;
	}
	strncpy(tmp, fname,len+1);
	spin_lock(&p->rproc_lock);
	if (!p->filename[coproc_num]) {
	p->filename[coproc_num]=tmp;
	p->suspended[coproc_num]=0;
	spin_unlock(&p->rproc_lock);
	} else {
	spin_unlock(&p->rproc_lock);
	dev_warn(&ics_pdev->dev,"filename rproc race %d\n", cpuNum);
	}
	ret = rproc_set_fw_name(rproc, p->filename[coproc_num]);

	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc_set_fw_name(rproc, p->filename[coproc_num]) / ret = %d\n", ret);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc      = %p\n", rproc);
	printk(KERN_DEBUG "ics_cpu_start_fw() : coproc_num = %d\n", coproc_num);

	if (p->filename[coproc_num])
	{
	printk(KERN_DEBUG "ics_cpu_start_fw() : p->filename[coproc_num]  = %s\n", p->filename[coproc_num]);
	}
	else
	{
	printk(KERN_DEBUG "ics_cpu_start_fw() : p->filename[coproc_num]  = NULL\n");
	}

	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->name              = %s\n", rproc->name );
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->firmware          = %s\n", rproc->firmware );
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->orig_firmware     = %s\n", rproc->orig_firmware);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->state             = %d\n", rproc->state);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->num_traces        = %d\n", rproc->num_traces);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->bootaddr          = %x\n", rproc->bootaddr);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->index             = %d\n", rproc->index);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->crash_cnt         = %d\n", rproc->crash_cnt);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->recovery_disabled = %d\n", rproc->recovery_disabled);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->max_notifyid      = %d\n", rproc->max_notifyid);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->table_csum        = %d\n", rproc->table_csum);
	printk(KERN_DEBUG "ics_cpu_start_fw() : rproc->fw_relocate       = %d\n", rproc->fw_relocate);

	if (ret) {
/* Bug 71784 - PIE: wrong firmware name in error message */	  
		dev_err(&ics_pdev->dev, "rproc:%s set fw:%s failed %d\n",
				rproc->name, load[coproc_num].filename, ret);
		return ICS_SYSTEM_ERROR;
	}
	ret= rproc_boot(rproc);
	if (ret) {
/* Bug 71784 - PIE: wrong firmware name in error message */	  
		dev_err(&ics_pdev->dev, "rproc:%s boot fw:%s failed %d\n",
				rproc->name, p->filename[coproc_num], ret);
		return ICS_SYSTEM_ERROR;
	}

    printk(KERN_INFO "ics_cpu_start_fw() end\n");
	
    return ICS_SUCCESS;
}

ICS_ERROR ics_cpu_reset(ICS_UINT cpuNum, ICS_UINT flags)
{
	struct ics_driver_data *p;
	struct rproc *rproc;
	int coproc_num = cpuNum -1;
	char *tmp;

	printk(KERN_INFO "ics_cpu_reset() start : cpuNum : %d coproc_num : %d, flags : 0x%x\n", cpuNum, coproc_num, flags);

	if ((coproc_num < 0)  || coproc_num >= (_ICS_MAX_CPUS-1)) {
		dev_err(&ics_pdev->dev, "cpuNum %d is not valid\n",
				cpuNum);
		return ICS_INVALID_ARGUMENT;
	}
	p = platform_get_drvdata(ics_pdev);
	if (!p)
		return ICS_INVALID_ARGUMENT;
	rproc = ics_cpunum_to_rproc(coproc_num);
	if (IS_ERR(rproc))
		return ICS_INVALID_ARGUMENT;

	spin_lock(&p->rproc_lock);
	if (p->filename[coproc_num]) {
		tmp = p->filename[coproc_num];
		p->suspended[coproc_num] = 0;
		p->filename[coproc_num] = (char*)NULL;
		spin_unlock(&p->rproc_lock);

        printk(KERN_INFO "ics_cpu_reset : call rproc_shutdown(coproc_num : %d)\n", coproc_num);

		rproc_shutdown(rproc);
		devm_kfree(&ics_pdev->dev, tmp);
	} else
	{
		spin_unlock(&p->rproc_lock);
	}

	printk(KERN_INFO "ics_cpu_reset() end\n");

	return ICS_SUCCESS;
}

void ics_cpu_reset_all(void)
{
	int i;

	for(i = 0; i < _ICS_MAX_CPUS; i++)
		ics_cpu_reset(i, 0);
}

#endif /* #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) */

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

static int load_firmware (void)
{
	ICS_ERROR err;
	int cpu;

	for (cpu = 0; cpu < numCpus; cpu++) {

		assert(load[cpu].filename);
/* bz 63794 */
		printk(KERN_INFO "ICS loading firmware '%s' cpu %d (%s)\n",
		       load[cpu].filename, load[cpu].cpuNum, ics_cpu_name(load[cpu].cpuNum));

		err = ics_cpu_start_fw(load[cpu].filename, load[cpu].cpuNum);
		if (err != ICS_SUCCESS) {
			printk(KERN_ERR "Failed to start cpu %d firmware %s : %s (%d)\n",
					load[cpu].cpuNum,load[cpu].filename,
					ics_err_str(err), err);
			return -EINVAL;
                 }
	}

	return 0;
}

#else /* #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) */

static int load_firmware (void)
{
	ICS_ERROR err;
	int cpu;

	for (cpu = 0; cpu < numCpus; cpu++) {
		ICS_OFFSET entryAddr;

		assert(load[cpu].filename);
/* bz 63794 */
		printk(KERN_INFO "ICS loading firmware '%s' cpu %d (%s)\n",
		       load[cpu].filename, load[cpu].cpuNum, ics_cpu_name(load[cpu].cpuNum));

#if defined(__arm__)
  /*  Map the reset registers physical addresses into (uncached) kernel space */
  slaveResetRegMap();
#endif /* __arm__ */
	
		/* Load the ELF image */
		err = ics_load_elf_file(load[cpu].filename, 0, &entryAddr);
		if (err != ICS_SUCCESS) {
			printk(KERN_ERR "Failed to load '%s' : %s (%d)\n",
			       load[cpu].filename,
			       ics_err_str(err), err);
			return -EINVAL;
		}

		printk(KERN_INFO "ICS starting cpu %d (%s) entry 0x%lx\n",
		       load[cpu].cpuNum, ics_cpu_name(load[cpu].cpuNum), entryAddr);

		/* Start the loaded code on the specified cpu */
		err = ics_cpu_start(entryAddr, load[cpu].cpuNum, 0);
		if (err != ICS_SUCCESS) {
			printk(KERN_ERR "Failed to start cpu %d entry 0x%lx : %s (%d)\n",
			       load[cpu].cpuNum, entryAddr,
			       ics_err_str(err), err);
			return -EINVAL;
         }
	}
	
	return 0;
}

#endif /* #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) */

static void ics_tune (int key, int *param)
{
	if (param[0] != 0xFFFFFFFF) {
		if (param[0] & 0x2)
			ICS_ModifyTuneable(key, _ICS_RT_PRIO(param[1]));
		else
			ICS_ModifyTuneable(key, param[1]);
	}
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

void enable_slave_clock(int cpu_num)
{
	struct ics_driver_data *p;
	int coproc_num = cpu_num - 1, err;
	struct rproc *rproc;

	BUG_ON(ics_pdev == NULL);
	if (coproc_num < 0  || coproc_num >=  (_ICS_MAX_CPUS-1))
		return;

	p = platform_get_drvdata(ics_pdev);
	if (!p)
		return;

	if(!p->filename[coproc_num]) {
		dev_err(&ics_pdev->dev, "coproc not started");
		return;
	}
	spin_lock(&p->rproc_lock);
	if (p->suspended[coproc_num]) {	
		p->suspended[coproc_num] -= 1;
		spin_unlock(&p->rproc_lock);
		rproc = p->rprocs[coproc_num];
		if (rproc == NULL) {
			dev_err(&ics_pdev->dev, "no coproc");
			return;
		}
		err = rproc_resume(rproc);
		if (err)
			dev_err(&ics_pdev->dev, "rproc_resume : failed");
	} else
		spin_unlock(&p->rproc_lock);
}

void disable_slave_clock(int cpu_num)
{
	struct ics_driver_data *p;
	int coproc_num = cpu_num - 1, err;
	struct rproc *rproc;

	BUG_ON(ics_pdev == NULL);
	if (coproc_num < 0  || coproc_num >= (_ICS_MAX_CPUS-1))
		return;

	p = platform_get_drvdata(ics_pdev);
	if (!p)
		return;

	rproc = p->rprocs[coproc_num];
	if (rproc == NULL) {
		dev_err(&ics_pdev->dev, "no coproc");
		return;
	}
	if(!p->filename[coproc_num]) {
		dev_err(&ics_pdev->dev, "coproc not started");
		return;
	}

	spin_lock(&p->rproc_lock);
	if (!p->suspended[coproc_num]) {
		p->suspended[coproc_num] += 1;
		spin_unlock(&p->rproc_lock);
		rproc = p->rprocs[coproc_num];
		if (rproc == NULL) {
		dev_err(&ics_pdev->dev, "no coproc");
		return;
	}
	
    err = rproc_suspend(rproc);
	if (err)
	dev_err(&ics_pdev->dev, "rproc_suspend : failed");

	} else
		spin_unlock(&p->rproc_lock);
}


#else
void enable_slave_clock(int cpu_num)
{
	return;
}
void disable_slave_clock(int cpu_num)
{
	return;
}
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

void ics_mbox_init(struct platform_device *pdev)
{
	struct ics_driver_data *p= platform_get_drvdata(pdev);
	if (p)
		spin_lock_init(&p->mbx_lock);
	else 
		printk(KERN_DEBUG "ics_mbox_init failed\n");
}

extern  int ics_shm_service_channel (int idx);
void ics_mbox_callback(struct mbox_client *cl, void *data)
{
	struct ics_mbox *instance = container_of(cl,struct ics_mbox,client);
		/*  scheduled if not blocked */
		if (!atomic_read(&instance->block))
		ics_shm_service_channel(instance->cpu);
}
void ics_mbox_unblock_cb(struct work_struct *unblock)
{
	struct ics_mbox *instance = container_of(unblock,struct ics_mbox,unblock);
	ics_shm_service_channel(instance->cpu);
}

/*  raise event on a mailbox */
int ics_mbox_kick(int cpu)
{
	struct ics_driver_data *p;
	BUG_ON(ics_pdev == NULL);
	if ((p = platform_get_drvdata(ics_pdev))&& (cpu < _ICS_MAX_CPUS)) {
		struct sti_mbox_msg msg;
		struct ics_mbox *inst;
		spin_lock(&p->mbx_lock);
		inst = p->mbx_instance[cpu];
		if (inst)
			atomic_inc(&inst->use);
		spin_unlock(&p->mbx_lock);
		msg.dsize = 0;
		msg.pdata = (u8*)0;
		if ((inst) && (inst->chan))
			mbox_send_message(inst->chan, (void*)&msg);
		else
			printk(KERN_DEBUG "no client registered for cpu %d\n", cpu );
		if (inst)
		 atomic_dec(&inst->use);
	}
	return 0;
}

int ics_mbox_block(int cpu)
{
	int ret=0;
	struct ics_driver_data *p;
	BUG_ON(ics_pdev == NULL);
	if ((p = platform_get_drvdata(ics_pdev))&& (cpu < _ICS_MAX_CPUS)) {
	struct ics_mbox *inst;
		spin_lock(&p->mbx_lock);
		inst = p->mbx_instance[cpu];
		ret = atomic_add_unless(&inst->block, 1, 1);
		spin_unlock(&p->mbx_lock);
	}
	printk(KERN_INFO "ics block %d\n",ret);
	return ret;
}
int ics_mbox_unblock(int cpu)
{
	struct ics_driver_data *p;
	BUG_ON(ics_pdev == NULL);
	if ((p = platform_get_drvdata(ics_pdev))&& (cpu < _ICS_MAX_CPUS)) {
		struct ics_mbox *inst;
		spin_lock(&p->mbx_lock);
		inst = p->mbx_instance[cpu];
		if (inst) {
			atomic_inc(&inst->use);
			atomic_set(&inst->block, 0);
		}
		spin_unlock(&p->mbx_lock);
		if ((inst) && (inst->chan))
			schedule_work(&inst->unblock);
		else
			printk(KERN_INFO "ics block: no client registered for this CPU: %d\n", cpu);
		atomic_dec(&inst->use);
	}
	return 0;
}

/* alloc a mailbox, this will enable mailbox interruption  */
int ics_mailbox_get(int cpu)
{
	struct ics_driver_data *p;
	BUG_ON(ics_pdev == NULL);
	printk(KERN_INFO "get mailbox %d\n",cpu);
	if ((p = platform_get_drvdata(ics_pdev))&& (cpu < _ICS_MAX_CPUS)) {
		struct ics_mbox *inst;
		struct mbox_client *mbox_user;
		spin_lock(&p->mbx_lock);
		inst = p->mbx_instance[cpu];
		if (!inst) {
			spin_unlock(&p->mbx_lock);
			inst = (struct ics_mbox *) kzalloc(sizeof(struct ics_mbox), GFP_KERNEL);
			mbox_user=&inst->client;
			mbox_user->dev = &ics_pdev->dev;
			mbox_user->tx_done = NULL;
			mbox_user->rx_callback = ics_mbox_callback;
			mbox_user->tx_block = false;
			mbox_user->tx_tout = 1000;
			mbox_user->knows_txdone = false;
			inst->cpu = cpu;
			inst->chan = mbox_request_channel(mbox_user,cpu);
			INIT_WORK(&inst->unblock, ics_mbox_unblock_cb);
			if (IS_ERR(inst->chan)) {
				kfree(inst);
				printk(KERN_DEBUG "cannot allocate mbx for cpu %d\n",cpu);
				return -ENOMEM;
			}
			spin_lock(&p->mbx_lock);
			if (!p->mbx_instance[cpu])
				p->mbx_instance[cpu]=inst;
			spin_unlock(&p->mbx_lock);
			if (p->mbx_instance[cpu]!=inst)
				goto failed;
			return 0;
		} else {
			spin_unlock(&p->mbx_lock);
failed:
			printk(KERN_DEBUG "ics_mailbox_get instance %d already get!\n", cpu);
		}

	} else
		printk(KERN_DEBUG "ics, no dev registered\n");
	return 0;
}
/* release a mailbox */
int ics_mailbox_release(int cpu)
{
	struct ics_driver_data *p;
	BUG_ON(ics_pdev == NULL);
	if ((p = platform_get_drvdata(ics_pdev))&& (cpu < _ICS_MAX_CPUS)) {
		struct ics_mbox *inst;
		int use = 0;
		spin_lock(&p->mbx_lock);
		inst = p->mbx_instance[cpu];
		if ((inst)) {
			use = atomic_read(&inst->use);
			if (!use)
			p->mbx_instance[cpu] = (struct ics_mbox *)NULL;
		}
		spin_unlock(&p->mbx_lock);
		if ((inst) && (!use) && (inst->chan)) {
			/*  release  */
			int ret = work_busy(&inst->unblock);
			if (ret)
				cancel_work_sync(&inst->unblock);
			mbox_free_channel(inst->chan);
			kfree(inst);
		}
	}
	/* bz 64304  */
	return 0;
}

static int ics_parse_dt(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	const struct of_device_id *match;
	struct ics_driver_data *p;
	int i;

	match = of_match_device((ics_of_match), dev);
	if (!match) {
		dev_err(dev, "No device match found\n");
		return -ENODEV;
	}

	p = devm_kzalloc(dev, sizeof(*p), GFP_KERNEL);

	if (!p)
		return -ENOMEM;

	p->pdev = pdev;
	platform_set_drvdata(pdev, p);

	for(i = 0; i < _ICS_MAX_CPUS; i++) {
		p->rprocs[i] = of_rproc_byindex(np, i);
		if (IS_ERR(p->rprocs[i]))
			p->rprocs[i] = NULL;
	}

	return 0;
}

struct bsp_mbox_regs *modify_bsp(struct bsp_mbox_regs *bsp_mbx, unsigned int *nb)
{
	struct ics_driver_data *p;
	unsigned int *shm;
	int ret=0;
	int size;
	int i;
	const __be32 *list;
	struct device_node *np;

        p = platform_get_drvdata(ics_pdev);

	if (p != NULL){
		np=ics_pdev->dev.of_node;
		list = of_get_property(np, "shm", &size);
		if ((!list)|| (size==0)) {
			dev_info(&ics_pdev->dev, "no shm in ics kernel DT\n");
			goto use_bsp;
		}
		size /=sizeof(*list);
		if (size > _ICS_MAX_CPUS) {
			dev_err(&ics_pdev->dev,"number of bsp_info > _ICS_MAX_CPUS\n");
			goto use_bsp;
		}
		shm = devm_kzalloc(&ics_pdev->dev, sizeof(u32) * size, GFP_KERNEL);
		if (!shm) {
			dev_err(&ics_pdev->dev,"could not alloc shm\n");
			goto use_bsp;
		}
		ret= of_property_read_u32_array(np, "shm", shm, size);
		if (ret) {
			dev_err(&ics_pdev->dev, "cannot read shm in ics kernel DT\n");
			devm_kfree(&ics_pdev->dev, shm);
			goto use_bsp;
		}

		/*  now fills device data  */
		p->nb_bsp_shm = size;
		/*  1st element is a9 export shm  */
		p->bsp_shm[0].interrupt = 1;
		for (i=0;i<size;i++)
			p->bsp_shm[i].base=(void*)shm[i];
		*nb=size;
		devm_kfree(&ics_pdev->dev, shm);
		dev_info(&ics_pdev->dev,"bsp modified by device tree\n");
		return p->bsp_shm;
	}
use_bsp:
	/*  no info provide bsp info as they are, compatible with previous behaviour */
	return bsp_mbx;
}

#else /* #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) */

int ics_mbox_kick(int cpu)
{
	return -1;
}
int ics_mailbox_get(int cpu)
{
	return -1;
}

int ics_mailbox_release(int cpu)
{
	return -1;
}
int ics_mbox_block(int cpu)
{
	return -1;
}

int ics_mbox_unblock(int cpu)
{
	return -1;
}
static int ics_parse_dt(struct platform_device *pdev)
{
	return 0;
}

#endif /* #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) */

/*static int __init ics_probe (struct platform_device *pdev)  *Bug 45206 */
static int ics_probe (struct platform_device *pdev)
{
	int res = 0;
	ICS_ERROR err = ICS_SUCCESS;
	
	ICS_UINT flags = 0;

        struct ics_driver_data *p;

	/*  copy for download firmware  */
	ics_pdev = pdev;

	printk(KERN_INFO "ICS Version %s \n",ics_cpu_version());
	printk(KERN_INFO "ICS init %d debug_flags 0x4%x debug_chan 0x%x connect %d watchdog %d bpa2_part '%s'\n",
	       init, debug_flags, debug_chan, connect, watchdog, bpa2_part);

	res = ics_parse_dt(pdev);
	if (res < 0) {
		return res;}

	ics_mbox_init(pdev);

	ics_debug_flags(debug_flags);

	ics_debug_chan(debug_chan);
	ics_tune(ICS_TUNEABLE_ADMIN_THREAD_PRIORITY, thread_ics_admin);
	ics_tune(ICS_TUNEABLE_NSRV_THREAD_PRIORITY, thread_ics_nsrv);
	ics_tune(ICS_TUNEABLE_STATS_THREAD_PRIORITY, thread_ics_stats);
	ics_tune(ICS_TUNEABLE_WATCHDOG_THREAD_PRIORITY, thread_ics_watchdog);

	if (connect)
	{ flags |= ICS_INIT_CONNECT_ALL; }

	if (watchdog)
	{ flags |= ICS_INIT_WATCHDOG; }

	/* Parse the regions module parameter string */
	res += (NULL == regions ? 0 : parseRegionString(regions));

	/* Parse the firmware module parameter string */
	res += (NULL == firmware ? 0 : parseFirmwareString(firmware));

        p = platform_get_drvdata(ics_pdev);
	if (!p)
		return -EINVAL;

        spin_lock_init(&p->rproc_lock);

	/* Load and start any firmware files */
	res += (NULL == firmware ? 0 : load_firmware());

	if (bpa2_part != NULL)
	{
		/* Look up associated BPA2 partition */
		ics_bpa2_part = bpa2_find_part(bpa2_part);
		if (ics_bpa2_part)
			printk(KERN_INFO "ICS: Using BPA2 partition '%s'\n", bpa2_part);
		else
			printk(KERN_ERR  "ICS: Failed to find BPA2 partition '%s'\n", bpa2_part);
		/* BZ 49149 & BZ 48077*/
		if (bpa2_low_part(ics_bpa2_part))
		{
			ICS_EPRINTF(ICS_DBG_INIT, "ICS can't use LOW BPA2 region already mapped by kernel\n");
			res = 1;
		}
	}

	if (res != 0) {
		err = -EINVAL;
		goto err_stop_fw;
	}

	printk(KERN_DEBUG "Test: ICS_GetTuneable(ICS_TUNEABLE_ADMIN_THREAD_PRIORITY) = %i \n",
	       ICS_GetTuneable(ICS_TUNEABLE_ADMIN_THREAD_PRIORITY));

	if (init) {
		/* user wants to overload BSP */
		if (cpu_num != 0 || cpu_mask != 0) {
			printk(KERN_DEBUG "Calling ics_cpu_init(%d, 0x%lx, 0x%x)\n", cpu_num, cpu_mask, flags);
			err = ics_cpu_init(cpu_num, cpu_mask, flags);
		}
		else {
			printk(KERN_DEBUG "Calling ICS_cpu_init(%d, 0x%lx, 0x%x)\n", ics_cpu_self(), ics_cpu_mask(), flags);
			err = ICS_cpu_init(flags);
		}

		if (err != ICS_SUCCESS) {
			printk(KERN_ERR "ICS Initialisation failed : %s (%d)\n",
			       ics_err_str(err), err);
			err = -ENODEV;
			goto err_stop_fw;
		}
		err = addRegion();
		if (err != ICS_SUCCESS) {
			printk(KERN_ERR "ICS add region failed");
			err = -ENODEV;
			goto err_stop_fw;
		}
	}


#ifdef CONFIG_PROC_FS	/* bz45137 */
	/* Create the procfs files and directories */
	ics_create_procfs(ics_cpu_mask());
#endif

	return 0;

err_stop_fw:
	if (firmware)
		ics_cpu_reset_all();

#if defined(__arm__)

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
#else
	slaveUnmapRegMap();
#endif

#endif

	return err;
}

static int __exit ics_remove (struct platform_device *pdev) 
{

#ifdef CONFIG_PROC_FS	/* bz45137 */
	/* Remove the per CPU procfs entries */
	ics_remove_procfs(ics_cpu_mask());
#endif
	ics_cpu_reset_all();
	ICS_cpu_term(0);

#if defined(__arm__)

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
#else
	slaveUnmapRegMap();
#endif

#endif

	ics_pdev = NULL;
	return 0;
}

#ifdef CONFIG_PM
static int ics_freeze(struct device *dev)
{
    printk(KERN_DEBUG "ics_freeze()\n");

	ics_cpu_reset_all();
	ICS_cpu_term(0);

	return 0;
}

/* Global ICS state structure */
extern ics_state_t *ics_state;

static int ics_restore(struct platform_device *pdev)
{
	ICS_ERROR err;

    printk(KERN_DEBUG "ics_restore()\n");

	if (ics_state != NULL)
	{
		printk(KERN_DEBUG "ics_restore(skip)\n");
		return 0;
	}

	/* Load the ELF image */
	load_firmware();

	/* Re-initiate ICS */
	/* --------------- */
	err=ICS_cpu_init(0);

	if (err != ICS_SUCCESS) {
		printk(KERN_ERR "ICS_init failed\n");
	}

	err= addRegion();

	if (err != ICS_SUCCESS) {
		printk(KERN_ERR "ICS add region failed\n");
	}

	return 0;
}

static const struct dev_pm_ops ics_pm_ops = {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
	.suspend = ics_freeze,
#else
	.freeze = ics_freeze, /* hibernation */
#endif
};

static int ics_pm_notifier_cb(struct notifier_block *nb,
			unsigned long action, void *ptr)
{
	switch (action) {
	case PM_POST_SUSPEND:
	case PM_POST_HIBERNATION:
	case PM_POST_RESTORE:
		ics_restore(ics_pdev);
		break;
	default:
		return NOTIFY_DONE;
	}
	return NOTIFY_OK;
}

static struct notifier_block ics_pm_notifier = {
		.notifier_call = ics_pm_notifier_cb,

};

#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

static struct platform_driver ics_driver = {
	.probe        = ics_probe,
	.remove       = __exit_p(ics_remove),
	.driver		= {
		.name	= MODULE_NAME,
		.owner  = THIS_MODULE,
		.of_match_table = ics_of_match,

#ifdef CONFIG_PM
	/*For power management*/
		.pm    = &ics_pm_ops,
#endif
		
	}

};

#else

static struct platform_driver ics_driver = {
	.driver.name  = MODULE_NAME,
	.driver.owner = THIS_MODULE,
#ifdef CONFIG_PM
	/*For power management*/
	.driver.pm    = &ics_pm_ops,
#endif

	.probe        = ics_probe,
	.remove       = __exit_p(ics_remove),
};
#endif
/*  copy for firmware download  */
struct platform_device *ics_pdev;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
#else
static void ics_release (struct device *dev) {}

struct platform_device ics_dev = {
	.name           = MODULE_NAME,
	.id             = -1,
	.dev.release    = ics_release,
};
#endif

int __init ics_mod_init( void )
{
	platform_driver_register(&ics_driver);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

    /* Device Tree */

#else
	platform_device_register(&ics_dev);
#endif
#ifdef CONFIG_PM
	register_pm_notifier(&ics_pm_notifier);
#endif
	return 0;
}

void __exit ics_mod_exit( void )
{
#ifdef CONFIG_PM
	unregister_pm_notifier(&ics_pm_notifier);
#endif
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)

    /* Device Tree */

#else
	platform_device_unregister(&ics_dev);
#endif	
	platform_driver_unregister(&ics_driver);

	return;
}

module_init(ics_mod_init);
module_exit(ics_mod_exit);

#endif /* defined(__KERNEL__) && defined(MODULE) */

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
