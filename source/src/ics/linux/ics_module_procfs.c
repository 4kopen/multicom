/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 
 
#ifdef CONFIG_PROC_FS


/* 
 * 
 * All rights reserved. 
 */ 



#include <ics.h>	/* External defines and prototypes */

#include "_ics_sys.h"	/* Internal defines and prototypes */

#include "_ics_module.h"

#include <linux/proc_fs.h>

static ICS_STATS_ITEM stats_array[ICS_STATS_MAX_ITEMS];

/* PROCFS stuff */
static struct proc_dir_entry *module_dir, *cpu_dir[_ICS_MAX_CPUS];
static struct proc_dir_entry *debuglog[_ICS_MAX_CPUS], *stats[_ICS_MAX_CPUS];


/* 
 * New implemetation fo proc_fs to match 3.10 kernel API change which 
 * fortunately in backward compatible with 3.4
 *
 * It consists of replacing create_proc_read_entry with proc_create_data and its
 * associated ops, that requires ics_read_debuglog and ics_read_stats 
 * modification
 */

static int ics_log_read (  struct file 	*file,
				char __user 	*buffer,
				size_t		len,
				loff_t		*offset) 
{
	int res;
	ICS_INT  bytes = 0;
	ICS_UINT cpuNum = (ICS_UINT) file->private_data;
	char *tmp_buf;
	int tmp_len = 0; 
		
	tmp_buf = kzalloc(len*sizeof(char),GFP_KERNEL);
	if (!tmp_buf){
		ICS_PRINTF(ICS_DBG_ERR, "%s : Unable to get buf memory\n", __func__);
		return -ENOMEM;
	}
	
	/* Extract the debug log into the supplied buffer
	 *
	 * bytes will be updated with the number of bytes copied
	 * and ENOMEM will be returned if there is more data to be returned
	 */
	res = ICS_debug_copy(cpuNum, tmp_buf, len, &bytes);
	tmp_len=bytes; /* 45527 */ 

	/* Display an error message in the case where the CPU is not
	 * connected so we can tell the difference between this and no output
	 */
	if ((res != ICS_SUCCESS) && (res != ICS_ENOMEM) && (offset == NULL)) {
		 tmp_len = snprintf(tmp_buf, len,
				 "Failed to dump cpu %02d log : %s (%d)\n",
				 cpuNum,
				 ics_err_str(res), res);
	}
	
	bytes = simple_read_from_buffer(buffer, len, offset, tmp_buf,tmp_len);
	
	kfree(tmp_buf);

	return bytes;
}


static int ics_open(struct inode *inode, struct file *file)
{
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(3,10,0))
	file->private_data = (void *) PDE_DATA(inode);
#else
	file->private_data = (void *) PDE(inode)->data;
#endif

	ICS_PRINTF(ICS_DBG_DUMP, "%s (user data --> cpuNum:%d)\n", __func__, (ICS_UINT ) file->private_data );
	
    return 0;
}

static int ics_close(struct inode *inode, struct file *file)
{
	return 0;
}

static const struct file_operations ics_log_fops = {
	.owner		= THIS_MODULE,
	.open		= ics_open,
	.read		= ics_log_read,
	.write		= NULL,
	.llseek		= default_llseek,
	.release	= ics_close,
};

static int ics_stats_read ( struct file 	*file,
			    	char __user 	*buffer,
			    	size_t		len,
			    	loff_t		*offset) 
{
	int res, bytes = 0;
	ICS_UINT cpuNum = (ICS_UINT) file->private_data;
	ICS_UINT i, nstats;
	ICS_ULONG version;
	char *tmp_buf;
	int tmp_len = 0;

	tmp_buf = kzalloc(len*sizeof(char), GFP_KERNEL);
	if (!tmp_buf){
		ICS_PRINTF(ICS_DBG_ERR, "%s : Unable to get buf memory\n", __func__);
		return -ENOMEM;
	}
	res = ICS_cpu_version(cpuNum, &version);
	if (res != ICS_SUCCESS)
	{	/* bz 54010 */
	    kfree(tmp_buf);
	    return -EINVAL;
	}
	
	res = ICS_stats_sample(cpuNum, stats_array, &nstats);
	
	if (res != ICS_SUCCESS)
	{	/* bz 54010 */
	    kfree(tmp_buf);
	    return -EINVAL;
	}
	
	tmp_len = sprintf(tmp_buf, "STATISTICS for CPU %02d ICS version 0x%08lx\n",
			 cpuNum, version);
	for (i = 0; i < nstats; i++) {
		ICS_STATS_ITEM *item;
		ICS_STATS_VALUE delta, timeDelta;
		
		item = &stats_array[i];

		/* Calculate deltas */
		delta     = (item->value[0] - item->value[1]);
		timeDelta = (item->timestamp[0] - item->timestamp[1]);
	  
	/*Update statics only if timeDelta is non zero to avoid divide by zero */
	if( 0 != timeDelta){
		tmp_len += sprintf(tmp_buf + tmp_len, "%-32s\t", 
					stats_array[i].name);
		if (item->type & ICS_STATS_COUNTER)
			  /* Display simple counter */
			tmp_len += sprintf(tmp_buf + tmp_len,  
					" %lld (%lld)", item->value[0], delta);

		if (item->type & ICS_STATS_RATE)
			  /* Display rate per sec */
			tmp_len += sprintf(tmp_buf + tmp_len,  " %ld/s",
				  	 /* No 64-bit udiv */
					 (1000*(unsigned long)delta)/
					 (unsigned long)timeDelta);
		
  		if (item->type & ICS_STATS_PERCENT)
	  		/* Display as a percentage of time */
			tmp_len += sprintf(tmp_buf + tmp_len, " %ld%%",
			  		 /* No 64-bit udiv */
				  	 (100*(unsigned long)delta)/
					 (unsigned long)timeDelta);

  		if (item->type & ICS_STATS_PERCENT100)
	  		/* Display as 100-percent of time */
			tmp_len += sprintf(tmp_buf + tmp_len, " %ld%%",
			  		 /* No 64-bit udiv */
				  	 100 - ((100*(unsigned long)delta)/
					 (unsigned long)timeDelta));
    		}
		tmp_len += sprintf(tmp_buf + tmp_len, "\n");
  	}
	bytes = simple_read_from_buffer(buffer, len, offset, tmp_buf, tmp_len);
	kfree(tmp_buf);

	return bytes;
}


static const struct file_operations ics_stats_fops = {
	.owner		= THIS_MODULE,
	.open		= ics_open,
	.read		= ics_stats_read,
	.write		= NULL,
	.llseek		= default_llseek,
	.release	= ics_close,
};


/* Create the per CPU procfs entries */
/* Bug 45206 */
int ics_create_procfs (ICS_ULONG cpuMask)
{
	int i, res;
	ICS_ULONG mask;
	
	module_dir = proc_mkdir(MODULE_NAME, NULL);
	ICS_PRINTF(ICS_DBG, "ICS debug: %x in module_dir \n",(int)module_dir );
	if(module_dir == NULL) {
                res = -ENOMEM;
                goto out;
        }

	SET_PROCFS_OWNER(module_dir);
	
	for (i = 0, mask = cpuMask; mask; i++, mask >>= 1) {
		if (mask & 1) {
			char procname[64];

			sprintf(procname, "cpu%02d", i);
			
			cpu_dir[i] = proc_mkdir(procname, module_dir);
			if (cpu_dir[i] == NULL) {
				res = -ENOMEM;
				ICS_PRINTF(ICS_DBG, "ICS debug: failed to create procname (%s) \n",procname );
				goto remove_cpus;
			}

			SET_PROCFS_OWNER(cpu_dir[i]);

			/*
			 * Create debug log entry
			 */
			debuglog[i] = proc_create_data( "log", 
							S_IRUGO, /* default mode */
							cpu_dir[i], /* parent dir */
							&ics_log_fops,
							(void *)i );/* client data */
			if (debuglog[i] == NULL) {
				res = -ENOMEM;
				goto remove_files;
			}

			SET_PROCFS_OWNER(debuglog[i]);

			/*
			 * Create stats entry
			 */
			stats[i] = proc_create_data(	"stats",
							S_IRUGO,
							cpu_dir[i], /* parent dir */
							&ics_stats_fops,
							(void *)i); /* client data */
			if (stats[i] == NULL) {
				res = -ENOMEM;
				goto remove_files;
			}
		}
	}

	return 0;

remove_files:
	for (i = 0, mask = cpuMask; mask; i++, mask >>= 1) {
		if ((mask & 1)) {
			if (debuglog[i])
				remove_proc_entry("log", cpu_dir[i]);
			if (stats[i])
				remove_proc_entry("stats", cpu_dir[i]);
		}
	}

remove_cpus:
	for (i = 0, mask = cpuMask; mask; i++, mask >>= 1) {
		if ((mask & 1) && cpu_dir[i]) {
			char procname[64];

			sprintf(procname, "cpu%02d", i);

			remove_proc_entry(procname, module_dir);
			module_dir=NULL;
		}
	}

	remove_proc_entry(MODULE_NAME, NULL);

out:
	return res;
}

void __exit ics_remove_procfs (ICS_ULONG cpuMask)
{
	int i;
	ICS_ULONG mask;

	if (module_dir) {
		/* Remove log/stat files */
		for (i = 0, mask = cpuMask; mask; i++, mask >>= 1) {
			if ((mask & 1)) {
				if (debuglog[i])
					remove_proc_entry("log", cpu_dir[i]);
				if (stats[i])
					remove_proc_entry("stats", cpu_dir[i]);
			}
		}

		/* Remove cpu dirs */
		for (i = 0, mask = cpuMask; mask; i++, mask >>= 1) {
			if ((mask & 1) && cpu_dir[i]) {
				char procname[64];
			
				sprintf(procname, "cpu%02d", i);

				remove_proc_entry(procname, module_dir);
			}
		}
	
		/* Remove module */
		remove_proc_entry(MODULE_NAME, NULL);
	}
	return;
}

#endif /* CONFIG_PROC_FS */

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
