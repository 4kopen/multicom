#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

#
# Per SoC specification file that uses templates to generate the BSP library targets
# 
# ST231 version

#
# stxh205
#
BSP_SOC   = stxh205
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7105
#
BSP_SOC   = stx7105
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7106
#
BSP_SOC   = stx7106
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7108
#
BSP_SOC   = stx7108
BSP_CORES = audio video gp
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7109
#
BSP_SOC   = stx7109
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7111
#
BSP_SOC   = stx7111
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7141
#
BSP_SOC   = stx7141
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx7200
#
BSP_SOC   = stx7200
BSP_CORES = audio0 audio1 video0 video1
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stx5289
#
BSP_SOC   = stx5289
BSP_CORES = audio video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# fli7510
#
BSP_SOC   = fli7510
BSP_CORES = audio0 audio1 video
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stxh415
#
BSP_SOC   = stxh415
BSP_CORES = audio0  audio1 gp video0 video1
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stxh416
#
BSP_SOC   = stxh416
BSP_CORES = audio0  audio1 gp video0 video1
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# fli7610
#
BSP_SOC   = fli7610
BSP_CORES = audio  gp0 gp1 video0 video1
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stxh407
#
BSP_SOC   = stxh407
BSP_CORES = audio video gp0 gp1
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stxh418
#
BSP_SOC   = stxh418
BSP_CORES = audio video gp0 gp1
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))

#
# stxh390
#
BSP_SOC   = stxh390
BSP_CORES = audio video gp0 gp2
$(foreach core,$(BSP_CORES),$(eval $(call BSPLIB_template,$(BSP_SOC),$(core))))
