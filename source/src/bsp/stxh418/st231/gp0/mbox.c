/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

/*
 *
 */

/*
 * b2198/b2199 / stih418 / st231 [gp0-secu]
 */

/*
 * Mailbox allocation
 * MBOX10_0x000 -> a9      [IRQ 1]
 * MBOX11_0x000 -> gp-11   [IRQ 0]
 * MBOX13_0x000 -> video   [IRQ 0]
 * MBOX13_0x100 -> audio   [IRQ 1]
 * MBOX12_0x000 -> gp0-secu    [IRQ 0]
 *
 * IRQ line naming
 * IRQ0 ---> ST40_IRQ
 * IRQ1 ---> ST200_IRQ
 */
#include <bsp/_bsp.h>

#define MBOX10_0_ADDR	0x08F00000
#define MBOX10_1_ADDR	0x08F00100
#define MBOX11_0_ADDR	0x08F01000
#define MBOX11_1_ADDR	0x08F01100
#define MBOX12_0_ADDR	0x08F02000
#define MBOX12_1_ADDR	0x08F02100
#define MBOX13_0_ADDR	0x08F03000
#define MBOX13_1_ADDR	0x08F03100


#ifdef __os21__

#include <os21.h>

extern interrupt_name_t OS21_INTERRUPT_MAILBOX_2_0;
#define MBOX12_IRQ0	((unsigned int) &OS21_INTERRUPT_MAILBOX_2_0)

#else
#error Invalid OS type.
#endif


struct bsp_mbox_regs bsp_mailboxes[] =
{
  {
    .base	= (void *) (MBOX10_0_ADDR), 		    /* MBOX10.0 Mailbox */
    .interrupt	= 0,
    .flags      = 0
  },
  {
    .base	= (void *) (MBOX10_1_ADDR), 		/* MBOX10.1 Mailbox */
    .interrupt	= 0,
    .flags      = 0
  },

  {
    .base	= (void *) (MBOX11_0_ADDR), 		    /* MBOX11.0 Mailbox */
    .interrupt	= 0,
    .flags      = 0
  },
  {
    .base	= (void *) (MBOX11_1_ADDR), 		/* MBOX11.1 Mailbox */
    .interrupt	= 0,
    .flags      = 0
  },

  {
    .base	= (void *) (MBOX12_0_ADDR), 		    /* MBOX12.0 Mailbox */
    .interrupt	= MBOX12_IRQ0,                /* We OWN this one*/
    .flags      = 0
  },
  {
    .base	= (void *) (MBOX12_1_ADDR),		 /* MBOX12.1 Mailbox */
    .interrupt	= 0,
    .flags      = 0
  },

  {
    .base	= (void *) (MBOX13_0_ADDR),							/* MBOX13.0 Mailbox */
    .interrupt  = 0,
    .flags      = 0
  },
  {
    .base	= (void *) (MBOX13_1_ADDR),		 /* MBOX13.1 Mailbox */
    .interrupt	= 0,
    .flags      = 0
  },

};

unsigned int bsp_mailbox_count = sizeof(bsp_mailboxes)/sizeof(bsp_mailboxes[0]);

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */

