/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

/*
 * stih418 arm cortexa9
 */

/*
 * Mailbox allocation
 * MBOX10_0x000 -> a9      [IRQ 1]
 * MBOX13_0x000 -> video   [IRQ 0]
 * MBOX13_0x100 -> audio   [IRQ 1]
 * MBOX12_0x000 -> gp0     [IRQ 0]
 * MBOX11_0x000 -> gp1     [IRQ 0]
 *
 * IRQ line naming
 * IRQ0 ---> ST40_IRQ
 * IRQ1 ---> ST200_IRQ
 */

#include <bsp/_bsp.h>

#define MBOX10_ADDR	0x08F00000
#define MBOX11_ADDR	0x08F01000
#define MBOX12_ADDR	0x08F02000
#define MBOX13_ADDR	0x08F03000

#ifdef __os21__
#error OS21 not supported on stih418 arm cortexa9 core
#endif

#ifdef __KERNEL__
/* MO: Not ported */
/* Based on an IRQ base of 32 + offsets in ADCS xxxxx (Table xxxxx) */
#define IRQ_BASE	(32)
#define MBOX10_IRQ1 (IRQ_BASE + 1)
#define MBOX11_IRQ1	(IRQ_BASE + 2)
#define MBOX12_IRQ1	(IRQ_BASE + 3)
#define MBOX13_IRQ1	(IRQ_BASE + 4)

#endif

struct bsp_mbox_regs bsp_mailboxes[] =
{
  {
    .base	= (void *) (MBOX10_ADDR), 								/*  Mailbox (MBOX10.1) */
    .interrupt	= MBOX10_IRQ1,
    .flags      = 0
  },
  {
    .base	= (void *) (MBOX10_ADDR+0x100), 		/* Mailbox (MBOX10.2) */
    .interrupt	= 0,
    .flags      = 0
  },

  {
    .base	= (void *) (MBOX11_ADDR), 							/* Mailbox (MBOX11.1) */
    .interrupt	= 0,																/* *WE* own this one */
    .flags      = 0
  },
  {
    .base	= (void *) (MBOX11_ADDR+0x100), 		/* Mailbox (MBOX11.2) */
    .interrupt	= 0,
    .flags      = 0
  },


  { .base	= (void *) (MBOX12_ADDR),									/* Mailbox (MBOX12.1) */
    .interrupt  = 0,
    .flags      = 0
  },
  { .base	= (void *) (MBOX12_ADDR+0x100),			/* Mailbox (MBOX12.2) */
    .interrupt	= 0,
    .flags      = 0
  },


  { .base	= (void *) (MBOX13_ADDR),								/* Mailbox (MBOX13.1) */
    .interrupt  = 0,
    .flags      = 0
  },
  { .base	= (void *) (MBOX13_ADDR+0x100),		/* Mailbox (MBOX13.2) */
    .interrupt	= 0,
    .flags      = 0
  },
};

unsigned int bsp_mailbox_count = sizeof(bsp_mailboxes)/sizeof(bsp_mailboxes[0]);

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
