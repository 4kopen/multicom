/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

/*
 */

#ifndef __STXH418REG_H
#define __STXH418REG_H

#include "sh4regtype.h"
/*
 * STiH418 control registers
 */

/* GP  System configuration registers 10 (CORE_sysconf from system_config_5000 to system_config_5999) */
#define STXH418_CORE_SYSCONF_REGS_BASE 0x092B0000

/*struct bsp_boot_address_reg bsp_sys_boot_registers[]*/
#define STXH418_SYSCONF_SYS_CFG5104 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x01A0)
#define STXH418_SYSCONF_SYS_CFG5136 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x0220)
#define STXH418_SYSCONF_SYS_CFG5137 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x0224)
#define STXH418_SYSCONF_SYS_CFG5138 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x0228)
#define STXH418_SYSCONF_SYS_CFG5139 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x022C)

/*struct bsp_reg_mask bsp_sys_reset_registers[]*/
#define STXH418_SYSCONF_SYS_CFG5128 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x0200)
#define STXH418_SYSCONF_SYS_CFG5131 SH4_DWORD_REG(STXH418_CORE_SYSCONF_REGS_BASE + 0x020C)

/* struct bsp_reg_mask bsp_debug_cpu_registers[] */
#define STXH418_SA_DEBUG_ENABLE_REG (0x08786000 + 0xE4) /* TO BE DONE */
#define STXH418_SA_DEBUG_UNLOCK_REG (0x08786000 + 0x58) /* TO BE DONE */
#define SA_DEBUG_ENABLE_REG (STXH418_SA_DEBUG_ENABLE_REG)
#define SA_DEBUG_UNLOCK_REG (STXH418_SA_DEBUG_UNLOCK_REG)

#endif /* __STXH418REG_H */
