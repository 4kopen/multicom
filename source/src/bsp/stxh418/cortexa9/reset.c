/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

/*
 *
 */

#include <bsp/_bsp.h>

#include "stxh418reg.h"

struct bsp_reg_mask bsp_sys_reset_bypass[] = {
};

/* Size of the above array */
unsigned int bsp_sys_reset_bypass_count = sizeof(bsp_sys_reset_bypass)/sizeof(bsp_sys_reset_bypass[0]);

struct bsp_boot_address_reg bsp_sys_boot_registers[] = {
  {STXH418_SYSCONF_SYS_CFG5104, 0, 0x00000007},   /* host-a9   */
  {STXH418_SYSCONF_SYS_CFG5136, 0, 0xFFFFFFE0},   /* gp-11     */
  {STXH418_SYSCONF_SYS_CFG5137, 0, 0xFFFFFFE0},   /* video     */
  {STXH418_SYSCONF_SYS_CFG5138, 0, 0xFFFFFFE0},   /* audio     */
  {STXH418_SYSCONF_SYS_CFG5139, 0, 0xFFFFFFE0},   /* gp0-secu  */
};
/* Size of the above array */
unsigned int bsp_sys_boot_registers_count = sizeof(bsp_sys_boot_registers)/sizeof(bsp_sys_boot_registers[0]);

/* STXH418 reset bits are active low - mask and ~mask will be used and value
   should be 0.
 */
struct bsp_reg_mask bsp_sys_reset_registers[] = {
  {STXH418_SYSCONF_SYS_CFG5128, ~(1 << 0), 0} ,	 /* host-a9     */
  {STXH418_SYSCONF_SYS_CFG5128, ~(1 << 2), 0} ,	 /* gp-11 	*/
  {STXH418_SYSCONF_SYS_CFG5131, ~(1 << 27), 0},	 /* video 	*/
  {STXH418_SYSCONF_SYS_CFG5131, ~(1 << 26), 0},	 /* audio       */
  {STXH418_SYSCONF_SYS_CFG5131, ~(1 << 28), 0},	 /* gp0-secu    */
};

/* Size of the above array */
unsigned int bsp_sys_reset_registers_count = sizeof(bsp_sys_reset_registers)/sizeof(bsp_sys_reset_registers[0]);

/* STXH418 has no boot enables - purely controlled by reset bits */
struct bsp_reg_mask bsp_sys_boot_enable[] = {
  {NULL, 0, 0},
  {NULL, 0, 0},
  {NULL, 0, 0},
  {NULL, 0, 0},
  {NULL, 0, 0}
};

/* STXH418 CPU debug control bits 0-Normal mode, 1-Debug mode  */
struct bsp_reg_mask bsp_debug_cpu_registers[] = {
  {(volatile unsigned int *)STXH418_SA_DEBUG_ENABLE_REG,0,(1 << 1)}, /* host-a9  */
  {(volatile unsigned int *)NULL, 0,(1 << 5)},	 /* gp-11   */
  {(volatile unsigned int *)NULL, 0,(1 << 2)},	 /* video   */
  {(volatile unsigned int *)NULL, 0,(1 << 3)},    /* audio   */
  {(volatile unsigned int *)NULL, 0,(1 << 4)},	 /* gp0- secu    */
};

/* Size of the above array */
unsigned int bsp_debug_cpu_registers_count = sizeof(bsp_debug_cpu_registers)/sizeof(bsp_debug_cpu_registers[0]);
unsigned int ics_bsp_cpu_debug_flag[8];

/* STXH418 CPU debug control register access unlock  */
struct bsp_reg_mask bsp_debug_unlock_registers[] = {
  {(volatile unsigned int *)STXH418_SA_DEBUG_UNLOCK_REG,0,0xa55a5a5a}
};



/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
