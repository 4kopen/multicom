/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

/*
 */

#ifndef __STIH418REG_H
#define __STIH418REG_H

#include "sh4regtype.h"

/*----------------------------------------------------------------------------*/

/*
 * Base addresses for control register banks.
 */


/* STiH418 control registers */
/* GP  System configuration registers 11 (IC_sysconf from system_config_6000 to system_config_6999) */
#ifndef STXH418_IC_SYSCONF_REGS_BASE
#define STXH418_IC_SYSCONF_REGS_BASE 0x092C0000
#endif

/*----------------------------------------------------------------------------*/

#include "st40reg.h"

/*
 * STiH418 SYSCFG control registers
 */
#define STIH418_SYSCONF_SYS_CFG SH4_DWORD_REG(STIH416_SYSCONF_REGS_BASE + 0x0004)

#endif /* __STIH418REG_H */
