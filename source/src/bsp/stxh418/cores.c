/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/*
 *
 */

#include <bsp/_bsp.h>

/* b2198/b2199/stxh418 */

/*
 * Logical CPU numbering
 *
 *    0 cortexa9	host (MASTER)
 *    1 st231 		gp1
 *    2 st231 		video
 *    3 st231 		audio
 *    4 st231 		gp0-secu
 *
 */

/*
 * BSP CPU info
 *
 * This table provides the complete list of all the CPUs 
 * on the SoC.
 * Each CPU is assigned a logical CPU number which is
 * used by ICS to identify it. Logical CPU 0 is assumed
 * to be the MASTER.
 *
 * The order of the CPUs in this table must match that of
 * the other BSP tables (e.g. reset.c) so that the correct
 * CPU registers are used for load/reset
 */
struct bsp_cpu_info bsp_cpus[] =
{
  {
    .name 		= "host",		/* host */
    .type     = "cortexa9",
    .num	  	= 0,			/* MASTER */
    .flags		= 0,
  },

  {
    .name 		= "gp1",		/* general purpose  */
    .type     = "st231",
    .num		  = 1,
    .flags		= 0,
  },

  {
    .name 		= "video",		/* video */
    .type     = "st231",
    .num	  	= 2,
    .flags		= 0,
  },

  {
    .name 	  = "audio",/* audio 0*/
    .type     = "st231",
    .num		  = 3,
    .flags		= 0,
  },

  {
    .name 		= "gp0",		/* general purpose  */
    .type     = "st231",
    .num		  = 4,
    .flags		= 0,
  },


};

unsigned int bsp_cpu_count = sizeof(bsp_cpus)/sizeof(bsp_cpus[0]);

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
