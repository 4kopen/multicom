/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <linux/string.h>
#include <linux/slab.h>
#include "stub_message_queue.h"

#define __CLZ          __builtin_clz

static msg_pool_t *allocate_pool(int list_length)
{
	int              i;
	msg_pool_t      *pool  = (msg_pool_t *) kzalloc(sizeof(msg_pool_t), GFP_KERNEL);
	struct mutex    *mutex = (struct mutex *) kmalloc(sizeof(struct mutex), GFP_KERNEL);

	if (pool != NULL && mutex != NULL) {
		pool->Mutex = mutex;

		mutex_init(mutex);

		/* Set all bits in Map above list_length to 1 to indicate unavailable locations */
		for (i = list_length; i < sizeof(pool->ElementUseMap) * 8; i++) {
			int  word_idx, bit_idx;

			word_idx = i >> 5;
			bit_idx	 = i & 0x1F;
			pool->ElementUseMap[word_idx] |= (1U << bit_idx);
		}
	} else {
		kfree(pool);
		pool = NULL;
		kfree(mutex);
	}

	return pool;
}

static void free_pool(msg_pool_t *pool)
{
	if (pool != NULL)	{
		kfree(pool->Mutex);
		kfree(pool);
	}
}

static __inline int get_zero_bit(unsigned int x)
{
	int y = (int) x; // Signed Number type cast to find whether bit 32 is set

	y = (y > -2) ? (32 - __CLZ(x)) : (31 - __CLZ(~x));

	return y;
}

static int pool_claim(msg_pool_t *pool)
{
	int word_idx = 0, bit_idx = 32;

	mutex_lock(pool->Mutex);
	for (word_idx = 0; word_idx < (sizeof(pool->ElementUseMap) / sizeof(int)); word_idx++) {
		bit_idx = get_zero_bit(pool->ElementUseMap[word_idx]);

		if (bit_idx < 32) {
			pool->ElementUseMap[word_idx] |= (1U << bit_idx); // mark this element as used
			break;
		}
	}
	mutex_unlock(pool->Mutex);

	return ((word_idx << 5) + bit_idx);
}

static void pool_release(msg_pool_t *pool, int element_idx)
{
	int word_idx, bit_idx;

	word_idx	= element_idx >> 5; // Element Idx / 32
	bit_idx	    = element_idx & 0x1F;

	mutex_lock(pool->Mutex);
	pool->ElementUseMap[word_idx] &= ~(1U << bit_idx); // Clear the bit corresponding to element idx in Map
	mutex_unlock(pool->Mutex);
}

static msg_fifo_t *allocate_fifo(void)
{
	msg_fifo_t        *fifo  = (msg_fifo_t *) kzalloc(sizeof(msg_fifo_t), GFP_KERNEL);
	struct mutex      *mutex = (struct mutex *) kmalloc(sizeof(struct mutex), GFP_KERNEL);
	struct semaphore *sem   = (struct semaphore *) kmalloc(sizeof(struct semaphore), GFP_KERNEL);

	if (fifo != NULL && mutex != NULL && sem != NULL) {
		fifo->mutex = mutex;
		fifo->sem   = sem;
		sema_init(sem, 0);   // Create Locked Semaphore
		mutex_init(mutex);
	} else {
		kfree(fifo);
		fifo = NULL;
		kfree(mutex);
		kfree(sem);
	}

	return fifo;
}

static void free_fifo(msg_fifo_t *fifo)
{
	if (fifo != NULL)	{
		kfree(fifo->mutex);
		kfree(fifo->sem);
		kfree(fifo);
	}
}

static void add_fifo(msg_fifo_t *fifo, void *message)
{
	/* Multiple Send need to be handled one after the another */
	msg_header_t *hdr = (msg_header_t *)((char *) message - sizeof(msg_header_t));

	hdr->next = NULL;

	mutex_lock(fifo->mutex);
	/* Push new node to the tail of fifo */
	if (fifo->tail != NULL) {
		fifo->tail->next = hdr;
		fifo->tail       = hdr;
	} else {
		fifo->head = hdr;
		fifo->tail = hdr;
	}
	mutex_unlock(fifo->mutex);

	// Signal one message available in queue
	up(fifo->sem);
}

static void *remove_fifo(msg_fifo_t *fifo)
{
	msg_header_t *hdr;

	if (down_interruptible(fifo->sem)) {
		// Signal Received, return No Message Received
		return NULL;
	}

	mutex_lock(fifo->mutex);
	/* Pop out the head node from Fifo */
	hdr        = fifo->head;
	fifo->head = hdr->next;
	if (hdr->next == NULL) {
		fifo->tail = NULL;
	}
	mutex_unlock(fifo->mutex);

	return (void *)((char *) hdr + sizeof(msg_header_t));
}

/*
 * Delete a message_queue
 */
void message_delete_queue(message_queue_t *msg_queue)
{
	/* ToDo :: Ideally this must ensure all threads waiting for messages do not wait on freed semaphores/mutexes */
	if (msg_queue != NULL) {
		free_pool(msg_queue->pool);
		free_fifo(msg_queue->fifo);
		kfree(msg_queue->ElementBuffer);
		kfree(msg_queue);
	}
}

/*
 * Create a message_queue
 */
message_queue_t *message_create_queue(int element_size, int list_length)
{
	char              *buffer;
	message_queue_t   *msg_queue;
	msg_pool_t        *pool;
	msg_fifo_t        *fifo;

	if (list_length > MAX_MSG_QUEUE_LENGTH) {
		return NULL;
	}

	element_size += sizeof(msg_header_t);// Add space for Fifo Next Ptr

	msg_queue = (message_queue_t *) kmalloc(sizeof(message_queue_t), GFP_KERNEL);
	buffer	  = (char *) kmalloc(element_size * list_length, GFP_KERNEL);
	pool      = allocate_pool(list_length);
	fifo      = allocate_fifo();

	if (msg_queue != NULL && buffer != NULL && pool != NULL && fifo != NULL) {
		msg_queue->ElementSize	    = element_size;
		msg_queue->ElementNb        = list_length;
		msg_queue->ElementBuffer    = buffer;
		msg_queue->pool             = pool;
		msg_queue->fifo             = fifo;
	} else {
		free_pool(pool);
		free_fifo(fifo);
		kfree(buffer);
		kfree(msg_queue);
		msg_queue = NULL;
	}

	return msg_queue;
}

void message_release(message_queue_t *msg_queue, void *msg_addr)
{
	int  element_idx;
	char *msg = (char *) msg_addr - sizeof(msg_header_t);


#ifdef _DEBUG
	if (msg < msg_queue->ElementBuffer || msg > (msg_queue->ElementBuffer + (msg_queue->ElementSize * ElementNb)))	{
		printk("message_release : Invalid Message ptr %x", msg_addr);
		return;
	}
#endif

	// Find the element idx for this message
	element_idx = ((char *)msg - msg_queue->ElementBuffer) / msg_queue->ElementSize;

	pool_release(msg_queue->pool, element_idx);
}

/*
 * Claim the next message from a message_queue's free message Q.
 */
void *message_claim(message_queue_t *msg_queue)
{
	char *msg = NULL;

	int element_idx = pool_claim(msg_queue->pool);

	if (element_idx < msg_queue->ElementNb) {
		msg = (char *) &msg_queue->ElementBuffer[element_idx * msg_queue->ElementSize] + sizeof(msg_header_t);
	}

	return msg;
}

void message_send(message_queue_t *queue, void *message)
{
	add_fifo(queue->fifo, message);
}

void *message_receive(message_queue_t *queue)
{
	return remove_fifo(queue->fifo);
}
