/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 


#define __OS21_POSIX_WRAPPER__

#include "stub_user_ioctl.h"
#include "stub_posix_wrapper.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
#include <stddef.h>

#define REPORT      fprintf

//#define _DEBUG
#ifdef _DEBUG
#define DP          printf
#else
#define DP(...)
#endif

#define MME_STUB_DEV_NAME	"/dev/"STUB_DEV_NAME

#define MAX_USERSPACE_TRANSFORMER            MAX_TRANSFORMER_REGISTRATION
#define MAX_COMMAND_QUEUE_SIZE               32
#define DEFAULT_TASK_STACK_SIZE              (32*1024)   // 32KB
#define MAX_NUM_DATABUFFFERS                 32
#define MAX_NUM_SCATTERPAGES                 64

#define MAX_SIZE_DATA_BUFFERS                ((sizeof(MME_DataBuffer_t) + sizeof(MME_DataBuffer_t*))*MAX_NUM_DATABUFFFERS)
#define MAX_SIZE_SCATTERPAGES                (sizeof(MME_ScatterPage_t)*MAX_NUM_SCATTERPAGES)
#define MAX_SIZE_DATABUFFER_SCATTERPAGES     (MAX_SIZE_DATA_BUFFERS + MAX_SIZE_SCATTERPAGES)

#define MAX_CMDMESSAGE_SIZE                   (MAX_SIZE_DATABUFFER_SCATTERPAGES)

#define __CLZ   __builtin_clz
typedef struct {
	unsigned int  init;
	unsigned int  phy_base[BPA_COUNT];
	unsigned int  base[BPA_COUNT];
	unsigned int  size[BPA_COUNT];
} bpa2_region_mmap_t;

typedef struct {
	int                            inUse;
	char                           name[MME_MAX_TRANSFORMER_NAME + 1];
	int                            deviceFd;
	message_queue_t               *msg_queue;
	task_t                        *msgrcvTask;
	task_t                        *execLoopTask;
	MME_AbortCommand_t             abortFunc;
	MME_GetTransformerCapability_t getTransformerCapabilityFunc;
	MME_InitTransformer_t          initTransformerFunc;
	MME_ProcessCommand_t           processCommandFunc;
	MME_TermTransformer_t          termTransformerFunc;
} MME_UserSpaceTransformer_t;

static void message_receiver_task(void *);
static void execution_loop_task(void *);
static void UnmapCmdPointers(MME_Command_t *cmdInfo);

static mutex_t                    *user_mutex = NULL;
static semaphore_t                *sem_run = NULL;
static MME_UserSpaceTransformer_t  userInst[MAX_USERSPACE_TRANSFORMER];
static unsigned int                nRegisterCount;
static bpa2_region_mmap_t          map = {0};

static MME_UserSpaceTransformer_t *GetFreeUserInstance()
{
	unsigned int i;
	for (i = 0; i < sizeof(userInst) / sizeof(MME_UserSpaceTransformer_t); i++) {
		if (userInst[i].inUse == 0) {
			return &userInst[i];
		}
	}
	return NULL;
}

static MME_UserSpaceTransformer_t *GeUserInstanceByName(const char *name)
{
	unsigned int i;
	for (i = 0; i < sizeof(userInst) / sizeof(MME_UserSpaceTransformer_t); i++) {
		if (userInst[i].inUse && strcmp(userInst[i].name, name) == 0) {
			return &userInst[i];
		}
	}
	return NULL;
}

static unsigned int  TranslateBPA2MMap(MME_UserSpaceTransformer_t *user, bpa2_region_mmap_t *map, unsigned int  paddr, unsigned int  len)
{
	unsigned int  vaddr = (unsigned int) NULL;
	unsigned int paddr_offset = paddr  & 0xfff;
	/*  len is a 4kbytes multiple  */
	len = (len + (paddr & 0xfff) + 0xFFF) & (~0xFFF);
	paddr = paddr & ~0xfff;
	vaddr = (unsigned int) mmap(0,(size_t) len, PROT_READ | PROT_WRITE,
						MAP_SHARED, user->deviceFd, paddr);
	if (!vaddr)
	{
		DP("TranslateBPA2MMap: Could not resolve %x \n", paddr);
		return NULL;
	}
	return vaddr+ paddr_offset;
}

#ifdef _DEBUG
static void print_msg(message_queue_t *msg_queue)
{
	int val, word_idx = 0, bit_idx = 32, element_idx;
	msg_pool_t *pool = msg_queue->pool;
    static const char StrCmdType[4] = {'G', 'T', 'S', ' '};

	DP("Pending CmdId: ");
	acc_mutex_lock(pool->Mutex);
	for (word_idx = 0; word_idx < (sizeof(pool->ElementUseMap) / sizeof(int)); word_idx++) {
		val = pool->ElementUseMap[word_idx];

		while (val) {
			bit_idx     = 31 - __CLZ(val);
			element_idx = ((word_idx << 5) + bit_idx);
			if (element_idx < msg_queue->ElementNb) {
				MME_CommandPacked_t        *cmd;
				cmd = (MME_CommandPacked_t *)((char *) &msg_queue->ElementBuffer[element_idx * msg_queue->ElementSize] + sizeof(
				                                      msg_header_t));
				if (cmd ->cmd_type == MME_PACKED_PROCESS) {
					MME_Command_t  *Command = &cmd->uPack.process.cmd;
                    DP("<%c-%x> ", StrCmdType[Command->CmdCode & 3], Command->CmdStatus.CmdId);
				}
			}
			val &=  ~(1U << bit_idx);
		}
	}
	acc_mutex_release(pool->Mutex);
	DP("\n");
}
#endif

MME_ERROR MME_USER_Init(void)
{
	MME_ERROR status = MME_DRIVER_ALREADY_INITIALIZED;

	DP(" In MME_Init \n");
	if (user_mutex == NULL) {
		user_mutex = STOS_MutexCreate();
		sem_run    = semaphore_create_fifo(0);
		if (user_mutex != NULL && sem_run != NULL) {
			nRegisterCount = 0;
			memset(userInst, 0, sizeof(userInst));
			status = MME_SUCCESS;
		} else {

			status = MME_NOMEM;
		}
	}
	return status;
}

MME_ERROR MME_USER_Term(void)
{
	MME_ERROR status = MME_DRIVER_NOT_INITIALIZED;
	if (user_mutex != NULL) {
		semaphore_signal(sem_run);
		STOS_MutexDelete(user_mutex);
		semaphore_delete(sem_run);
		user_mutex = NULL;
		status = MME_SUCCESS;
	}
	return status;
}

/* MME_Run()
 * Run the MME message loop on a companion CPU
 */
MME_ERROR MME_Run(void)
{
	semaphore_wait(sem_run);
	return MME_SUCCESS;
}

/* MME_RegisterTransformer()
 * Register a transformer after which instantiations may be made
 * Deliberately named similar to Multicom routine to avoid issues in linking
 */
MME_ERROR MME_RegisterTransformer(const char *name,
                                  MME_AbortCommand_t             abortFunc,
                                  MME_GetTransformerCapability_t getTransformerCapabilityFunc,
                                  MME_InitTransformer_t          initTransformerFunc,
                                  MME_ProcessCommand_t           processCommandFunc,
                                  MME_TermTransformer_t          termTransformerFunc)
{
	char                        devname[64];
	char                        tname[256];
	int                         minor = 0;
	int                         res;
	MME_ERROR                   err = MME_INVALID_ARGUMENT;
	MME_UserSpaceTransformer_t *user;
	MME_StubRegisterTransform_t reg;

	/* Sanity on Input Args */
	if (name == NULL || strlen(name) > MME_MAX_TRANSFORMER_NAME) 
		return err;

	STOS_MutexLock(user_mutex);

	user = GetFreeUserInstance();
	if (user == NULL) {
		REPORT(stderr, "ERR>> No further registration possible\n");
		goto err_return;
	}

	/* Clear out all stale information */
	memset(user, 0, sizeof(MME_UserSpaceTransformer_t));

	/* Iterate on all the dev to see which one is available */
	do {
		sprintf(devname, "%s%d", MME_STUB_DEV_NAME, minor++);
		user->deviceFd = open(devname, O_RDWR);
	} while (minor < MAX_TRANSFORMER_REGISTRATION && user->deviceFd < 0);

	if (user->deviceFd < 0) {
		REPORT(stderr, "ERR>> Failed to open %s - errno %d\n", devname, errno);
		goto err_return;
	}

	strcpy(reg.TransformerName, name);
	strcpy(user->name, name);

	user->abortFunc                    = abortFunc;
	user->getTransformerCapabilityFunc = getTransformerCapabilityFunc;
	user->initTransformerFunc          = initTransformerFunc;
	user->processCommandFunc           = processCommandFunc;
	user->termTransformerFunc          = termTransformerFunc;

	res = ioctl(user->deviceFd, MME_STUB_IOC_REGISTER, &reg);
	if (res != 0) {
		REPORT(stderr, "ERR>> IOCTL MME_STUB_IOC_REGISTER Failed, Internal Error %d\n", err);
		goto err_close;
	}
	user->msg_queue = message_create_queue((sizeof(MME_CommandPacked_t) + MAX_CMDMESSAGE_SIZE), MAX_COMMAND_QUEUE_SIZE);
	if (user->msg_queue == NULL) {
		REPORT(stderr, "ERR>> Process Command Queue creation failed \n");
		err = MME_INVALID_ARGUMENT;
		goto err_close;
	}

	sprintf(tname, "M-%s", name);
	/* Create the two threads, the message loop and the execution loop */
	user->msgrcvTask   = task_create(message_receiver_task, user, DEFAULT_TASK_STACK_SIZE, MAX_USER_PRIORITY - 10, tname,
			0);
	if (user->msgrcvTask == NULL) {
		REPORT(stderr, "ERR>> Message Receiver Task creation failed \n");
		err = MME_INVALID_ARGUMENT;
		goto err_close;
	}

	sprintf(tname, "E-%s", name);
	user->execLoopTask = task_create(execution_loop_task, user, DEFAULT_TASK_STACK_SIZE, MAX_USER_PRIORITY - 20, tname, 0);
	if (user->execLoopTask == NULL) {
		REPORT(stderr, "ERR>> Execution Loop Task creation failed \n");
		goto err_task;
	}

	/* Everything went well, mark instance as used */
	user->inUse = 1;
	nRegisterCount++;

	STOS_MutexRelease(user_mutex);

	return MME_SUCCESS;

err_task:
	task_kill(user->msgrcvTask, 0, 0);
	task_delete(user->msgrcvTask);

err_close:
	close(user->deviceFd);

err_return:
	STOS_MutexRelease(user_mutex);
	return err;
}

/* MME_DeregisterTransformer()
 * Deregister a transformer that has been registered
 */
MME_ERROR MME_DeregisterTransformer(const char *name)
{
	MME_UserSpaceTransformer_t *user;

	STOS_MutexLock(user_mutex);

	user = GeUserInstanceByName(name);
	if (NULL == user) {
		STOS_MutexRelease(user_mutex);
		REPORT(stderr, "ERR>> Transformer %s Not Registered\n", name);
		return MME_UNKNOWN_TRANSFORMER;
	}

	message_send(user->msg_queue, NULL); // Dummy post to unlock and exit the execution loop task

	task_kill(user->msgrcvTask, 0, 0);
	task_delete(user->msgrcvTask);

	task_wait(&user->execLoopTask, 1, TIMEOUT_INFINITY);
	task_kill(user->execLoopTask, 0, 0);
	task_delete(user->execLoopTask);

	message_delete_queue(user->msg_queue);

	if (user->deviceFd >= 0) {
		close(user->deviceFd);
	}

	user->inUse = 0;
	nRegisterCount--;

	STOS_MutexRelease(user_mutex);

	return MME_SUCCESS;
}

/* MME_NotifyHost()
 * Notify the Command Client that a transformer event has occurred
 */
MME_ERROR MME_NotifyHost(MME_Event_t event, MME_Command_t *command, MME_ERROR result)
{
	MME_CommandPacked_t         *cmd;
	MME_UserSpaceTransformer_t  *user;
	int                          res;
	int                          offset;

	if (command == NULL) {
		return MME_INVALID_ARGUMENT;
	}

	if (event < MME_COMMAND_COMPLETED_EVT || event > MME_NOT_ENOUGH_MEMORY_EVT) {
		return MME_INVALID_ARGUMENT;
	}

	offset = offsetof(MME_CommandPacked_t, uPack.process.cmd);
	cmd    = (MME_CommandPacked_t *)((unsigned int) command - offset);
	user   = (MME_UserSpaceTransformer_t *) cmd->userData;

	/* Return supplied error code */
	cmd->uPack.process.status = result;
	cmd->event                = event;
#ifdef _DEBUG
	if(MME_SUCCESS != result)
	{
		DP("MME_NotifyHost: result = %d\n", result);
	}
#endif

	/* Send Notification to Stub */
	res = ioctl(user->deviceFd, MME_STUB_IOC_NOTIFYHOST, cmd);

	if (event == MME_COMMAND_COMPLETED_EVT) {
		/*  un map data  */
		UnmapCmdPointers(&cmd->uPack.process.cmd);
		/* Release the message command for these cases */
		message_release(user->msg_queue, cmd);
#ifdef _DEBUG
		print_msg(user->msg_queue);
#endif
	}

	if (res != 0)	{
		REPORT(stderr, "ERR>> IOCTL MME_STUB_IOC_NOTIFYHOST Failed, Internal Error %d\n", res);
		return MME_INVALID_ARGUMENT;
	}

	return MME_SUCCESS;
}
static void UnMap(unsigned long address, unsigned long size)
{
/*  aligned address and length on 4kbytes */
	unsigned long vaddr =  address & ~0xfff;
    unsigned  length= (address -vaddr + size + 0xFFF) & ~0xfff;
	munmap((void*)vaddr, length);
}

static void UnmapCmdPointers(MME_Command_t *cmdInfo)
{
	unsigned long p;
	MME_UINT              nBuf, nPage;
	MME_CommandStatus_t  *status = &cmdInfo->CmdStatus;

	/* Remap the physical addresses to Virtual mmaped addresses for this cmd */
	if (cmdInfo->ParamSize) {
		p                = (unsigned long) cmdInfo->Param_p;
		UnMap(p, cmdInfo->ParamSize);
	}
	if (status->AdditionalInfoSize) {
		p  = (unsigned long) status->AdditionalInfo_p;
		UnMap(p, status->AdditionalInfoSize);
	}

	for (nBuf = 0; nBuf < (cmdInfo->NumberInputBuffers + cmdInfo->NumberOutputBuffers); nBuf++) {
		MME_DataBuffer_t *buf = cmdInfo->DataBuffers_p[nBuf];
		for (nPage = 0; nPage < buf->NumberOfScatterPages; nPage++) {
			MME_ScatterPage_t *page = &buf->ScatterPages_p[nPage];

			if (page->Size == 0) { // Even MME doesnot translate Zero sized Pages
				continue;
			}
			p            = (unsigned long) page->Page_p;
			UnMap(p, page->Size);
		}
	}
}



static void RemapCmdPointers(MME_UserSpaceTransformer_t *user, MME_Command_t *cmdInfo, bpa2_region_mmap_t *map)
{
	unsigned long         p;
	MME_UINT              nBuf, nPage;
	MME_CommandStatus_t  *status = &cmdInfo->CmdStatus;

	/* Remap the physical addresses to Virtual mmaped addresses for this cmd */
	if (cmdInfo->ParamSize) {
		p                = (unsigned long) cmdInfo->Param_p;
		cmdInfo->Param_p = (MME_GenericParams_t) TranslateBPA2MMap(user, map, p, cmdInfo->ParamSize);
	}
	if (status->AdditionalInfoSize) {
		p                        = (unsigned long) status->AdditionalInfo_p;
		status->AdditionalInfo_p = (MME_GenericParams_t) TranslateBPA2MMap(user, map, p, status->AdditionalInfoSize);
	}

	for (nBuf = 0; nBuf < (cmdInfo->NumberInputBuffers + cmdInfo->NumberOutputBuffers); nBuf++) {
		MME_DataBuffer_t *buf = cmdInfo->DataBuffers_p[nBuf];
		for (nPage = 0; nPage < buf->NumberOfScatterPages; nPage++) {
			MME_ScatterPage_t *page = &buf->ScatterPages_p[nPage];

			if (page->Size == 0) { // Even MME doesnot translate Zero sized Pages
				continue;
			}
			p            = (unsigned long) page->Page_p;
			page->Page_p = (void *) TranslateBPA2MMap(user, map, p, page->Size);
		}
	}
}

static void release_msg_cmdId(message_queue_t *msg_queue, MME_CommandId_t CmdId)
{
	int val, word_idx = 0, bit_idx = 32, element_idx;
	msg_pool_t *pool = msg_queue->pool;

	acc_mutex_lock(pool->Mutex);
	for (word_idx = 0; word_idx < (sizeof(pool->ElementUseMap) / sizeof(int)); word_idx++) {
		val = pool->ElementUseMap[word_idx];

		while (val) {
			bit_idx     = 31 - __CLZ(val);
			element_idx = ((word_idx << 5) + bit_idx);
			if (element_idx < msg_queue->ElementNb) {
				MME_CommandPacked_t        *cmd;
				cmd = (MME_CommandPacked_t *)((char *) &msg_queue->ElementBuffer[element_idx * msg_queue->ElementSize] + sizeof(
				                                      msg_header_t));
				if (cmd ->cmd_type == MME_PACKED_PROCESS) {
					MME_Command_t  *Command = &cmd->uPack.process.cmd;
					if (Command->CmdStatus.CmdId == CmdId) {
						/*  memory has been mapped, the un-map is needed  */
						UnmapCmdPointers(Command);
						/* ******************* */
						pool->ElementUseMap[word_idx] &=  ~(1U << bit_idx);
						acc_mutex_release(pool->Mutex);
						return;
					}
				}
			}
			val &=  ~(1U << bit_idx);
		}
	}
	acc_mutex_release(pool->Mutex);
}

static void message_receiver_task(void *arg)
{
	MME_UserSpaceTransformer_t *user = (MME_UserSpaceTransformer_t *) arg;
	int                         res;
	MME_CommandPacked_t        *cmd;

	DP("Starting message_receiver_task\n");
	/* Loop waiting for messages from Kernel and Reply Pack as a Ping action */
	while (1) {
		cmd = (MME_CommandPacked_t *) message_claim(user->msg_queue);
		if (cmd == NULL) {
			REPORT(stderr, "ERR>> No New Message Buffer Could be claimed, increase queue size \n");
			continue;
		}

		/* Just Set The Size, illustrating to the driver that this is the amount of memory for the cmd */
		cmd->size     = (sizeof(MME_CommandPacked_t) + MAX_CMDMESSAGE_SIZE);
		cmd->userData = (MME_UINT) user;

		res = ioctl(user->deviceFd, MME_STUB_IOC_RECEIVE, cmd);
		if (res == -EINTR) {
			continue; // Some reason the GDB issues Software interrupts and breaks this IOCTL
		} else if (res != 0) {
			REPORT(stderr, "ERR>> IOCTL MME_STUB_IOC_RECEIVE Failed, Internal Error %d\n", res);
			break;
		}

		switch (cmd->cmd_type) {
		case MME_PACKED_GETCAP:
			if (user->getTransformerCapabilityFunc) {
				MME_UINT              TransformerInfoSize = cmd->uPack.caps.capability.TransformerInfoSize;
				MME_GenericParams_t   TransformerInfo_p   = cmd->uPack.caps.capability.TransformerInfo_p;
				/* Translate to virtual address from mmap */
				DP("message_receiver_task>> GETCAPS %s\n", user->name);
				cmd->uPack.caps.capability.TransformerInfo_p = (MME_GenericParams_t) TranslateBPA2MMap(user, &map,
				                                                                                       (unsigned long) TransformerInfo_p,
				                                                                                       TransformerInfoSize);
				DP("message_receiver_task>> GETCAPS TranslateBPA2 Done\n");

				if (cmd->uPack.caps.capability.TransformerInfo_p == NULL) {
					cmd->uPack.caps.capability.TransformerInfoSize = 0;
				}

				/* Call Registered GetCapability function */
				cmd->uPack.caps.status = user->getTransformerCapabilityFunc(&cmd->uPack.caps.capability);
				/*  unnap the return pointer */
				if (cmd->uPack.caps.capability.TransformerInfo_p) {
					UnMap((unsigned long)cmd->uPack.caps.capability.TransformerInfo_p, TransformerInfoSize);
					DP("message_receiver_task>> GETCAPS %s Done err = %d\n", user->name, cmd->uPack.caps.status);
				}
			}
			break;

		case MME_PACKED_INIT:
			if (user->initTransformerFunc) {
				MME_UINT            initParamsLength = cmd->uPack.init.initParamsLength;
				MME_GenericParams_t initParams       = cmd->uPack.init.initParams;

				/* Translate to virtual address from mmap */
				DP("message_receiver_task>> InitParams TranslateBPA2 \n");
				initParams = (MME_GenericParams_t) TranslateBPA2MMap(user, &map, (unsigned long) initParams, initParamsLength);
				DP("message_receiver_task>> InitParams TranslateBPA2 Done\n");

				if (initParams == NULL) {
					initParamsLength = 0;
				}

				DP("message_receiver_task>> InitTransformer %s\n", user->name);
				cmd->uPack.init.status  = user->initTransformerFunc(initParamsLength,  initParams, (void **)&cmd->uPack.init.handle);
				DP("message_receiver_task>> InitTransformer %s Done handle %x err %d\n", user->name, cmd->uPack.init.handle,
				   cmd->uPack.init.status);
				/* un-map init area */
				UnMap((unsigned long)initParams, initParamsLength);
			}
			break;

		case MME_PACKED_TERM:
			if (user->termTransformerFunc) {
				DP("message_receiver_task>> TermTransformer %s\n", user->name);
				cmd->uPack.term.status = user->termTransformerFunc((void *) cmd->uPack.term.handle);
				DP("message_receiver_task>> TermTransformer %s Done err = %d\n", user->name, cmd->uPack.term.status);
			}
			break;

		case MME_PACKED_ABORT:
			if (user->abortFunc) {
				cmd->uPack.abort.status = user->abortFunc((void *) cmd->uPack.abort.handle, cmd->uPack.abort.commandId);
				DP("message_receiver_task>> AbortTransformer %s CmdId: 0x%x err = %d\n", user->name, cmd->uPack.abort.commandId, cmd->uPack.abort.status);
				if (MME_SUCCESS == cmd->uPack.abort.status) {
					// release the aborted message from our queue if aborted successfully in companion
					release_msg_cmdId(user->msg_queue, cmd->uPack.abort.commandId);
				}
#ifdef _DEBUG
				print_msg(user->msg_queue);
#endif
			}
			break;

		case MME_PACKED_PROCESS:
			if (user->processCommandFunc) {
				MME_Command_t        *cmdInfo = &cmd->uPack.process.cmd;
				MME_CommandCode_t     CmdCode = cmdInfo->CmdCode;

				RemapCmdPointers(user, cmdInfo, &map);

				/* Only Send Buffer Command processed in this context, all others posted to execution loop */
				if (CmdCode == MME_SEND_BUFFERS) {
					DP("message_receiver_task>> ProcessCmd: %s SEND BUFFERS\n", user->name);
					cmd->uPack.process.status = user->processCommandFunc((void *) cmd->uPack.process.handle, cmdInfo);
					if (cmd->uPack.process.status == MME_SUCCESS) {
						/* Successfully posted SEND BUFFERS need to be treated as deferred commands */
						cmd->uPack.process.status = MME_TRANSFORM_DEFERRED;
					}
					else if (cmd->uPack.process.status != MME_TRANSFORM_DEFERRED)
					{
						DP("message_receiver_task>> ProcessCmd SEND BUFFERS Done err = %d\n", cmd->uPack.process.status);
						UnmapCmdPointers(cmdInfo);
						message_release(user->msg_queue, cmd);
					}
				} else {
					res = -EINVAL; // Fake an error to prevent a callback generation for these commands
					message_send(user->msg_queue, cmd);
				}
			}
			break;
		default:
			res = -EINVAL;
			REPORT(stderr, "ERR>> Unhandled CMD Type %d, skipped\n", cmd->cmd_type);
			break;
		}

		/* Send Callback of completion */
		if (res == 0) {
			cmd->event = MME_COMMAND_COMPLETED_EVT;
			res = ioctl(user->deviceFd, MME_STUB_IOC_COMPLETED, cmd);
			if (MME_PACKED_PROCESS != cmd->cmd_type) {
				message_release(user->msg_queue, cmd);
			}
			if (0 != res)	{
				REPORT(stderr, "ERR>> IOCTL MME_STUB_IOC_COMPLETED Failed, Internal Error %d\n", res);
				break;
			}
		}
	}
}

static void execution_loop_task(void *arg)
{
	MME_UserSpaceTransformer_t *user = (MME_UserSpaceTransformer_t *) arg;
	MME_CommandPacked_t        *cmd;
	int                         res;

	while (1) {
		MME_Command_t        *cmdInfo;

		/* Read the New Command Message */
		cmd = message_receive(user->msg_queue);

		if (cmd == NULL) {
			REPORT(stderr, "ERR>> NULL Command Provided, Internal Error\n");
			continue;
		}

		cmdInfo = &cmd->uPack.process.cmd;

		DP("execution_loop_task>> ProcessCmd %s -> %s\n", user->name,
		   (cmd->uPack.process.cmd.CmdCode == MME_TRANSFORM) ? "TRANSFORM" : "SETGLOBAL");
		cmd->uPack.process.status = user->processCommandFunc((void *) cmd->uPack.process.handle, cmdInfo);

		if ( cmd->uPack.process.status != MME_SUCCESS &&
			cmd->uPack.process.status != MME_TRANSFORM_DEFERRED )
		{
			DP("execution_loop_task>> ProcessCmd Transform/SetGbl Done err = %d\n", cmd->uPack.process.status);
		}

		/* Send Callback of completion */
		cmd->event = MME_COMMAND_COMPLETED_EVT;
		res        = ioctl(user->deviceFd, MME_STUB_IOC_COMPLETED, cmd);
		if (MME_TRANSFORM_DEFERRED != cmd->uPack.process.status) {
			UnmapCmdPointers(cmdInfo);
			message_release(user->msg_queue, cmd);
		}
		if (0 != res)	{
			REPORT(stderr, "ERR>> ExecutionLoop IOCTL MME_STUB_IOC_COMPLETED Failed, Internal Error %d\n", res);
			break;
		}
	}
}


