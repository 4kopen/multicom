/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 


#include <linux/slab.h>
#include <asm/errno.h>

#include "stub_user_ioctl.h"
#include "stub_message_queue.h"
#include "stub_user_process.h"
#include "stub_mme_transformer.h"

static const stub_sem_id_t CommandToSemIdMap[STUB_MAX_COMMANDS] = {
	SEM_MANAGER_THREAD,          /* STUB_GETCAPABILITY           */
	SEM_MANAGER_THREAD,          /* STUB_INIT_TRANSFORMER        */
	SEM_MESSAGE_RECEIVER_THREAD, /* STUB_PROCESS_SENDBUFFER      */
	SEM_EXECUTION_LOOP,          /* STUB_PROCESS_TRANSFORMPARAMS */
	SEM_MESSAGE_RECEIVER_THREAD, /* STUB_ABORT_CMD               */
	SEM_MANAGER_THREAD           /* STUB_TERM_TRANSFORMER        */
};

static MME_ERROR stub_execute_cmd(int minor, stub_command_t type, void *arg1, void *arg2, void *arg3)
{
	MME_ERROR              status = MME_NOT_IMPLEMENTED;
	stub_CommandMessage_t *msg;
	message_queue_t       *queue;
	struct semaphore      *sem;

	if (IsKilled(minor)) {
		printk("ERR>> stub_execute_cmd: Killed, Exiting\n");
		goto err_exit;
	}

	sem = GetSemaphoreByMinor(minor, CommandToSemIdMap[type]);
	if (sem == NULL) {
		printk("ERR>> stub_execute_cmd: Semaphore not found\n");
		goto err_exit;
	}

	queue = GetQueueByMinor(minor);
	if (queue == NULL) {
		printk("ERR>> stub_execute_cmd: Queue not found\n");
		goto err_exit;
	}

	/* Claim a new message from queue */
	msg = message_claim(queue);
	if (msg == NULL) {
		printk("ERR>> stub_execute_cmd: Message claim failed\n");
		goto err_exit;
	}

	/* Load the Message with the arguments */
	msg->type   = type;
	msg->status = status;
	msg->sem    = sem;
	msg->arg1   = arg1;
	msg->arg2   = arg2;
	msg->arg3   = arg3;

	/* Send the message to the IOCTL */
	message_send(queue, msg);

	/* Now Start waiting for a ACK from user space on completion of this command */
	if (down_interruptible(sem)) {
		printk("ERR>> stub_execute_cmd: Wait Interrupted\n");
		goto err_exit_release;
	}

	if (IsKilled(minor)) {
		printk("ERR>> stub_execute_cmd: Killed, Exiting\n");
		goto err_exit_release;
	}

	/* ACK Received -> Read the status back */
	status = msg->status;

err_exit_release:
	/* The message needs to be returned back to the pool */
	message_release(queue, msg);

err_exit:
	if (MME_SUCCESS != status && MME_TRANSFORM_DEFERRED != status) {
		printk("ERR>> stub_execute_cmd: Returned Error %d\n", status);
	}
	return status;
}

static MME_ERROR stub_AbortCommand(int minor, void *context, MME_CommandId_t commandId)
{
	return stub_execute_cmd(minor, STUB_ABORT_CMD, (void *) context, (void *) commandId, (void *) NULL);
}

static MME_ERROR stub_GetTransformerCapability(int minor, MME_TransformerCapability_t *capability)
{
	return stub_execute_cmd(minor, STUB_GETCAPABILITY, (void *) capability, (void *) NULL, (void *) NULL);
}

static MME_ERROR stub_InitTransformer(int minor, MME_UINT initParamsLength, MME_GenericParams_t initParams,
                                      void **context)
{
	int ret = stub_execute_cmd(minor, STUB_INIT_TRANSFORMER, (void *) initParamsLength, (void *) initParams, (void *) context);
/*  return specific success flag to discriminate userspace init */
	return ((ret==MME_SUCCESS) ?  MME_USERSPACE_INIT_SUCCESS : ret);
}

static MME_ERROR stub_ProcessCommand(int minor, void *context, MME_Command_t *commandInfo)
{
	MME_ERROR          err      = MME_SUCCESS;
	stub_command_t     cmdtype  = (MME_SEND_BUFFERS == commandInfo->CmdCode) ? STUB_PROCESS_SENDBUFFER :
	                              STUB_PROCESS_TRANSFORMPARAMS;

	/* Send Buffer command and Transform/SetGlobal all called from different contexts,
	 * and therefore need to wait on different semaphores for call back completion.
	 */
	err = stub_execute_cmd(minor, cmdtype, (void *) context, (void *) commandInfo, (void *) NULL);

	if ((STUB_PROCESS_SENDBUFFER == cmdtype) && (err == MME_SUCCESS)) {
		err = MME_TRANSFORM_DEFERRED; // Send buffer are treated as deferred commands
	}

	return err;
}

static MME_ERROR stub_TermTransformer(int minor, void *context)
{
	return stub_execute_cmd(minor, STUB_TERM_TRANSFORMER, (void *) context, (void *) NULL, (void *) NULL);
}

#define AbortCommand(n)  \
static MME_ERROR AbortCommand_##n(void *context, MME_CommandId_t commandId)\
{\
    return stub_AbortCommand(n, context, commandId); \
}

#define GetTransformerCapability(n)  \
static MME_ERROR GetTransformerCapability_##n(MME_TransformerCapability_t * capability)\
{\
    return stub_GetTransformerCapability(n, capability);\
}

#define InitTransformer(n)  \
static MME_ERROR InitTransformer_##n(MME_UINT initParamsLength, MME_GenericParams_t initParams, void **context)\
{\
    return stub_InitTransformer(n, initParamsLength, initParams, context);\
}

#define ProcessCommand(n)  \
static MME_ERROR ProcessCommand_##n(void *context, MME_Command_t * commandInfo)\
{\
    return stub_ProcessCommand (n, context, commandInfo);\
}

#define TermTransformer(n)  \
static MME_ERROR TermTransformer_##n(void *context)\
{\
    return stub_TermTransformer (n, context);\
}

#define TransformerFn(n)  \
    AbortCommand(n) \
    GetTransformerCapability(n) \
    InitTransformer(n) \
    ProcessCommand(n) \
    TermTransformer(n)

TransformerFn(0)
TransformerFn(1)
TransformerFn(2)
TransformerFn(3)
TransformerFn(4)
TransformerFn(5)
TransformerFn(6)
TransformerFn(7)
TransformerFn(8)
TransformerFn(9)
TransformerFn(10)
TransformerFn(11)
TransformerFn(12)
TransformerFn(13)
TransformerFn(14)
TransformerFn(15)
TransformerFn(16)
TransformerFn(17)
TransformerFn(18)
TransformerFn(19)
TransformerFn(20)
TransformerFn(21)
TransformerFn(22)
TransformerFn(23)
TransformerFn(24)
TransformerFn(25)
TransformerFn(26)
TransformerFn(27)
TransformerFn(28)
TransformerFn(29)
TransformerFn(30)
TransformerFn(31)

typedef struct {
	char         Name[MME_MAX_TRANSFORMER_NAME];
	MME_ERROR(* AbortCommand)(void *context, MME_CommandId_t commandId);
	MME_ERROR(* GetTransformerCapability)(MME_TransformerCapability_t *capability);
	MME_ERROR(* InitTransformer)(MME_UINT initParamsLength, MME_GenericParams_t initParams, void **context);
	MME_ERROR(* ProcessCommand)(void *context, MME_Command_t *commandInfo);
	MME_ERROR(* TermTransformer)(void *context);
} tTransformerRegistry;

#undef  ENTRY
#define ENTRY(n)      { "", \
                        AbortCommand_##n, \
                        GetTransformerCapability_##n, \
                        InitTransformer_##n, \
                        ProcessCommand_##n, \
                        TermTransformer_##n }

static tTransformerRegistry Registry[MAX_TRANSFORMER_REGISTRATION] = {
	ENTRY(0), ENTRY(1), ENTRY(2), ENTRY(3),
	ENTRY(4), ENTRY(5), ENTRY(6), ENTRY(7),
	ENTRY(8), ENTRY(9), ENTRY(10), ENTRY(11),
	ENTRY(12), ENTRY(13), ENTRY(14), ENTRY(15),
	ENTRY(16), ENTRY(17), ENTRY(18), ENTRY(19),
	ENTRY(20), ENTRY(21), ENTRY(22), ENTRY(23),
	ENTRY(24), ENTRY(25), ENTRY(26), ENTRY(27),
	ENTRY(28), ENTRY(29), ENTRY(30), ENTRY(31)
};

/* Routines to Register/Unregister the MME STUB Transformer in UserSpace */
int mme_stub_register_transformer(int minor, char *name)
{
	MME_ERROR err;

	if (minor >= MAX_TRANSFORMER_REGISTRATION) {
		return -EFAULT;
	}

	err = MME_RegisterTransformer(name,
	                              Registry[minor].AbortCommand,
	                              Registry[minor].GetTransformerCapability,
	                              Registry[minor].InitTransformer,
	                              Registry[minor].ProcessCommand,
	                              Registry[minor].TermTransformer);
	if (err == 0) {
		strncpy(Registry[minor].Name, name, MME_MAX_TRANSFORMER_NAME - 1);
		Registry[minor].Name[MME_MAX_TRANSFORMER_NAME - 1] = 0;
	}

	if (err != MME_SUCCESS)	{
		printk("%s RegisterTransformer failed\n", name);
		return -EFAULT;
	} else {
		printk("%s Transformer Registered\n", name);
	}

	return 0;
}

int mme_stub_unregister_transformer(int minor, char *name)
{
		if (MME_SUCCESS != MME_DeregisterTransformer(name)) {
			printk("MME_DeregisterTransformer failed :: %s\n", name);
			return -ENOTTY;
		}
		if (minor < MAX_TRANSFORMER_REGISTRATION) {
			Registry[minor].Name[0] = 0;
		}
		printk("Unregistered %s Transformer\n", name);
	
	return 0;
}
