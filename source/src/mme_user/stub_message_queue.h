/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 



#ifndef MESSAGE_QUEUE_H_
#define MESSAGE_QUEUE_H_

#define MAX_MSG_QUEUE_LENGTH		64

#include <linux/semaphore.h>
#include <linux/mutex.h>

typedef struct {
	unsigned int    ElementUseMap[MAX_MSG_QUEUE_LENGTH / (8 * sizeof(int))];
	struct mutex   *Mutex;
} msg_pool_t;

typedef struct msg_node_s {
	struct msg_node_s *next;
} msg_header_t;

typedef struct msq_queue_s {
	msg_header_t      *head;
	msg_header_t      *tail;
	struct semaphore  *sem;
	struct mutex      *mutex;
} msg_fifo_t;

// Quick implementation of message queue.
typedef struct message_queue_s {
	int              ElementSize;
	int              ElementNb;
	char            *ElementBuffer;
	msg_pool_t      *pool;
	msg_fifo_t      *fifo;
} message_queue_t;

message_queue_t *message_create_queue(int element_size, int list_length);
void             message_delete_queue(message_queue_t *msg_queue);
void             message_release(message_queue_t *queue, void *msg);
void            *message_claim(message_queue_t *msg_queue);
void            *message_receive(message_queue_t *queue);
void             message_send(message_queue_t *queue, void *message);

#endif /* MESSAGE_QUEUE_H_ */
