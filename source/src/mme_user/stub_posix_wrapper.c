/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#define __OS21_POSIX_WRAPPER__

#include "stub_posix_wrapper.h"
#include <sys/prctl.h>

#ifndef MAX
#define MAX(a,b)    ((a) > (b) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b)    ((a) < (b) ? (a) : (b))
#endif

#define __CLZ   __builtin_clz

void kernel_initialize(void *x)
{
	// Dummy Call, Kernel already initialized
}

void kernel_start(void)
{
	// Dummy Call, Kernel already running
}

void kernel_timeslice(int on)
{
	// Linux Kernel always have timeslicing enabled for same priority
}

osclock_t time_ticks_per_sec(void)
{
	return (1000000); // Ticks Per Sec = 1e6 us
}

//#include <time.h>
#include <sys/timeb.h>


osclock_t time_now(void)
{
#if 0
	osclock_t       res = 0;
	struct timespec tp;

	clock_gettime(CLOCK_REALTIME, &tp);

	res  = ((unsigned int) tp.tv_sec * 10000000); //Will always overflow every 36min
	res += (((long long) tp.tv_nsec * 2147484) >> 32) * 2;  //nsec to usec

	return res; //usec resolution
#else
	struct timeb tstruct;

	ftime(&tstruct);              // start time ms
	return tstruct.time * 1000 + tstruct.millitm;
#endif
}

osclock_t time_plus(const osclock_t time1, const osclock_t time2)
{
	return (time1 + time2);
}

osclock_t time_minus(const osclock_t time1, const osclock_t time2)
{
	return (time1 - time2);
}

void osclock2timespec(const osclock_t tclk, struct timespec *ptspec)
{
	ptspec->tv_sec  = (((long long) tclk * 2147U) >> 32) * 2;  //usec to sec
	ptspec->tv_nsec = tclk - ((unsigned long) ptspec->tv_sec * 1000000);//residual usec
	ptspec->tv_nsec *= 1000; // residual nsec
}

mutex_t *acc_mutex_create(void)
{
	pthread_mutex_t  *Mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));
	pthread_mutexattr_t mattr;
	int err = -1;

	if (Mutex != NULL) {
		err  = pthread_mutexattr_init(&mattr);
		err |= pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_PRIVATE);
		err |= pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
		err |= pthread_mutex_init(Mutex, &mattr);
		err |= pthread_mutexattr_destroy(&mattr);
	}

	if (err) {
		acc_mutex_delete(Mutex);
		Mutex = NULL;
	}

	return Mutex;
}

void acc_mutex_lock(mutex_t *mutex)
{
	if (mutex != NULL) {
		pthread_mutex_lock(mutex);
	}
}

int acc_mutex_release(mutex_t *mutex)
{
	if (mutex != NULL) {
		pthread_mutex_unlock(mutex);
	}

	return OS21_SUCCESS;
}

int acc_mutex_delete(mutex_t *mutex)
{
	int Result = OS21_FAILURE;
	if (mutex != NULL) {
		Result = OS21_SUCCESS;

		pthread_mutex_destroy(mutex);

		free(mutex);
	}
	return Result;
}

semaphore_t *semaphore_create_fifo(int val)
{
	sem_t *Semaphore = (sem_t *) malloc(sizeof(sem_t));
	if (Semaphore != NULL) {
		sem_init(Semaphore, 0, val);
	}
	return Semaphore;
}

int semaphore_wait(semaphore_t *semaphore)
{
	int err;
	do {
		/* sem_wait can return from block on receiving interrupt signal so
		 * we need to loop till we actually come out from semaphore post */
		err = sem_wait(semaphore);
	} while ((err == -1) && (errno == EINTR));

	return OS21_SUCCESS;
}

int semaphore_wait_timeout(semaphore_t *semaphore, const osclock_t *timeout)
{
	int			Result = OS21_SUCCESS;

	if (semaphore == NULL) {
		Result = OS21_FAILURE;
	} else if (timeout == TIMEOUT_INFINITY) {
		semaphore_wait(semaphore);
	} else if (timeout == TIMEOUT_IMMEDIATE) {
		Result = sem_trywait(semaphore);
		Result = (Result == -1 && errno == EAGAIN) ? OS21_FAILURE : OS21_SUCCESS;
	} else {
		struct timespec tspec;

		osclock2timespec(*timeout, &tspec);

		Result = sem_timedwait(semaphore, &tspec);
		Result = (Result == -1 && errno == EAGAIN) ? OS21_FAILURE : OS21_SUCCESS;
	}

	return Result;
}

void semaphore_signal(semaphore_t *semaphore)
{
	if (semaphore != NULL) {
		sem_post(semaphore);
	}
}

int semaphore_delete(semaphore_t *semaphore)
{
	int Result = OS21_FAILURE;

	if (semaphore != NULL) {
		Result = OS21_SUCCESS;
		sem_destroy(semaphore);
		free(semaphore);
	}

	return Result;
}

event_group_t *event_group_create(int option)
{
	int                err = -1;
	event_group_t     *event = (event_group_t *) malloc(sizeof(event_group_t)) ;
	pthread_condattr_t cattr;

	if (event != NULL) {
		event->mask  = 0;
		event->mutex =  acc_mutex_create();

		err  = (event->mutex == NULL) ? -1 : 0;
		err |= pthread_condattr_init(&cattr);
		err |= pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_PRIVATE);
		err |= pthread_cond_init(&event->cond, &cattr);
		err |= pthread_condattr_destroy(&cattr);
	}

	if (err) {
		event_group_delete(event);
		event = NULL;
	}

	return event;
}

int event_group_delete(event_group_t *event_group)
{
	int Result = OS21_FAILURE;
	if (event_group != NULL) {
		Result = OS21_SUCCESS;
		pthread_cond_destroy(&event_group->cond);
		acc_mutex_delete(event_group->mutex);
		free(event_group);
	}
	return Result;
}

int event_wait_any(event_group_t *event_group, const unsigned int in_mask, unsigned int *out_mask,
                   const osclock_t *timeout)
{
	int Result = OS21_SUCCESS;

	(void) out_mask;  // Unused Args - Not Implemented yet

	acc_mutex_lock(event_group->mutex);

	while (0 == (in_mask & event_group->mask)) {
		pthread_cond_wait(&event_group->cond, event_group->mutex);
	}

	acc_mutex_release(event_group->mutex);

	return Result;
}

void event_post(event_group_t *event_group, int value)
{
	acc_mutex_lock(event_group->mutex);
	event_group->mask |= value;
	pthread_cond_broadcast(&event_group->cond);
	acc_mutex_release(event_group->mutex);
}

void event_clear(event_group_t *event_group, int value)
{
	acc_mutex_lock(event_group->mutex);
	event_group->mask &= ~value;
	acc_mutex_release(event_group->mutex);
}

static msg_pool_t *allocate_pool(int list_length)
{
	int              i;
	msg_pool_t      *pool  = (msg_pool_t *) malloc(sizeof(msg_pool_t));
	mutex_t         *mutex = acc_mutex_create();

	if (pool != NULL && mutex != NULL) {
		memset(pool, 0, sizeof(msg_pool_t));//Clear All Use Flags

		pool->Mutex = mutex;

		/* Set all bits in Map above list_length to 1 to indicate unavailable locations */
		for (i = list_length; i < sizeof(pool->ElementUseMap) * 8; i++) {
			int  word_idx, bit_idx;

			word_idx = i >> 5;
			bit_idx	 = i & 0x1F;
			pool->ElementUseMap[word_idx] |= (1U << bit_idx);
		}
	} else {
		free(pool);
		pool = NULL;
		acc_mutex_delete(mutex);
	}

	return pool;
}

static void free_pool(msg_pool_t *pool)
{
	if (pool != NULL)	{
		acc_mutex_delete(pool->Mutex);
		free(pool);
	}
}

static __inline int get_zero_bit(unsigned int x)
{
	int y = (int) x; // Signed Number type cast to find whether bit 32 is set

	y = (y > -2) ? (32 - __CLZ(x)) : (31 - __CLZ(~x));

	return y;
}

static int pool_claim(msg_pool_t *pool)
{
	int word_idx = 0, bit_idx = 32;

	acc_mutex_lock(pool->Mutex);
	for (word_idx = 0; word_idx < (sizeof(pool->ElementUseMap) / sizeof(int)); word_idx++) {
		bit_idx = get_zero_bit(pool->ElementUseMap[word_idx]);

		if (bit_idx < 32) {
			pool->ElementUseMap[word_idx] |= (1U << bit_idx); // mark this element as used
			break;
		}
	}
	acc_mutex_release(pool->Mutex);

	return ((word_idx << 5) + bit_idx);
}

static void pool_release(msg_pool_t *pool, int element_idx)
{
	int word_idx, bit_idx;

	word_idx	= element_idx >> 5; // Element Idx / 32
	bit_idx	    = element_idx & 0x1F;

	acc_mutex_lock(pool->Mutex);
	pool->ElementUseMap[word_idx] &= ~(1U << bit_idx); // Clear the bit corresponding to element idx in Map
	acc_mutex_release(pool->Mutex);
}

static msg_fifo_t *allocate_fifo(void)
{
	msg_fifo_t   *fifo  = (msg_fifo_t *) malloc(sizeof(msg_fifo_t));
	mutex_t      *mutex = acc_mutex_create();
	semaphore_t *sem   = semaphore_create_fifo(0);

	if (fifo != NULL && mutex != NULL && sem != NULL) {
		fifo->mutex = mutex;
		fifo->sem   = sem;
		fifo->head  = NULL;
		fifo->tail  = NULL;
	} else {
		free(fifo);
		fifo = NULL;
		acc_mutex_delete(mutex);
		semaphore_delete(sem);
	}

	return fifo;
}

static void free_fifo(msg_fifo_t *fifo)
{
	if (fifo != NULL)	{
		acc_mutex_delete(fifo->mutex);
		semaphore_delete(fifo->sem);
		free(fifo);
	}
}

static void add_fifo(msg_fifo_t *fifo, void *message)
{
	/* Multiple Send need to be handled one after the another */
	msg_header_t *hdr = (msg_header_t *)((char *) message - sizeof(msg_header_t));

	hdr->next = NULL;

	acc_mutex_lock(fifo->mutex);
	/* Push new node to the tail of fifo */
	if (fifo->tail != NULL) {
		fifo->tail->next = hdr;
		fifo->tail       = hdr;
	} else {
		fifo->head = hdr;
		fifo->tail = hdr;
	}
	acc_mutex_release(fifo->mutex);

	// Signal one message available in queue
	semaphore_signal(fifo->sem);
}

static void *remove_fifo(msg_fifo_t *fifo)
{
	msg_header_t *hdr;

	if (semaphore_wait(fifo->sem)) {
		// Signal Received, return No Message Received
		return NULL;
	}

	acc_mutex_lock(fifo->mutex);
	/* Pop out the head node from Fifo */
	hdr        = fifo->head;
	fifo->head = hdr->next;
	if (hdr->next == NULL) {
		fifo->tail = NULL;
	}
	acc_mutex_release(fifo->mutex);

	return (void *)((char *) hdr + sizeof(msg_header_t));
}

/*
 * Delete a message_queue
 */
void message_delete_queue(message_queue_t *msg_queue)
{
	/* ToDo :: Ideally this must ensure all threads waiting for messages do not wait on freed semaphores/mutexes */
	if (msg_queue != NULL) {
		free_pool(msg_queue->pool);
		free_fifo(msg_queue->fifo);
		free(msg_queue->ElementBuffer);
		free(msg_queue);
	}
}

/*
 * Create a message_queue
 */
message_queue_t *message_create_queue(int element_size, int list_length)
{
	char              *buffer;
	message_queue_t   *msg_queue;
	msg_pool_t        *pool;
	msg_fifo_t        *fifo;

	if (list_length > MAX_MSG_QUEUE_LENGTH) {
		return NULL;
	}

	element_size += sizeof(msg_header_t);// Add space for Fifo Next Ptr

	msg_queue = (message_queue_t *) malloc(sizeof(message_queue_t));
	buffer	  = (char *) malloc(element_size * list_length);
	pool      = allocate_pool(list_length);
	fifo      = allocate_fifo();

	if (msg_queue != NULL && buffer != NULL && pool != NULL && fifo != NULL) {
		msg_queue->ElementSize	    = element_size;
		msg_queue->ElementNb        = list_length;
		msg_queue->ElementBuffer    = buffer;
		msg_queue->pool             = pool;
		msg_queue->fifo             = fifo;
	} else {
		free_pool(pool);
		free_fifo(fifo);
		free(buffer);
		free(msg_queue);
		msg_queue = NULL;
	}

	return msg_queue;
}

void message_release(message_queue_t *msg_queue, void *msg_addr)
{
	int  element_idx;
	char *msg = (char *) msg_addr - sizeof(msg_header_t);


#ifdef _DEBUG
	if (msg < msg_queue->ElementBuffer || msg > (msg_queue->ElementBuffer + (msg_queue->ElementSize * ElementNb)))	{
		printk("message_release : Invalid Message ptr %x", msg_addr);
		return;
	}
#endif

	// Find the element idx for this message
	element_idx = ((char *)msg - msg_queue->ElementBuffer) / msg_queue->ElementSize;

	pool_release(msg_queue->pool, element_idx);
}

/*
 * Claim the next message from a message_queue's free message Q.
 */
void *message_claim(message_queue_t *msg_queue)
{
	char *msg = NULL;

	int element_idx = pool_claim(msg_queue->pool);

	if (element_idx < msg_queue->ElementNb) {
		msg = (char *) &msg_queue->ElementBuffer[element_idx * msg_queue->ElementSize] + sizeof(msg_header_t);
	}

	return msg;
}

void message_send(message_queue_t *queue, void *message)
{
	add_fifo(queue->fifo, message);
}

void *message_receive(message_queue_t *queue)
{
	return remove_fifo(queue->fifo);
}

void *message_claim_timeout(message_queue_t *msg_queue, const osclock_t *time)
{
	(void) time; // No Timeout Implementation yet
	return message_claim(msg_queue);
}

int task_priority(task_t *task)
{
	int Result = 0;    /* Not a SCHED_RR, so lowest priority */
	struct sched_param  sparam;
	int                 policy;

	task   = (task == NULL) ?  task_id() : task;

	pthread_getschedparam((pthread_t)task, &policy, &sparam);

	if (policy == SCHED_RR) {
		Result = sparam.sched_priority;
	}

	return Result;
}

int task_priority_set(task_t *task, int priority)
{
	struct sched_param  sparam;
	int                 policy;
	int                 Result;

	memset(&sparam, 0, sizeof(sparam));

	priority = MAX(priority, MIN_USER_PRIORITY);
	priority = MIN(priority, MAX_USER_PRIORITY);
	task     = (task == NULL) ?  task_id() : task;
	policy   = (priority == 0) ? SCHED_OTHER : SCHED_RR;

	sparam.sched_priority = priority;

	Result = pthread_setschedparam((pthread_t) task, policy, &sparam);

	Result = (Result != 0) ? OS21_FAILURE : OS21_SUCCESS;

	return Result;
}


typedef struct {
	char    name[16];
	void (*funcp)(void *param);
	void    *param;
} task_desc_t;

static void TaskBaseRoutine(void *param)
{
	task_desc_t *p = (task_desc_t *) param;

	// Set the name of the thread
	prctl(PR_SET_NAME, p->name, 0, 0, 0);

	p->funcp(p->param); // Evoke the desired thread handler

	pthread_exit(p->param);
	free(p);
}

task_t *task_create(void (*funcp)(void *param), void *param, size_t stack_size, int priority, const char *name,
                    task_flags_t flags)
{
	int                 err;
	pthread_t           t = (pthread_t) NULL;
	pthread_attr_t      attr;
	struct sched_param  sparam;
	int                 policy;

	priority = MAX(priority, MIN_USER_PRIORITY);
	priority = MIN(priority, MAX_USER_PRIORITY);
	policy   = (priority == 0) ? SCHED_OTHER : SCHED_RR;

	err  = pthread_attr_init(&attr);
	err |= pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	if (stack_size > 0) {
		err |= pthread_attr_setstacksize(&attr, stack_size);
	}

	err |= pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	err |= pthread_attr_setschedpolicy(&attr, policy);
	err |= pthread_attr_getschedparam(&attr, &sparam);
	sparam.sched_priority = sched_get_priority_min(SCHED_RR) - 1 + priority;

	err |= pthread_attr_setschedparam(&attr, &sparam);
	err |= pthread_attr_getschedpolicy(&attr, &policy);

	if (!err) {
		task_desc_t *desc = malloc(sizeof(task_desc_t));
		if (desc != NULL) {
			strncpy(desc->name, name, 15);
			desc->name[15] = 0;
			desc->funcp = funcp;
			desc->param = param;
			err = pthread_create(&t, &attr, (void *(*)(void *)) TaskBaseRoutine, desc);
		} else {
			err = pthread_create(&t, &attr, (void *(*)(void *)) funcp, param);
		}
	}

	err |= pthread_attr_destroy(&attr);

	return (task_t *)t;
}

int task_delete(task_t *task)
{
	/* Nothing To Do - Task should have been stopped in task_wait */
	(void) task;
	return OS21_SUCCESS;
}

int task_wait(task_t **tasklist, int ntasks, osclock_t *timeout)
{
	(void) timeout; // Not Implemented Yet

	while (ntasks) {
		pthread_join((pthread_t)*tasklist, NULL); // Wait till task exits/canceled
		ntasks--;
		tasklist++;
	}
	return OS21_SUCCESS;
}

int task_kill(task_t *task, int status, int flags)
{
	(void) status;
	(void) flags;
	pthread_cancel((pthread_t)task);
	return OS21_SUCCESS;
}

task_t *task_id(void)
{
	return (task_t *) pthread_self();
}

void task_delay(osclock_t delay)
{
	usleep(delay);
}

void task_delay_until(osclock_t this_time)
{
	osclock_t  cur_time = time_now();

	if (cur_time < this_time) {
		// Wait until this_time is achieved
		task_delay(time_minus(this_time, cur_time));
	}
}

int task_suspend(task_t *task)
{
	(void) task; // Not implemented
	return OS21_SUCCESS;
}

int task_resume(task_t *task)
{
	(void) task; // Not Implemented
	return OS21_SUCCESS;
}

int  task_exit(void *Param)
{
	pthread_exit(Param);
	return OS21_SUCCESS;
}
