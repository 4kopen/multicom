/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 
 



#include <asm/errno.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/bpa2.h>

#include "stub_user_ioctl.h"
#include "stub_user_process.h"
#include "stub_message_packing.h"
#include "stub_mme_transformer.h"

#define TERM_WAIT_TIME_MSEC       500

int stub_user_free_instance(stub_instance_t *instance)
{
	int nsemp;
	int err = 0;
	if (instance != NULL)	{
		if (instance->isInit) {
			printk("stub_user_free_instance: %s Is Init, %d open handles, need to kill it/them\n", instance->name,
			       instance->isInit);

			/* The Running transformer did not receive Term, so Multicom threads may get stuck
			   Post all Semaphores so that they know we are dying and act accordingly */
			for (nsemp = 0; nsemp < MAX_SIMULTANEOUS_COMMAND_THREAD; nsemp++) {
				up(instance->sem[nsemp]);
			}
			printk("stub_user_free_instance: Waiting for threads to callback\n");
			msleep(TERM_WAIT_TIME_MSEC);
			/* We expect all threads now know we are dying, and do not hold any message
			 * so that we can start free-up of the messages */
		}

		/* Free up the Message Queue */
		message_delete_queue(instance->queue);
		/* Free-up the semaphores */
		for (nsemp = 0; nsemp < MAX_SIMULTANEOUS_COMMAND_THREAD; nsemp++) {
			kfree(instance->sem[nsemp]);
		}

		/* Try to free up the Registered Transform Stub */
		err = mme_stub_unregister_transformer(instance->minor, instance->name);
		if (err) {
			printk("Unregistered %s Transformer failed, err = %d\n", instance->name, err);
		}

		/* Release this instance */
		kfree(instance);
	}
	return err;
}
extern stub_instance_t init_instance;

stub_instance_t *stub_user_allocate_instance(int minor)
{
	// Allocate a Stub Instance and initialize it
	int i;
	stub_instance_t   *instance = (stub_instance_t *) kmalloc(sizeof(stub_instance_t), GFP_KERNEL);

	if (instance != NULL) {
		int nsemp;

		/* Clear Out all pointers inside */
		memset(instance, 0, sizeof(stub_instance_t));

		instance->minor = minor;

		for (nsemp = 0; nsemp < MAX_SIMULTANEOUS_COMMAND_THREAD; nsemp++) {
			instance->sem[nsemp] = (struct semaphore *) kmalloc(sizeof(struct semaphore), GFP_KERNEL);
			if (instance->sem[nsemp] != NULL) {
				sema_init(instance->sem[nsemp], 0);   // Create Locked Semaphore
			} else {
				goto err_exit;
			}
		}

		instance->queue = message_create_queue(MAX_SIZE_PER_MESSAGE, MAX_MESSAGE_FIFO_LENGTH);
		if (instance->queue == NULL) 	{
			goto err_exit;
		}
		for (i=0; i<BPA_COUNT; i++) {
			instance->base[i] = init_instance.base[i];
			instance->size[i] = init_instance.size[i];
		}
	}
	return instance;

err_exit:
	stub_user_free_instance(instance);
	return NULL;
}

int stub_user_receive(stub_instance_t *instance, void *arg)
{
	int err;
	stub_CommandMessage_t *msg;

start:
	/* Get a new message from Queue and pack it to be returned to user space */
	msg = message_receive(instance->queue);

	if (msg == NULL) {
		/* Signal received -> Abort this IOCTL */
		printk("stub_user_receive: message_receive() failed, probably signalled\n");
		return  -EINTR;
	}

	switch (msg->type) {
	case STUB_GETCAPABILITY:
		err = stub_pack_capability((MME_TransformerCapability_t *) msg->arg1, msg, arg);
		break;
	case STUB_INIT_TRANSFORMER:
		err = stub_pack_initTransformer((MME_UINT) msg->arg1, (MME_GenericParams_t) msg->arg2, msg, arg);
		break;
	case STUB_PROCESS_SENDBUFFER:
	case STUB_PROCESS_TRANSFORMPARAMS:
		err = stub_pack_processCommand((void *) msg->arg1, (MME_Command_t *) msg->arg2, msg, arg);
		break;
	case STUB_ABORT_CMD:
		err = stub_pack_abort((void *) msg->arg1, (MME_CommandId_t) msg->arg2, msg, arg);
		break;
	case STUB_TERM_TRANSFORMER:
		err = stub_pack_termTransformer((void *) msg->arg1, msg, arg);
		break;
	default:
		printk("Incorrect Message Type(%d) received on Queue. Skipped\n", msg->type);
		goto start;
	}

	if (err) 	{
		// Probably not enuf space or invalid user space pointer provided
		printk("stub_user_receive: MsgType %d failed with err %d\n", msg->type, err);
	}

	return err;
}

int stub_user_completed(stub_instance_t *instance, void *arg)
{
	int                    err;
	stub_CommandMessage_t *msg;

	err = stub_get_callbackData((MME_UINT *)&msg, arg);
	if (err || msg == NULL) {
		printk("stub_user_completed: %d Callback IOCTL Failed\n", err);
		return -EFAULT;
	}

	/* Stub Message Needs to be updated for status */
	switch (msg->type) {
	case STUB_GETCAPABILITY:
		err = stub_update_capability(&msg->status, (MME_TransformerCapability_t *) msg->arg1, arg);
		break;
	case STUB_INIT_TRANSFORMER:
		err = stub_update_initTransformer(&msg->status, (void **) msg->arg3, arg);
		if (!err) {
			instance->isInit++;
		}
		break;
	case STUB_PROCESS_TRANSFORMPARAMS:
	case STUB_PROCESS_SENDBUFFER:
		err = stub_update_processCommand(&msg->status, (MME_Command_t *) msg->arg2, arg);
		break;
	case STUB_ABORT_CMD:
		err = stub_update_abort(&msg->status, arg);
		break;
	case STUB_TERM_TRANSFORMER:
		err = stub_update_termTransformer(&msg->status, arg);
		if (!err) {
			instance->isInit--;
		}
		break;
	default:
		printk("Incorrect Message Type(%d) received on Queue.\n", msg->type);
		err = -EFAULT;
	}

	if (err) 	{
		// Probably not enuf space or invalid user space pointer provided
		printk("stub_user_completed: %d IOCTL Failed\n", err);
	} else if (msg->sem) {
		/* The calling thread was blocked on semaphore, trigger it to start running */
		up(msg->sem);
	}

	return err;
}

int stub_user_register(stub_instance_t *instance, void *arg)
{
	char *name  = instance->name;
	int  minor = instance->minor;
	int err = 0;
	MME_StubRegisterTransform_t  *reg = (MME_StubRegisterTransform_t *) arg;
	err += copy_from_user(name, reg->TransformerName, MME_MAX_TRANSFORMER_NAME);
#if 0
	int i;
	for(i = 0; i < BPA_COUNT; i++) {
		err += put_user(instance->base[i], &reg->base[i]);
		err += put_user(instance->size[i], &reg->size[i]);
	}
	if (err) {
		printk("stub_user_register: IOCTL Failed\n");
		return -EFAULT;
	}
#endif
	name[MME_MAX_TRANSFORMER_NAME] = '\0';

	/* Register MME Transformer In This Name */
	err =  mme_stub_register_transformer(minor, name);

	if (err)
	{
	    name[0] = '\0';
	}

	return err;
}

int stub_user_notify(stub_instance_t *instance, void *arg)
{
	int                    err;
	MME_Event_t            event;
	MME_ERROR              res;
	MME_CommandPacked_t   *process = (MME_CommandPacked_t *) arg;
	int                    minor = instance->minor;
	message_queue_t       *queue;
	MME_Command_t         *command ;

	queue = GetQueueByMinor(minor);
	if (queue == NULL) {
		printk("ERR>> stub_user_notify: Queue not found\n");
		return -EFAULT;
	}

	if (get_user(command, &(process->uPack.process.kcmd)) ||
	    get_user(event, &(process->event))                ||
	    get_user(res  , &(process->uPack.process.status))) {
		printk("stub_user_notify: Callback IOCTL Failed\n");
		return -EFAULT;
	}

	if (MME_COMMAND_COMPLETED_EVT == event) {
		err = stub_update_processCmdBuffers(command, arg);
		if (err) {
			return err;
		}
	}
    else
    {
        /* MME_NotifyHost is supposed to also update the command status */
        if (get_user(command->CmdStatus.State, &(process->uPack.process.cmd.CmdStatus.State)) ||
            get_user(command->CmdStatus.Error, &(process->uPack.process.cmd.CmdStatus.Error))) {
            return -EFAULT;
        }
    }

	res = MME_NotifyHost(event, command, res);

	if (put_user(res  , &(process->uPack.process.status))) {
		printk("stub_user_notify: put_user IOCTL Failed\n");
		return -EFAULT;
	}

	return 0;
}
