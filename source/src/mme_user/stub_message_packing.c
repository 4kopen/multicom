/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 
#include "ics.h"
#include "mme.h"
#include <linux/module.h>
#include <asm/uaccess.h>

#include "stub_message_packing.h"

#define MINVAL(x,y)   ( ((x) > (y)) ? (y) : (x))
#define VIRT2PHY      ICS_region_virt2phys

int stub_pack_abort(void *handle, MME_CommandId_t cmd_id, void *callbackData, void *arg)
{
	int                    res = 0;
	unsigned int           size, reqsz;
	MME_CommandPacked_t   *abort = (MME_CommandPacked_t *) arg;

	reqsz = sizeof(abort->size) + sizeof(abort->cmd_type) + sizeof(abort->callbackData) + sizeof(abort->uPack.abort);

	if (get_user(size, &(abort->size))) {
		return -EFAULT;
	}

	/* Check whether enough memory available to write an Abort Command */
	if (size < reqsz) {
		return -EFAULT;
	}

	if (put_user(MME_PACKED_ABORT, &(abort->cmd_type))     || /* Command Type */
	    put_user((MME_UINT) handle, &(abort->uPack.abort.handle))     || /* Transformer Handle */
	    put_user(cmd_id, &(abort->uPack.abort.commandId))  || /* Command ID */
	    put_user((MME_UINT) callbackData, &(abort->callbackData))) {                       /* Semaphore */
		res = -EFAULT;
	}

	return res;
}

int stub_update_abort(MME_ERROR *status, void *arg)
{
	MME_ERROR              error;
	MME_CommandPacked_t   *abort = (MME_CommandPacked_t *) arg;

	if (get_user(error, &(abort->uPack.abort.status))) {
		return -EFAULT;
	}

	*status = error;

	return 0;
}

int stub_pack_capability(MME_TransformerCapability_t *capa, void *callbackData, void *arg)
{
	int                    res  = 0;
	unsigned int           size, reqsz;
	MME_CommandPacked_t   *caps = (MME_CommandPacked_t *) arg;
	ICS_MEM_FLAGS          mflags;
	ICS_OFFSET             paddr = 0;

	reqsz = sizeof(caps->size) + sizeof(caps->cmd_type) + sizeof(caps->callbackData) + sizeof(caps->uPack.caps);

	if (get_user(size, &(caps->size))) {
		goto err_return;
	}

	/* Get the physical address of  TransformerInfoSize to re-map in stub user space app */
	if (capa->TransformerInfoSize) {
		if (ICS_SUCCESS != VIRT2PHY((ICS_VOID *) capa->TransformerInfo_p, &paddr, &mflags)) {
			printk("ERR>> Invalid TransformerInfo_p %p, skipping\n", capa->TransformerInfo_p);
		}
	}

	/* Check whether enough memory available to write a GetCapability Command */
	if (size < reqsz) {
		res = -EFAULT;
		goto err_return;
	}

	if (put_user(MME_PACKED_GETCAP, &(caps->cmd_type)) || /* Command Type */
	    put_user((MME_UINT) callbackData, &(caps->callbackData)) || /* Callback Data Message */
	    put_user(capa->StructSize, &(caps->uPack.caps.capability.StructSize)) || /* StructSize */
	    put_user((MME_GenericParams_t) paddr, &(caps->uPack.caps.capability.TransformerInfo_p)) || /* TransformerInfo_p */
	    put_user(capa->TransformerInfoSize, &(caps->uPack.caps.capability.TransformerInfoSize))) { /* TransformerInfoSize */
		res = -EFAULT;
		goto err_return;
	}

err_return:
	return res;
}

int stub_update_capability(MME_ERROR *status, MME_TransformerCapability_t *capa, void *arg)
{
	MME_ERROR              error;
	MME_CommandPacked_t   *caps = (MME_CommandPacked_t *) arg;

	if (get_user(error, &(caps->uPack.caps.status)) ||
	    get_user(capa->Version, &(caps->uPack.caps.capability.Version)) ||
	    copy_from_user(&(capa->InputType), &(caps->uPack.caps.capability.InputType), sizeof(MME_DataFormat_t)) ||
	    copy_from_user(&(capa->OutputType), &(caps->uPack.caps.capability.OutputType), sizeof(MME_DataFormat_t))) {
		return -EFAULT;
	}

	*status = error;

	return 0;
}

int stub_pack_initTransformer(MME_UINT initParamsLength, MME_GenericParams_t initParams, void *callbackData, void *arg)
{
	int                    res  = 0;
	unsigned int           size, reqsz;
	MME_CommandPacked_t   *init = (MME_CommandPacked_t *) arg;

	reqsz = sizeof(init->size) + sizeof(init->cmd_type) + sizeof(init->callbackData) + sizeof(init->uPack.init);

	if (get_user(size, &(init->size))) {
		res  = -EFAULT;
		goto err_return;
	}

	/* Check whether enough memory available to write an InitTransformer Command */
	if (size < reqsz) {
		res  = -EFAULT;
		goto err_return;
	}

	if (initParamsLength) {
		/* Get the physical address of  initParams to re-map in stub user space app */
		ICS_MEM_FLAGS      mflags;
		ICS_OFFSET         paddr = 0;
		if (ICS_SUCCESS != VIRT2PHY((ICS_VOID *) initParams, &paddr, &mflags)) {
			printk("ERR>> Invalid initParams %p, skipping\n", initParams);
		}
		initParams = (MME_GenericParams_t) paddr;
	}

	if (put_user(MME_PACKED_INIT, &(init->cmd_type)) || /* Command Type */
	    put_user((MME_UINT) callbackData, &(init->callbackData)) || /* Callback Data Message */
	    put_user(initParamsLength, &(init->uPack.init.initParamsLength)) ||	/* initParamsLength */
	    put_user(initParams, &(init->uPack.init.initParams))) { /* InitParams */
		res  = -EFAULT;
	}

err_return:
	return res;
}

int stub_update_initTransformer(MME_ERROR *status, void **handle, void *arg)
{
	MME_UINT               context;
	MME_ERROR              error;
	MME_CommandPacked_t   *init = (MME_CommandPacked_t *) arg;

	if (get_user(error, &(init->uPack.init.status)) ||
	    get_user(context, &(init->uPack.init.handle))) {
		return -EFAULT;
	}

	*status = error;
	*handle = (void *) context;

	return 0;
}

int stub_pack_termTransformer(void *handle, void *callbackData, void *arg)
{
	int                    res  = 0;
	unsigned int           size, reqsz;
	MME_CommandPacked_t   *term = (MME_CommandPacked_t *) arg;

	reqsz = sizeof(term->size) + sizeof(term->cmd_type) + sizeof(term->callbackData) + sizeof(term->uPack.term);

	if (get_user(size, &(term->size))) {
		res  = -EFAULT;
		goto err_return;
	}

	/* Check whether enough memory available to write a TermTransformer Command */
	if (size < reqsz) {
		res  = -EFAULT;
		goto err_return;
	}

	if (put_user(MME_PACKED_TERM, &(term->cmd_type)) || /* Command Type */
	    put_user((MME_UINT) callbackData, &(term->callbackData)) || /* Callback Data Message */
	    put_user((MME_UINT) handle, &(term->uPack.term.handle))) { /* handle */
		res  = -EFAULT;
	}

err_return:
	return res;
}

int stub_update_termTransformer(MME_ERROR *status, void *arg)
{
	MME_ERROR              error;
	MME_CommandPacked_t   *term = (MME_CommandPacked_t *) arg;

	if (get_user(error, &(term->uPack.term.status))) {
		return -EFAULT;
	}

	*status = error;

	return 0;
}

int stub_pack_processCommand(void *handle, MME_Command_t *cmd, void *callbackData, void *arg)
{
	int                    res  = 0;
	unsigned int           size, reqsz;
	unsigned int           nbuffer, nPages = 0;
	MME_CommandPacked_t   *process = (MME_CommandPacked_t *) arg;
	MME_DataBuffer_t     **databuffer_p, *databuffer;
	MME_ScatterPage_t     *scatterPages_p;
	unsigned int           iPage, iBuffer, PageIdx = 0;
	ICS_MEM_FLAGS          mflags;
	ICS_OFFSET             paddr = 0;

	/* Calculate the amount of memory required for Data buffers and Scatter Pages */
	nbuffer = (cmd->NumberInputBuffers + cmd->NumberOutputBuffers);
	for (iBuffer = 0; iBuffer < nbuffer; iBuffer++) {
		nPages += cmd->DataBuffers_p[iBuffer]->NumberOfScatterPages;
	}

	reqsz          = sizeof(process->size) + sizeof(process->cmd_type) + sizeof(process->callbackData) +
	                 sizeof(process->uPack.process);
	reqsz         += nbuffer * sizeof(MME_DataBuffer_t *); /* Size of Data Buffers Pointers */
	reqsz         += nbuffer * sizeof(MME_DataBuffer_t);   /* Size of Data Buffers */
	reqsz         += nPages * sizeof(MME_ScatterPage_t);   /* Size of all the Scatter Pages */
	/* Param_p and AdditionalInfo_p are mmaped in user space, so not explicitly copied*/

	if (get_user(size, &(process->size))) {
		res  = -EFAULT;
		goto err_return;
	}

	/* Check whether enough memory available to write an Process Command */
	if (size < reqsz) {
		res  = -EFAULT;
		goto err_return;
	}

	/* Derive Start Addresses of various variable Sized Pointers to be set in UserSpace Command */
	databuffer_p   = (MME_DataBuffer_t **)(process->uPack.process.var); /* Start Address of buffer pointer */
	databuffer     = (MME_DataBuffer_t *)(databuffer_p + nbuffer);    /* Start Address of Data Buffers */
	scatterPages_p = (MME_ScatterPage_t *)(databuffer + nbuffer);     /* Start Address of ScatterPages */

	/* Start Filling up the DataBuffers and Scatter Pages */
	for (iBuffer = 0; iBuffer < nbuffer; iBuffer++) {
		MME_DataBuffer_t *buf = cmd->DataBuffers_p[iBuffer];
		unsigned int Fail = 0;
		unsigned int PageStartIdx = PageIdx; // First Scatter Page starts here for this buffer
		/* Copy the Scatter Pages for this DataBuffer */
		for (iPage = 0; iPage < buf->NumberOfScatterPages; iPage++) {
			MME_ScatterPage_t *page = &(buf->ScatterPages_p[iPage]);

			Fail += copy_to_user(&scatterPages_p[PageIdx], page, sizeof(MME_ScatterPage_t));

			/* Translate all Kernel Logical addresses of Data buffers to Physical Addresses */
			paddr = 0;
			if (page->Size != 0) {
				/* MME itself does not translate Pages with zero size */
				if (ICS_SUCCESS != VIRT2PHY((ICS_VOID *) page->Page_p, &paddr, &mflags)) {
					printk("ERR>> stub_ProcessCommand: Buf %d Page %d %p %p (0x%02x) %x\n", iBuffer, iPage, page->Page_p, (void *)paddr,
					       buf->Flags & 0xFF,
					       page->FlagsIn);
				}
			}
			Fail += put_user((void *) paddr, &(scatterPages_p[PageIdx].Page_p));
			PageIdx++;
		}

		Fail += copy_to_user(&(databuffer[iBuffer]), buf, sizeof(MME_DataBuffer_t));

		if (Fail > 0 ||
		    put_user(&(databuffer[iBuffer]), &(databuffer_p[iBuffer])) ||
		    put_user(&scatterPages_p[PageStartIdx], &(databuffer[iBuffer].ScatterPages_p))) {
			res  = -EFAULT;
			goto err_return;
		}
	}

	if (put_user(MME_PACKED_PROCESS, &(process->cmd_type)) || /* Command Type */
	    put_user((MME_UINT) handle, &(process->uPack.process.handle)) || /* Transformer Handle */
	    copy_to_user(&(process->uPack.process.cmd), cmd, sizeof(MME_Command_t)) ||  /* Command */
	    put_user((MME_UINT) callbackData, &(process->callbackData)) ||  /* Callback Data Message */
	    put_user(cmd, &(process->uPack.process.kcmd))) { /* Orig. Cmd Pointer used in Notify Host */
		res  = -EFAULT;
		goto err_return;
	}

	/* Update the Pointer's in UserSpace Command to UserSpace memories */
	if (put_user(databuffer_p, &(process->uPack.process.cmd.DataBuffers_p))) {
		res  = -EFAULT;
		goto err_return;
	}

	/* Get the physical address of  Param_p to re-map in stub user space app */
	if (cmd->ParamSize) {
		paddr = 0;
		if (ICS_SUCCESS != VIRT2PHY((ICS_VOID *) cmd->Param_p, &paddr, &mflags)) {
			printk("ERR>> stub_ProcessCommand: Param_p Translation failed \n");
		}
		if (put_user((MME_GenericParams_t) paddr, &(process->uPack.process.cmd.Param_p))) {
			res  = -EFAULT;
			goto err_return;
		}
	}

	/* Get the physical address of  AdditionalInfo_p to re-map in stub user space app */
	if (cmd->CmdStatus.AdditionalInfoSize) {
		paddr = 0;
		if (ICS_SUCCESS != VIRT2PHY((ICS_VOID *) cmd->CmdStatus.AdditionalInfo_p, &paddr, &mflags)) {
			printk("ERR>> stub_ProcessCommand: AdditionalInfo_p Translation failed \n");
		}
		if (put_user((MME_GenericParams_t) paddr, &(process->uPack.process.cmd.CmdStatus.AdditionalInfo_p))) {
			res  = -EFAULT;
			goto err_return;
		}
	}

err_return:
	return res;
}

int stub_update_processCmdBuffers(MME_Command_t *cmd, void *arg)
{
	MME_CommandPacked_t   *process = (MME_CommandPacked_t *) arg;
	unsigned int           iPage, iBuffer, nbuffer;
	unsigned int           PageIdx = 0;
	MME_DataBuffer_t     **databuffer_p, *databuffer;
	MME_ScatterPage_t     *scatterPages_p;

	/* Calculate the amount of memory required for Data buffers and Scatter Pages */
	nbuffer = (cmd->NumberInputBuffers + cmd->NumberOutputBuffers);

	/* Derive Start Addresses of various variable Sized Pointers to be set in UserSpace Command */
	databuffer_p   = (MME_DataBuffer_t **)(process->uPack.process.var); /* Start Address of buffer pointer */
	databuffer     = (MME_DataBuffer_t *)(databuffer_p + nbuffer);    /* Start Address of Data Buffers */
	scatterPages_p = (MME_ScatterPage_t *)(databuffer + nbuffer);     /* Start Address of ScatterPages */

	if (get_user(cmd->CmdStatus.State, &(process->uPack.process.cmd.CmdStatus.State)) ||
	    get_user(cmd->CmdStatus.Error, &(process->uPack.process.cmd.CmdStatus.Error)) ||
	    get_user(cmd->CmdStatus.ProcessedTime, &(process->uPack.process.cmd.CmdStatus.ProcessedTime))) {
		return -EFAULT;
	}

	/* Update output Databuffer's Scatter Pages */
	for (iBuffer = 0; iBuffer < nbuffer; iBuffer++) {
		MME_DataBuffer_t *buf = cmd->DataBuffers_p[iBuffer];
		/* Update the Scatter Pages for this DataBuffer */
		for (iPage = 0; iPage < buf->NumberOfScatterPages; iPage++) {
			if ( iBuffer >= cmd->NumberInputBuffers ) // only Output Buffers need to be updated
			{
				MME_ScatterPage_t *page = &(buf->ScatterPages_p[iPage]);
				if (get_user(page->BytesUsed, &(scatterPages_p[PageIdx].BytesUsed)) ||
					get_user(page->FlagsOut, &(scatterPages_p[PageIdx].FlagsOut))) {
					return -EFAULT;
				}
			}
			PageIdx++;
		}
	}
	return 0;
}

int stub_update_processCommand(MME_ERROR *status, MME_Command_t *cmd, void *arg)
{
	MME_ERROR              error = MME_SUCCESS;
	MME_CommandPacked_t   *process = (MME_CommandPacked_t *) arg;
	int                    res = 0;

	/* read the error code to see if the command has been deffered */
	if (get_user(error, &(process->uPack.process.status))) {
		res = -EFAULT;
	} else if (error != MME_TRANSFORM_DEFERRED) {
		/* Data buffers for deferred command to be updated in Notify Host */
		res = stub_update_processCmdBuffers(cmd, arg);
	}

	*status = error;

	return res;
}

int stub_get_callbackData(MME_UINT *callbackData, void *arg)
{
	MME_UINT               data;
	MME_CommandPacked_t   *cmd = (MME_CommandPacked_t *) arg;

	if (get_user(data, &(cmd->callbackData))) {
		return -EFAULT;
	}

	*callbackData = data;

	return 0;
}
