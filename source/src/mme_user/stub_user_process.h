/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 
 


#ifndef STUB_USER_PROCESS_H_
#define STUB_USER_PROCESS_H_

#include <mme.h>
#include "stub_user_ioctl.h"
#include "stub_message_queue.h"

// Manager Thread -> Init/Term/GetCapability
// Message Receiver -> Abort/SendBuffers
// Execution Loop -> Transform/Set Global
typedef enum {
	SEM_MANAGER_THREAD,
	SEM_MESSAGE_RECEIVER_THREAD,
	SEM_EXECUTION_LOOP,

	/* Do Not Edit Below this Line */
	MAX_SIMULTANEOUS_COMMAND_THREAD
} stub_sem_id_t;

typedef enum {
	STUB_GETCAPABILITY,
	STUB_INIT_TRANSFORMER,
	STUB_PROCESS_SENDBUFFER,
	STUB_PROCESS_TRANSFORMPARAMS,
	STUB_ABORT_CMD,
	STUB_TERM_TRANSFORMER,

	/* Do not edit below this line */
	STUB_MAX_COMMANDS
} stub_command_t;

typedef struct {
	stub_command_t                     type;   // Command Type
	MME_ERROR                          status; // Return Status of the API
	struct semaphore                  *sem;    // Semaphore to be triggered on completion
	void                              *arg1;   // First argument of the API
	void                              *arg2;   // Second argument of the API
	void                              *arg3;   // Third argument of the API
} stub_CommandMessage_t;

#define MAX_SIZE_PER_MESSAGE                 (sizeof(stub_CommandMessage_t))
#define MAX_MESSAGE_FIFO_LENGTH               MAX_SIMULTANEOUS_COMMAND_THREAD //Ideally only this much is required.

typedef struct {
	unsigned int       isInit;// States whether the transformer is initialized
	unsigned int       minor; // Minor Number of Device
	message_queue_t   *queue;
	char               name[MME_MAX_TRANSFORMER_NAME + 1]; // MME Stub Transformer Registered Name
	struct mutex      *mutex;
	struct semaphore  *sem[MAX_SIMULTANEOUS_COMMAND_THREAD];
	unsigned long      base[BPA_COUNT];
	unsigned long      size[BPA_COUNT];
} stub_instance_t;

stub_instance_t *stub_user_allocate_instance(int minor);
int              stub_user_free_instance(stub_instance_t *instance);
int              stub_user_receive(stub_instance_t *instance, void *arg);
int              stub_user_completed(stub_instance_t *instance, void *arg);
int              stub_user_register(stub_instance_t *instance, void *arg);
int              stub_user_notify(stub_instance_t *instance, void *arg);

message_queue_t  *GetQueueByMinor(int minor);
struct semaphore *GetSemaphoreByMinor(int minor, stub_sem_id_t  id);
int IsKilled(int minor);

#endif /* STUB_USER_PROCESS_H_ */
