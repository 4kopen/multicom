/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <linux/module.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/spinlock.h>
#include <linux/version.h>
#include <linux/bpa2.h>
#include "stub_user_process.h"
#include "stub_user_ioctl.h"
#include <ics.h>
#include "../ics/include/_ics_region.h"

MODULE_DESCRIPTION("MME Userspace Driver Stub");
MODULE_AUTHOR("STMicroelectronics");
MODULE_LICENSE("GPL");

static int major = 0 /*STUB_MAJOR_NUM */;	/* Can be set to 0 for dynamic */
module_param(major, int, 0);
MODULE_PARM_DESC(major, "MME Major device number");
/*
 * Support for declaring bpa2_parts on module load
 * The syntax is
 *
 *     bpa2_parts = bpa_part[,bpa_part]
 *     bpa_part = <bpa2name>
 *
 * 
 * Where bpa2name is the ASCII name of the bpa2 partition e.g. LMI_VID or LMI_SYS
 *
 * Once configured these bpa2_parts will be given to each instance in the userspace
 * A maximum of 4 bpa2_parts is supported.
 * if some bpa2_parts are  not exiting at module insertion, module probe will
 * failed
 */

static char *     bpa2_parts = NULL;
module_param     (bpa2_parts, charp, S_IRUGO);
MODULE_PARM_DESC (bpa2_parts, "Memory region string of the form <bpa2name>");

/* Forward declarations */
static int stub_user_open(struct inode *inode, struct file *filp);
static int stub_user_release(struct inode *inode, struct file *filp);
static long stub_user_ioctl(struct file *filp, unsigned int command, unsigned long arg);
static int stub_user_mmap(struct file *filp, struct vm_area_struct *vma);

static struct file_operations stub_user_ops = {
	.owner          = THIS_MODULE,
	.open           = stub_user_open,
	.release        = stub_user_release,
	.unlocked_ioctl = stub_user_ioctl,
	.mmap           = stub_user_mmap
};

#define STUB_DEV_COUNT      MAX_TRANSFORMER_REGISTRATION

static struct cdev          stub_cdev;
static dev_t                stub_devid;
static struct class        *stub_class;
static struct device       *stub_class_dev[STUB_DEV_COUNT];

static spinlock_t   lock;
stub_instance_t init_instance;
static int  nInstance[STUB_DEV_COUNT]; //Instance Per Minor Number
static int  bKill[STUB_DEV_COUNT]; //Indicates instance is being killed
static stub_instance_t *handle[STUB_DEV_COUNT];

message_queue_t *GetQueueByMinor(int minor)
{
	message_queue_t *queue = NULL;
	if ((minor < STUB_DEV_COUNT) && (handle[minor] != NULL)) {
		queue = handle[minor]->queue;
	}
	return queue;
}

struct semaphore *GetSemaphoreByMinor(int minor, stub_sem_id_t  id)
{
	struct semaphore *sem = NULL;
	if ((minor < STUB_DEV_COUNT) && (handle[minor] != NULL) && id < MAX_SIMULTANEOUS_COMMAND_THREAD) {
		sem = handle[minor]->sem[id];
	}
	return sem;
}

int IsKilled(int minor)
{
	if (minor < STUB_DEV_COUNT) {
		return bKill[minor];
	} else {
		return 0;
	}
}
/* Parse a comma separated list of region declarations
   
 * Each element can be of the form <bpa2name> *
 */
static int parseBpaString (char *str)
{
	char *name;
	unsigned long  base;
	unsigned long size;
	int numBpa =0;
	if (!str || !*str)
		return -EINVAL;
	while ((name = strsep(&str, ",")) != NULL) {
		/* Assume its a bpa2name */
		struct bpa2_part *part;
		/* Lookup the BPA2 partition */
		part = bpa2_find_part(name);
		if (part == NULL) {
			printk("Failed to find BPA2 region '%s'\n", name);
			return -EINVAL;
		}
		/* Query the base and size of partition */
		bpa2_memory(part, &base, &size);
		init_instance.base[numBpa] = base;
		init_instance.size[numBpa] = size;

		numBpa++;
		if (numBpa >= BPA_COUNT) {
			printk("more than %d region !!\n", BPA_COUNT);
				return -EFAULT;
		}
	}
	return 0;
}

/* ==========================================================================
 *
 * Called by the kernel when the module is loaded
 *
 * ==========================================================================
 */

int __init stub_user_init(void)
{
	int result;
	int i;

	if (major) {
		/* Static major number allocation */
		stub_devid = MKDEV(major, 0);
		result =  register_chrdev_region(stub_devid, STUB_DEV_COUNT, STUB_DEV_NAME);
	} else {
		/* Dynamic major number allocation */
		result = alloc_chrdev_region(&stub_devid, 0, STUB_DEV_COUNT, STUB_DEV_NAME);
	}

	if (result) {
		printk(KERN_ERR "stub: register_chrdev failed : %d\n", result);
		goto err_register;
	}

	cdev_init(&stub_cdev, &stub_user_ops);
	stub_cdev.owner = THIS_MODULE;
	result = cdev_add(&stub_cdev, stub_devid, STUB_DEV_COUNT);

	if (result) {
		printk(KERN_ERR "stub: cdev_add failed : %d\n", result);
		goto err_cdev_add;
	}

	/* It appears we have to create a class in order for udev to work */
	stub_class = class_create(THIS_MODULE, STUB_DEV_NAME);
	if ((result = IS_ERR(stub_class))) {
		printk(KERN_ERR "stub: class_create failed : %d\n", result);
		goto err_class_create;
	}

	/* class_device_create() causes the /dev/stub file to appear when using udev
	 * however it is a GPL only function.
	 */
	for (i = 0; i < STUB_DEV_COUNT; i++) {
		stub_class_dev[i] = device_create(stub_class, NULL, MKDEV(MAJOR(stub_devid), MINOR(stub_devid) + i), NULL,
		                                  STUB_DEV_NAME"%d", i);
		if ((result = IS_ERR(stub_class_dev[i]))) {
			printk(KERN_ERR "stub: class_device_create failed : %d\n", result);
			goto err_class_device_create;
		}
	}
	parseBpaString(bpa2_parts);
	memset(nInstance, 0, sizeof(nInstance));
	memset(bKill, 0, sizeof(bKill));
	memset(handle, 0, sizeof(handle));

	spin_lock_init(&lock);

	if (result == 0) {
		printk("MME_STUB: module loaded\n");
	}

	return result;

err_class_device_create:
	class_destroy(stub_class);

err_class_create:
	cdev_del(&stub_cdev);

err_cdev_add:
	unregister_chrdev_region(stub_devid, STUB_DEV_COUNT);

err_register:

	return result;
}

/* ==========================================================================
 *
 * Called by the kernel when the module is unloaded
 *
 * ==========================================================================
 */
void __exit stub_user_exit(void)
{
	int i;
	for (i = 0; i < STUB_DEV_COUNT; i++) {
		device_unregister(stub_class_dev[i]);
	}
	class_destroy(stub_class);
	cdev_del(&stub_cdev);
	unregister_chrdev_region(stub_devid, STUB_DEV_COUNT);
	printk("MME_STUB: module unloaded\n");
}

module_init(stub_user_init);
module_exit(stub_user_exit);

/* ==========================================================================
 *
 * Called by the kernel when the device is opened by an app (the stub user lib)
 *
 * ==========================================================================
 */
static int
stub_user_open(struct inode *inode, struct file *filp)
{
	int                 err = 0;
	stub_instance_t    *instance;
	int                 minor = iminor(inode);

	spin_lock(&lock); // Deny any other process to open the same device twice
	if (nInstance[minor] > 0) {
		err = -EBUSY;
	} else {
		nInstance[minor] ++;
	}
	spin_unlock(&lock);

	if (err) {
		return err;
	}

	// Allocate a Stub Instance and initialize it
	instance = stub_user_allocate_instance(minor);

	if (instance == NULL) {
		spin_lock(&lock); // No Memory Allocated, free up this instance
		nInstance[minor] --;
		spin_unlock(&lock);
		err = -ENOMEM;
	} else {
		handle[minor]      = instance;
		filp->private_data = (void *) instance;
		bKill[minor]       = 0;
	}

	return err;
}

/* ==========================================================================
 *
 * Called by the kernel when the device is closed by an app (the stub user lib)
 *
 * ==========================================================================
 */
static int
stub_user_release(struct inode *inode, struct file *filp)
{
	int                 err;
	int                 minor    = iminor(inode);
	stub_instance_t    *instance = (stub_instance_t *) filp->private_data;

	/* Indicate this device being killed */
	bKill[minor] = 1;

	err = stub_user_free_instance(instance);

	filp->private_data = (void *) NULL;

	spin_lock(&lock);
	nInstance[minor] --; // Decrement instance count
	spin_unlock(&lock);

	return err;
}

/* ==========================================================================
 *
 * Called by the kernel when an ioctl system call is made
 *
 * ==========================================================================
 */
static long
stub_user_ioctl(struct file *filp, unsigned int command, unsigned long arg)
{
	stub_instance_t  *instance = (stub_instance_t *) filp->private_data;
	int res = -ENOTTY;

	switch (command) {
	case MME_STUB_IOC_RECEIVE:
		res = stub_user_receive(instance, (void *) arg);
		break;

	case MME_STUB_IOC_COMPLETED:
		res = stub_user_completed(instance, (void *) arg);
		break;

	case MME_STUB_IOC_REGISTER:
		res = stub_user_register(instance, (void *) arg);
		break;

	case MME_STUB_IOC_NOTIFYHOST:
		res = stub_user_notify(instance, (void *) arg);
		break;

	default:
		break;
	}

	return res;
}

/* ==========================================================================
 *
 * stub_user_mmap()
 * Called via mmap sys call from the user space
 *
 * ==========================================================================
 */
static int
stub_user_mmap(struct file *filp, struct vm_area_struct *vma)
{
	int res = -EINVAL, i;
	ICS_MEM_FLAGS mflags;
    stub_instance_t  *instance = (stub_instance_t *) filp->private_data;

	unsigned long size        = vma->vm_end - vma->vm_start;
	unsigned long offsetstart = vma->vm_pgoff * PAGE_SIZE;
	unsigned long offsetend   = offsetstart + size;

    /*  for now we assume that all area exposed to cpu 0 can be used  */
	if (ics_region_mappeable(MME_USERSPACE_CPU,offsetstart, size,&mflags)!=ICS_SUCCESS) {
		for (i=0; ((i< BPA_COUNT) && (res!=0)) ; i++)
			/*  loop on all the descriptors till one area match */
			if (instance->size[i]!=0) {
				if ((offsetstart >= instance->base[i])
					&& (offsetend <= (instance->base[i] + instance->size[i])))

					res = 0;
			}
		if (res == -EINVAL)
			goto exit;
	} else 
		res=0;

	if (mflags & ICS_UNCACHED) {
		/* Mark the physical area as uncached */
		vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	}
	/* Update the vm flags to always keep mapping and not dump in core */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(3, 10, 0))
	vma->vm_flags |= VM_RESERVED | VM_IO;	// kernel 3.4 and previous
#else
	vma->vm_flags |= VM_DONTDUMP | VM_IO;	// kernel 3.10 and above
#endif
	if (remap_pfn_range(vma, vma->vm_start, offsetstart >> PAGE_SHIFT, size, vma->vm_page_prot) < 0) {
		res = -EAGAIN;
	}

exit:
	return res;
}
