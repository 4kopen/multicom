/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 
 

#ifndef _MME_STUB_IOCTL_H_
#define _MME_STUB_IOCTL_H_

#include <mme.h>
/*  maximum memory area shared between kernel and userspace defined at mme_stub insertion */
#define BPA_COUNT 4
#define STUB_DEV_NAME                       "mmestub"
#define MAX_TRANSFORMER_REGISTRATION        32


typedef enum {
	MME_PACKED_GETCAP,
	MME_PACKED_INIT,
	MME_PACKED_TERM,
	MME_PACKED_ABORT,
	MME_PACKED_PROCESS
} MME_PackedCommand_Type;

typedef struct {
	/* IN */
	MME_UINT                      handle;
	MME_CommandId_t               commandId;
	/* OUT */
	MME_ERROR                     status;
} MME_Packed_AbortCommand_t;

typedef struct {
	/* IN/OUT */
	MME_ERROR                     status;
	MME_TransformerCapability_t   capability;
	unsigned char                 var[1];     // Variable memory used for TransformerInfo_p
} MME_Packed_GetCapability_t;

typedef struct {
	/* OUT */
	MME_ERROR                     status;
	MME_UINT                      handle;
	/* IN */
	MME_UINT                      initParamsLength;
	MME_GenericParams_t           initParams;
} MME_Packed_InitTransformer_t;

typedef struct {
	/* IN */
	MME_UINT                      handle;
	/* OUT */
	MME_ERROR                     status;
} MME_Packed_TermTransformer_t;

typedef struct {
	/* IN */
	MME_Command_t                *kcmd; //Kernel CMD saved for Notify Host calls in Stream Based
	MME_UINT                      handle;
	MME_Command_t                 cmd; //In/Out
	/* OUT */
	MME_ERROR                     status;
	unsigned char                 var[1];     // Variable Sized Memory used on per command basis
} MME_Packed_ProcessCommand_t;

typedef struct {
	unsigned int                       size;         // Provided from User (Max Possible Size Available)
	MME_UINT                           userData;     // Filled by user space lib
	MME_PackedCommand_Type             cmd_type;     // Filled by Kernel, stating Command Type
	MME_UINT                           callbackData; // Filled By Kernel, stating callback data
	MME_Event_t                        event;
	union {
		MME_Packed_GetCapability_t     caps;
		MME_Packed_InitTransformer_t   init;
		MME_Packed_ProcessCommand_t    process;
		MME_Packed_AbortCommand_t      abort;
		MME_Packed_TermTransformer_t   term;
	} uPack;                                       // Command Specific Input/Output Arguments
} MME_CommandPacked_t;

typedef struct {
	char TransformerName[MME_MAX_TRANSFORMER_NAME + 1];
	unsigned long base[BPA_COUNT];  //BPA2 Partition in LMI0
	unsigned long size[BPA_COUNT];  //Size of BPA2 Partition in LMI0
} MME_StubRegisterTransform_t;


#define MME_STUB_IOC_MAGIC 			'h'

#define MME_STUB_IOC_RECEIVE		             _IOWR(MME_STUB_IOC_MAGIC, 0x01, MME_CommandPacked_t)
#define MME_STUB_IOC_COMPLETED		             _IOWR(MME_STUB_IOC_MAGIC, 0x02, MME_CommandPacked_t)
#define MME_STUB_IOC_NOTIFYHOST		             _IOWR(MME_STUB_IOC_MAGIC, 0x03, MME_CommandPacked_t)
#define MME_STUB_IOC_REGISTER		             _IOWR(MME_STUB_IOC_MAGIC, 0x04, MME_StubRegisterTransform_t)

#endif /* _MME_STUB_IOCTL_H_ */
