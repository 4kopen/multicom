/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef STUB_POSIX_WRAPPER_H_
#define STUB_POSIX_WRAPPER_H_

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#ifndef _LINUX_TYPES_H
typedef unsigned int size_t;
#endif

#define print        printf

typedef unsigned int                   interrupt_name_t;
typedef unsigned long                  osclock_t;        // Time in microseconds
typedef unsigned long                  task_flags_t;

#ifndef __OS21_POSIX_WRAPPER__
// User should not be exposed to Linux Kernel definitions to maintain encapsulation
typedef void                           semaphore_t;
typedef void                           mutex_t;
typedef void                           task_t;
typedef void                           waitq_t;
typedef void                           event_group_t;
typedef void                           message_queue_t;
#else
#define _GNU_SOURCE

#define _XOPEN_SOURCE     600   // sem_timedwait
#define __USE_UNIX98

#include <pthread.h>
#include <semaphore.h>

typedef sem_t                          semaphore_t;
typedef pthread_mutex_t                mutex_t;
typedef pthread_t                      task_t;

// Event Handling Abstraction Layer
typedef struct {
	int              mask;
	pthread_cond_t   cond;
	pthread_mutex_t *mutex;
} event_group_t;

#define MAX_MSG_QUEUE_LENGTH		64

// Quick implementation of message queue.
typedef struct {
	unsigned int    ElementUseMap[MAX_MSG_QUEUE_LENGTH / (8 * sizeof(int))];
	mutex_t        *Mutex;
} msg_pool_t;

typedef struct msg_node_s {
	struct msg_node_s *next;
} msg_header_t;

typedef struct msq_queue_s {
	msg_header_t      *head;
	msg_header_t      *tail;
	semaphore_t       *sem;
	mutex_t           *mutex;
} msg_fifo_t;

// Quick implementation of message queue.
typedef struct message_queue_s {
	int              ElementSize;
	int              ElementNb;
	char            *ElementBuffer;
	msg_pool_t      *pool;
	msg_fifo_t      *fifo;
} message_queue_t;

#endif

#define task_flags_suspended               (1)
#define MAX_USER_PRIORITY                  (99)
#define MIN_USER_PRIORITY                  (0)
#define PRIORITY_CONV(x)                   ((x) ? (-(((x) * 100)>>8)) : -1)
#define OS21_SUCCESS    	               (0)
#define OS21_FAILURE    	               (-1)
#define TIMEOUT_IMMEDIATE                  ((osclock_t *)0)
#define TIMEOUT_INFINITY                   ((osclock_t *)0xFFFFFFFF)

#define STOS_MALLOC                         malloc
#define STOS_FREE                           free
#define STOS_PRINTF                         print
#define STOS_SPRINTF                        sprintf

#define STOS_EventGroupCreate               event_group_create
#define STOS_EventGroupDelete               event_group_delete
#define STOS_EventWaitAny                   event_wait_any
#define STOS_EventPost                      event_post
#define STOS_EventClear                     event_clear
#define STOS_semaphore_create_fifo          semaphore_create_fifo
#define STOS_semaphore_wait                 semaphore_wait
#define STOS_semaphore_wait_timeout         semaphore_wait_timeout
#define STOS_semaphore_signal               semaphore_signal
#define STOS_semaphore_delete               semaphore_delete
#define STOS_MutexCreate                    acc_mutex_create
#define STOS_MutexLock                      acc_mutex_lock
#define STOS_MutexRelease                   acc_mutex_release
#define STOS_MutexDelete                    acc_mutex_delete

#define STOS_TaskId                         task_id
#define STOS_task_create                    task_create
#define STOS_TaskKill                       task_kill
#define STOS_task_wait                      task_wait
#define STOS_task_delete                    task_delete
#define STOS_TaskSuspend                    task_suspend
#define STOS_task_priority                  task_priority
#define STOS_MessageQueueCreate             message_create_queue

/* Following Functions Not Implemented Yet */
#define STOS_FOPEN                          (void)
#define STOS_FCLOSE                         (void)
#define STOS_FWRITE                         (void)
#define STOS_ASSERT                         (void)
#define STOS_CachePurgeDataAll()

void             kernel_initialize(void *x);
void             kernel_start(void);
void             kernel_timeslice(int on);

osclock_t        time_now(void);
osclock_t        time_plus(const osclock_t time1, const osclock_t time2);
osclock_t        time_minus(const osclock_t time1, const osclock_t time2);

mutex_t         *acc_mutex_create(void);
void             acc_mutex_lock(mutex_t *mutex);
int              acc_mutex_release(mutex_t *mutex);
int              acc_mutex_delete(mutex_t *mutex);

semaphore_t     *semaphore_create_fifo(int val);
int              semaphore_wait(semaphore_t *semaphore);
int              semaphore_wait_timeout(semaphore_t *semaphore, const osclock_t *timeout);
void             semaphore_signal(semaphore_t *semaphore);
int              semaphore_delete(semaphore_t *semaphore);

message_queue_t *message_create_queue(int element_size, int list_length);
void             message_delete_queue(message_queue_t *msg_queue);
void             message_release(message_queue_t *queue, void *msg);
void            *message_claim(message_queue_t *msg_queue);
void            *message_claim_timeout(message_queue_t *msg_queue, const osclock_t *time);
void            *message_receive(message_queue_t *queue);
void             message_send(message_queue_t *queue, void *message);

event_group_t   *event_group_create(int option);
int              event_group_delete(event_group_t *event_group);
int              event_wait_any(event_group_t *event_group, const unsigned int in_mask, unsigned int *out_mask,
                                const osclock_t *timeout);
void             event_post(event_group_t *os21_event, int value);
void             event_clear(event_group_t *os21_event, int value);

void             task_delay(osclock_t delay);
void             task_delay_until(osclock_t this_time);
int              task_suspend(task_t *task);
int              task_resume(task_t *task);
task_t          *task_id(void);
task_t          *task_create(void (*function)(void *), void *param, size_t stack_size, int priority, const char *name,
                             task_flags_t flags);
int              task_delete(task_t *task);
int              task_wait(task_t **tasklist, int ntasks, osclock_t *timeout);
int              task_kill(task_t *task, int status, int flags);
int              task_priority(task_t *task);
int              task_priority_set(task_t *task, int priority);
int              task_exit(void *Param);

#endif /* STUB_POSIX_WRAPPER_H_ */
