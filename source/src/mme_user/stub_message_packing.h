/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 


#ifndef MESSAGE_PACKING_H_
#define MESSAGE_PACKING_H_

#include "stub_user_ioctl.h"

int stub_pack_abort(void *handle, MME_CommandId_t cmd_id, void *callbackData, void *arg);
int stub_pack_capability(MME_TransformerCapability_t *capa, void *callbackData, void *arg);
int stub_pack_initTransformer(MME_UINT initParamsLength, MME_GenericParams_t initParams, void *callbackData, void *arg);
int stub_pack_termTransformer(void *handle, void *callbackData, void *arg);
int stub_pack_processCommand(void *handle, MME_Command_t *cmd, void *callbackData, void *arg);

int stub_update_abort(MME_ERROR *status, void *arg);
int stub_update_capability(MME_ERROR *status, MME_TransformerCapability_t *capa, void *arg);
int stub_update_initTransformer(MME_ERROR *status, void **handle, void *arg);
int stub_update_termTransformer(MME_ERROR *status, void *arg);
int stub_update_processCommand(MME_ERROR *status, MME_Command_t *cmd, void *arg);
int stub_update_processCmdBuffers(MME_Command_t *cmd, void *arg);
int stub_get_callbackData(MME_UINT *callbackData, void *arg);

#endif /* MESSAGE_PACKING_H_ */
