/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <linux/suspend.h>
#include <mme.h>	/* External defines and prototypes */

#include "_mme_sys.h"	/* Internal defines and prototypes */

#define MME_RT_PRIO(X)	(-(X+20))

static uint  debug_flags;
static int   init=1;
static ulong pool_size = 0;
static int tuneables[2] = {0xFFFFFFFF,0x0};  /* tuneables[0]-key [1]-value */
static int thread_mme_manager[2] = { 2, 64 };
static int thread_mme_rcv[2] = { 2, 63 };
static int thread_mme_exec00[2] = { 2, 62 };
static int thread_mme_exec01[2] = { 2, 61 };
static int thread_mme_exec02[2] = { 2, 60 };
static int thread_mme_exec03[2] = { 2, 59 };
static int thread_mme_exec04[2] = { 2, 58 };

static int *param_thread[7] = {  thread_mme_manager, thread_mme_rcv, \
							thread_mme_exec04, thread_mme_exec03, \
							thread_mme_exec02, thread_mme_exec01,\
							thread_mme_exec00};
static uint affinity_mask;

#if defined(__KERNEL__) && defined(MODULE)

MODULE_DESCRIPTION("MME: Multi Media Encoder");
MODULE_AUTHOR("STMicroelectronics Ltd");
MODULE_LICENSE("GPL");
MODULE_VERSION(__MULTICOM_VERSION__);

module_param(debug_flags, int, 0);
MODULE_PARM_DESC(debug_flags, "Bitmask of MME debugging flags");

module_param(init, int, S_IRUGO);
MODULE_PARM_DESC(init, "Do not initialize MME if set to 0");

module_param(pool_size, ulong, 0);
MODULE_PARM_DESC(pool_size, "Size of the MME_DataBuffer buffer pool");

module_param_array(tuneables,int,NULL, 0);
MODULE_PARM_DESC(tuneables, "tuneable parameters for MME_ModifyTuneable");

module_param_array_named(thread_MME_Manager, thread_mme_manager, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Manager, "MME-Manager thread:s(Mode),p(Priority)");

module_param_array_named(thread_MME_Rcv, thread_mme_rcv, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Rcv, "MME-Rcv and MME-Tfm thread:s(Mode),p(Priority)");

module_param_array_named(thread_MME_Exec00, thread_mme_exec00, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Exec00, "MME-Exec00 thread:s(Mode),p(Priority)");

module_param_array_named(thread_MME_Exec01, thread_mme_exec01, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Exec01, "MME-Exec01 thread:s(Mode),p(Priority)");

module_param_array_named(thread_MME_Exec02, thread_mme_exec02, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Exec02, "MME-Exec02 thread:s(Mode),p(Priority)");

module_param_array_named(thread_MME_Exec03, thread_mme_exec03, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Exec03, "MME-Exec03 thread:s(Mode),p(Priority)");

module_param_array_named(thread_MME_Exec04, thread_mme_exec04, int, NULL, 0644);
MODULE_PARM_DESC(thread_MME_Exec04, "MME-Exec04 thread:s(Mode),p(Priority)");

module_param(affinity_mask, int, 0);
MODULE_PARM_DESC(affinity_thread_MME_Tfm, "MME-Tfm thread:bitmask of MME CPU Affinity");

/*
 * Export all the MME module symbols
 */

EXPORT_SYMBOL( MME_SendCommand );
EXPORT_SYMBOL( MME_AbortCommand );
EXPORT_SYMBOL( MME_NotifyHost );

EXPORT_SYMBOL( MME_AllocDataBuffer );
EXPORT_SYMBOL( MME_FreeDataBuffer );

EXPORT_SYMBOL( MME_RegisterTransport );
EXPORT_SYMBOL( MME_DeregisterTransport );

EXPORT_SYMBOL( MME_Init );
EXPORT_SYMBOL( MME_Term );
EXPORT_SYMBOL( MME_Run );		/* DEPRECATED */

EXPORT_SYMBOL( MME_RegisterTransformer );
EXPORT_SYMBOL( MME_DeregisterTransformer );
EXPORT_SYMBOL( MME_IsTransformerRegistered );
EXPORT_SYMBOL( MME_TermTransformer );
EXPORT_SYMBOL( MME_InitTransformer );

EXPORT_SYMBOL( MME_KillTransformer );
EXPORT_SYMBOL( MME_KillCommand );
EXPORT_SYMBOL( MME_KillCommandAll );

EXPORT_SYMBOL( MME_ModifyTuneable );
EXPORT_SYMBOL( MME_GetTuneable );

EXPORT_SYMBOL( MME_IsStillAlive );
EXPORT_SYMBOL( MME_GetTransformerCapability );

/*
 * MME4 Extensions
 */
EXPORT_SYMBOL( MME_Version );
EXPORT_SYMBOL( MME_WaitCommand );
EXPORT_SYMBOL( MME_RegisterMemory );
EXPORT_SYMBOL( MME_DeregisterMemory );
EXPORT_SYMBOL( MME_DebugFlags );
EXPORT_SYMBOL( MME_ErrorStr );

EXPORT_SYMBOL( MME_Transformer_cpu);
		/*
 * Internal APIs exported for mme_user module
 */
EXPORT_SYMBOL( mme_debug_flags );

static int mme_probe (struct platform_device *pdev)
{
	MME_ERROR res;
	int nb;
	
  	printk("MME Version %s \n", MME_Version());
  	printk("MME module init=%d debug_flags=0x%x pool_size=%ld\n", init, debug_flags, pool_size);
  
	MME_DebugFlags(debug_flags);

	if (pool_size)
    {
		MME_ModifyTuneable(MME_TUNEABLE_BUFFER_POOL_SIZE, pool_size);
    }
  
    if(tuneables[0] != 0xFFFFFFFF)
    {
		MME_ModifyTuneable(tuneables[0],tuneables[1]);
    }

    for (nb = 0; nb < 7; nb++) {
	if (param_thread[nb][0] != 0xFFFFFFFF) {
		if (param_thread[nb][0] & 0x2)
			MME_ModifyTuneable(nb, MME_RT_PRIO(param_thread[nb][1]));
		else
			MME_ModifyTuneable(nb, param_thread[nb][1]);
	}
  }

	if (!init)
	  return 0;

        ICS_SetAffinity (affinity_mask);

	res = MME_Init();
	
	if (res != MME_SUCCESS)
	  return -ENODEV;

	return 0;
}

static int mme_remove (struct platform_device *pdev) 
{
	MME_ERROR status;
	
	status = MME_Term();

	/* XXXX Do we need to force shutdown in this case ? */
	if (status != MME_SUCCESS)
		printk("MME_Term() failed : %s (%d)\n",
		       MME_ErrorStr(status), status);
	
	return (status == MME_SUCCESS) ? 0 : -EBUSY;
}

#ifdef CONFIG_PM
static int mme_freeze(struct device *dev)
{
	MME_ERROR status;
	status = MME_Term();

	/* XXXX Do we need to force shutdown in this case ? */
	if (status != MME_SUCCESS)
		printk("MME_Term() failed : %s (%d)\n",
				MME_ErrorStr(status), status);

	return ((status == MME_SUCCESS)|| (status==MME_DRIVER_NOT_INITIALIZED)) ? 0 : -EBUSY;
}

static int mme_restore(void)
{
	MME_ERROR res;
	printk("MME module init=%d debug_flags=0x%x pool_size=%ld\n",
		init,
		debug_flags,
		pool_size);

	MME_DebugFlags(debug_flags);

	if (pool_size)
		MME_ModifyTuneable(MME_TUNEABLE_BUFFER_POOL_SIZE, pool_size);

	if (!init)
		return 0;

	res = MME_Init();

	if (res != MME_SUCCESS)
		return -ENODEV;

	return 0;
}

static int pm_mme_notifier_cb(struct notifier_block *nb,
		unsigned long action, void *ptr)
{
	switch (action) {
	case PM_POST_SUSPEND:
	case PM_POST_HIBERNATION:
	case PM_POST_RESTORE:
		mme_restore();
		break;
	default:
		return NOTIFY_DONE;
	}
	return NOTIFY_OK;
}

static struct notifier_block mme_pm_notifier = {
	.notifier_call = pm_mme_notifier_cb,

};

static const struct dev_pm_ops mme_pm_ops = {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
	.suspend = mme_freeze
#else
	.freeze = mme_freeze, /* hibernation */
#endif
};

#endif


static struct platform_driver mme_driver = {
	.driver.name  = "mme",
	.driver.owner = THIS_MODULE,
#ifdef CONFIG_PM
	/*For power management*/
	.driver.pm = &mme_pm_ops,
#endif

	.probe        = mme_probe,
	.remove       = mme_remove,
};

static void mme_release (struct device *dev) {}

struct platform_device mme_pdev = {
	.name           = "mme",
	.id             = -1,
	.dev.release    = mme_release,
};


int __init mme_mod_init( void )
{

	platform_driver_register(&mme_driver);

	platform_device_register(&mme_pdev);
#ifdef CONFIG_PM
	register_pm_notifier(&mme_pm_notifier);
#endif
	return 0;
}

void __exit mme_mod_exit( void )
{

#ifdef CONFIG_PM
	unregister_pm_notifier(&mme_pm_notifier);
#endif
	platform_device_unregister(&mme_pdev);
	
	platform_driver_unregister(&mme_driver);

	return;
}

module_init(mme_mod_init);
module_exit(mme_mod_exit);

#endif /* defined(__KERNEL__) && defined(MODULE) */

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
