/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#include <mme.h>	/* External defines and prototypes */

#include "_mme_sys.h"	/* Internal defines and prototypes */

#include <mme/mme_ioctl.h>	/* External ioctl interface */

#include "_mme_user.h"		/* Internal definitions */

#include <asm/uaccess.h>

/*
 * Userspace->Kernel wrappers for MME calls
 */

int mme_user_mmeinit(mme_user_t *instance, void *arg)
{
	 int                    res = -ENODEV;
	 MME_ERROR              status = MME_INTERNAL_ERROR;

  mme_userinit_t *user_mmeinit = (mme_userinit_t *) arg;

  /* Init MME */
  status = MME_Init(); 
  if (put_user(status,&(user_mmeinit->status))) {
		  res = -EFAULT;
	 }
 return res;
}

int mme_user_mmeterm(mme_user_t *instance, void *arg)
{
	 int                    res = -ENODEV;
	 MME_ERROR              status = MME_INTERNAL_ERROR;

  mme_userterm_t *user_mmeterm = (mme_userterm_t *) arg;

  /* Init MME */
  status = MME_Term(); 
  if (put_user(status,&(user_mmeterm->status))) {
		  res = -EFAULT;
	 }
 return res;
}
