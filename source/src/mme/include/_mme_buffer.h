/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _MME_BUFFER_SYS_H
#define _MME_BUFFER_SYS_H

#define _MME_BUF_CACHE_FLAGS	ICS_CACHED	/* Default cache flags for MME_DataBuffer_t allocation */

#define _MME_BUF_POOL_SIZE	MME_GetTuneable(MME_TUNEABLE_BUFFER_POOL_SIZE)

/*
 * Internal MME_DataBuffer_t container
 */
typedef struct mme_buffer
{
  MME_DataBuffer_t		buffer;		/* MME_DataBuffer_t presented to user */

  MME_AllocationFlags_t		flags;
  MME_ScatterPage_t		pages[1];

  void                         *owner;		/* DEBUG buffer owner */

} mme_buffer_t;

/* Exported internal APIs */

#endif /* _MME_BUFFER_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
