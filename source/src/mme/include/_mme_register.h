/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _MME_REGISTER_SYS_H
#define _MME_REGISTER_SYS_H

typedef struct mme_transformer_func
{
  MME_AbortCommand_t			abort;
  MME_GetTransformerCapability_t	getTransformerCapability;
  MME_InitTransformer_t			initTransformer;
  MME_ProcessCommand_t			processCommand;
  MME_TermTransformer_t			termTransformer;

} mme_transformer_func_t;

typedef struct mme_transformer_reg
{
  struct list_head		 	list;		/* Doubly linked list element */
  
  mme_transformer_func_t		funcs;		/* Transformer function pointers */

  ICS_NSRV_HANDLE			nhandle;	/* Nameserver registration handle */
  
  /* SUPst00005132*/
  char 					name[MME_MAX_TRANSFORMER_NAME];	/* Variable length string */
  
} mme_transformer_reg_t;


/* Exported internal APIs */
mme_transformer_reg_t * mme_transformer_lookup (const char *name);

#endif /* _MME_REGISTER_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
