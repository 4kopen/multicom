/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _MME_MSG_SYS_H
#define _MME_MSG_SYS_H

#define _MME_MSG_CACHE_FLAGS	ICS_CACHED	/* Default cache flags for MME msg Meta data allocation */

#define _MME_MSG_SMALL_SIZE	1024		/* Maximum size of a cached MME msg buffer (MME meta data) */

typedef struct mme_msg
{
  struct list_head		 list;		/* Doubly linked list */

  MME_UINT			 size;		/* Allocated memory size */
  void				*buf;		/* Companion mapped buffer */
  
  void				*owner;		/* DEBUG: owning function */

} mme_msg_t;

/* Exported internal APIs */
MME_ERROR  mme_msg_init (void);
void       mme_msg_term (void);

mme_msg_t *mme_msg_alloc (MME_UINT size);
void       mme_msg_free (mme_msg_t *msg);

#endif /* _MME_MSG_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
