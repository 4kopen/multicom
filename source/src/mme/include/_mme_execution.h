/*******************************************************************************
 * This file is part of multicom
 * 
 * Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
 * 
 * multicom is dual licensed : you can use it either under the terms of 
 * the GPL V2, or ST Proprietary license, at your option.
 *
 * multicom is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * multicom is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  multicom. If not, see http://www.gnu.org/licenses.
 *
 * multicom may alternatively be licensed under a proprietary 
 * license from ST :
 *
 * STMicroelectronics confidential
 * Reproduction and Communication of this document is strictly 
 * prohibited unless specifically authorized in writing by 
 * STMicroelectronics.
 *******************************************************************************/

/* 
 * 
 */ 

#ifndef _MME_EXECUTION_SYS_H
#define _MME_EXECUTION_SYS_H

#define _MME_EXECUTION_TASKS	(5)		/* Total number of execution tasks */

typedef struct mme_execution_task
{
  _ICS_OS_MUTEX			lock;		/* Lock to protect this structure */

  _ICS_OS_TASK_INFO		task;		/* Task id */

  _ICS_OS_EVENT			event;		/* Task blocks here waiting for work */

  unsigned int			taskStop;	/* Set to non zero to cause task to exit */

  struct list_head		receivers;	/* Linked list of receivers for this task priority */

  ICS_STATS_HANDLE		stats;		/* STATISTICS handle */

} mme_execution_task_t;

/* Exported internal APIs */
MME_ERROR mme_execution_add (mme_receiver_t *receiver, MME_Priority_t priority);
void      mme_execution_remove (mme_receiver_t *receiver);
MME_ERROR mme_execution_term (void);

#endif /* _MME_EXECUTION_SYS_H */

/*
 * Local Variables:
 *  tab-width: 8
 *  c-indent-level: 2
 *  c-basic-offset: 2
 * End:
 */
