#/*******************************************************************************
#* This file is part of multicom
#* 
#* Copyright (C) 2016   STMicroelectronics. All Rights Reserved.
#* 
#* multicom is dual licensed : you can use it either under the terms of 
#* the GPL V2, or ST Proprietary license, at your option.
#*
#* multicom is free software; you can redistribute it and/or
#* modify it under the terms of the GNU General Public License
#* version 2 as published by the Free Software Foundation.
#*
#* multicom is distributed in the hope that it will be
#* useful, but WITHOUT ANY WARRANTY; without even the implied
#* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#* See the GNU General Public License for more details.
#*
#* You should have received a copy of the GNU General Public License
#* along with  multicom. If not, see http://www.gnu.org/licenses.
#*
#* multicom may alternatively be licensed under a proprietary 
#* license from ST :
#*
#* STMicroelectronics confidential
#* Reproduction and Communication of this document is strictly 
#* prohibited unless specifically authorized in writing by 
#* STMicroelectronics.
#*******************************************************************************/

#*******************************************************************************
#
# Generic driver configuration makefile.
#
#*******************************************************************************
#*******************************************************************************

################################################################################

#
# Set list of all possible CPUs
#
ALLCPUS = arm9 st40 st231

#
# Check CPU exists and valid
#
ifeq ($(CPU),)
$(error CPU not defined: $(ALLCPUS))
else
ifeq ($(findstring $(CPU),$(ALLCPUS)),)
$(error CPU ($(CPU)) not supported: $(ALLCPUS))
endif
endif

################################################################################

#
# Set list of all possible CPU platforms
#
ifeq ($(CPU),arm9)
ALLCPUPLATFORMS = null mb453
endif
ifeq ($(CPU),st40)
ALLCPUPLATFORMS  = mb411stb7109 mb411stb7109se mb442stb7109 mb442stb7109se mb519
ALLCPUPLATFORMS += mb519se mb618 mb618se mb628 mb628se mb628ecm mb628ecmse mb628_ecm mb628_se_ecm mb680 mb680se mb680sti7106 mb680sti7106lmise mb671 mb671se mb837se mb837rtse hdk7108 hdk7108_rt hdk7108se hdk7108_rtse b2000_rtse fli7610hdkse
endif
ifeq ($(CPU),st231)
ALLCPUPLATFORMS  = mb411stb7109_audio mb411stb7109_video mb442stb7109_audio mb442stb7109_video mb442_7109_audio mb442_7109_video mb519_audio0 mb519_video0 mb519_audio1 mb519_video1 mb519_se_audio0 mb519_se_video0 mb519_se_audio1 
ALLCPUPLATFORMS += mb519_se_video1 mb618_audio mb618_video mb618_se_audio mb618_se_video mb628_audio mb628_video mb628_se_audio mb628_se_video mb680_video mb680_audio mb680_se_video mb680_se_audio mb680_7106_video mb680_7106_audio 
ALLCPUPLATFORMS += mb680_se_7106_video mb680_se_7106_audio mb671_audio0 mb671_video0 mb671_audio1 mb671_video1 mb671_se_audio0 mb671_se_video0 mb671_se_audio1 mb671_se_video1 mb837_se_audio mb837_se_video mb837_se_gp hdk7108_se_audio
ALLCPUPLATFORMS += hdk7108_se_video hdk7108_se_gp b2000_se_video0 b2000_se_video1 b2000_se_audio0 b2000_se_audio1 b2000_se_gp fli7610hdk_7610_audio fli7610hdk_7610_gp0 fli7610hdk_7610_gp1 fli7610hdk_7610_video0 
ALLCPUPLATFORMS += fli7610hdk_7610_video1
endif

################################################################################

#
# Set list of possible platforms for CPU
#
ALLPLATFORMS = $(ALLCPUPLATFORMS)

#
# Check ALLPLATFORMS is non-empty
#
ifeq ($(ALLPLATFORMS),)
$(error ALLPLATFORMS not defined, no valid platform for CPU ($(CPU)))
endif

#
# Check PLATFORM exists and valid
#
ifeq ($(PLATFORM),)
$(error PLATFORM not defined: $(ALLPLATFORMS))
else
ifeq ($(findstring $(PLATFORM),$(ALLPLATFORMS)),)
$(error PLATFORM ($(PLATFORM)) not supported: $(ALLPLATFORMS))
endif
endif

################################################################################

#
# Set BOARD to the platform
#
BOARD = $(PLATFORM)

################################################################################

#
# Set CHIP type and CC board type for ARM9 platforms changing BOARD as required
#
ifeq ($(CPU),arm9)
ifeq ($(PLATFORM),mb453)
CHIP = stn8815
CORE = arm9
CCPLATFORM = mb453
CCUSERFLAGS += -D__mmc__
endif
ifeq ($(PLATFORM),null)
CHIP = null
CORE = arm9
CCPLATFORM = null
endif
endif
################################################################################

#
# Set CHIP type and CC board type for ST40 platforms changing BOARD as required
#
ifeq ($(CPU),st40)

# Default core name
CORE = st40

ifeq ($(PLATFORM),mb411stb7109)
CHIP = stx7109
CCCHIP = stb7109
CCPLATFORM = $(PLATFORM)p1
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb411stb7109se)
CHIP = stx7109
CCCHIP = stb7109
CCPLATFORM = $(PLATFORM)
CCUSERFLAGS += -DCONF_MASTER
endif

ifeq ($(PLATFORM),mb442stb7109)
CHIP = stx7109
CCCHIP = stb7109
CCPLATFORM = $(PLATFORM)p1
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb442stb7109se)
CHIP = stx7109
CCCHIP = stb7109
CCPLATFORM = $(PLATFORM)
CCUSERFLAGS += -DCONF_MASTER
endif

ifeq ($(PLATFORM),mb519)
CHIP = stx7200
CCCHIP = sti7200
CCUSERFLAGS += -DCONF_MASTER
CCPLATFORM = $(PLATFORM)p1
endif
ifeq ($(PLATFORM),mb519se)
CHIP = stx7200
CCCHIP = sti7200
CCUSERFLAGS += -DCONF_MASTER
CCPLATFORM = $(PLATFORM)
endif

ifeq ($(PLATFORM),mb618)
CHIP = stx7111
CCCHIP = sti7111
CCPLATFORM = $(PLATFORM)p1
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb618se)
CHIP = stx7111
CCCHIP = sti7111
CCPLATFORM = $(PLATFORM)
CCUSERFLAGS += -DCONF_MASTER
endif

ifeq ($(PLATFORM),mb628)
CHIP = stx7141
CCCHIP = sti7141
CORE = estb
CCPLATFORM = $(PLATFORM)_$(CORE)p1
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb628se)
CHIP = stx7141
CCCHIP = sti7141
CORE = estb
CCPLATFORM = mb628_$(CORE)se
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb628ecm)
CHIP = stx7141
CCCHIP = sti7141
CORE = ecm
CCPLATFORM = mb628_$(CORE)p1
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb628ecmse)
CHIP = stx7141
CCCHIP = sti7141
CORE = ecm
CCPLATFORM = mb628_$(CORE)se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb628_ecm)
CHIP = stx7141
CCCHIP = sti7141
CORE = ecm
BOARD = mb628
CCPLATFORM = mb628_$(CORE)p1
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb628_se_ecm)
CHIP = stx7141
CCCHIP = sti7141
CORE = ecm
BOARD = mb628se
CCPLATFORM = mb628_$(CORE)se
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb680)
CHIP = stx7105
CCCHIP = sti7105
CCPLATFORM = $(PLATFORM)p1
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb680se)
CHIP = stx7105
CCCHIP = sti7105
CCPLATFORM = $(PLATFORM)
CCUSERFLAGS += -DCONF_MASTER
endif

ifeq ($(PLATFORM),mb680sti7106)
CHIP = stx7106
CCCHIP = sti7106
CCPLATFORM = $(PLATFORM)p1
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb680sti7106lmise)
CHIP = stx7106
CCCHIP = sti7106
CCPLATFORM = $(PLATFORM)
CCUSERFLAGS += -DCONF_MASTER
endif

ifeq ($(PLATFORM),mb671)
CHIP  = stx7200
CCCHIP = sti7200
CCUSERFLAGS += -DCONF_MASTER
CCPLATFORM = $(PLATFORM)p1
endif
ifeq ($(PLATFORM),mb671se)
CHIP  = stx7200
CCCHIP = sti7200
CCUSERFLAGS += -DCONF_MASTER
CCPLATFORM = $(PLATFORM)
endif

ifeq ($(PLATFORM),mb837se)
CHIP = stx7108
CCCHIP = sti7108
CORE = host
BOARD = mb837se
CCPLATFORM = mb837_$(CORE)lmise
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),mb837rtse)
CHIP = stx7108
CCCHIP = sti7108
CORE = rt
BOARD = mb837se
CCPLATFORM = mb837_$(CORE)lmise
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),hdk7108)
CHIP = stx7108
CCCHIP = sti7108
CORE = host
BOARD = hdk7108
CCPLATFORM = hdk7108_$(CORE)
CCUSERFLAGS += -DCONF_MASTER
endif

ifeq ($(PLATFORM),hdk7108_rt)
CHIP = stx7108
CCCHIP = sti7108
CORE = rt
BOARD = hdk7108
CCPLATFORM = hdk7108_$(CORE)
CCUSERFLAGS += 
#LDFLAGS += startseinit.S -Wl,--entry=__start_se_init -I../source/include/bsp -I../source/src/bsp -D__$(CHIP)__ -DPMBCONFIGINCLUDE=\"rt.romgen\"
#LDFLAGS += ../source/obj/os21/$(CPU)/bsp/$(CHIP)/$(CPU)/$(CORE)/startseinit.o -Wl,--entry=__start_se_init
LDFLAGS += -u__start_se_init -e__start_se_init -Wl,--entry=__start_se_init
endif

ifeq ($(PLATFORM),hdk7108se)
CHIP = stx7108
CCCHIP = sti7108
CORE = host
BOARD = hdk7108se
CCPLATFORM = hdk7108_$(CORE)lmise
CCUSERFLAGS += -DCONF_MASTER
endif
ifeq ($(PLATFORM),hdk7108_rtse)
CHIP = stx7108
CCCHIP = sti7108
CORE = rt
BOARD = hdk7108se
CCPLATFORM = hdk7108_$(CORE)lmise
CCUSERFLAGS +=
endif

ifeq ($(PLATFORM),b2000_rt)
CHIP = stxh415
CCCHIP = stih415
CORE = rt
BOARD =sti415
CCPLATFORM = stih415_$(CORE)
LDFLAGS += -u__start_se_init -e__start_se_init -Wl,--entry=__start_se_init
endif
ifeq ($(PLATFORM),b2000_rtse)
CHIP = stxh415
CCCHIP = stih415
CORE = rt
BOARD = stih415se
CCPLATFORM = b2000lmise
CCUSERFLAGS +=
endif

ifeq ($(PLATFORM),fli7610hdkse)
CHIP = fli7610
CCCHIP = fli7610
CORE = rt
BOARD = fli7610hdk
#CCPLATFORM = fli7610hdklmise
CCPLATFORM = fli7610hdkse
CCUSERFLAGS +=
endif



endif

################################################################################

#
# Set CHIP type and CC board type for ST231 platforms changing BOARD as required
#
ifeq ($(CPU),st231)
ifeq ($(PLATFORM),mb411stb7109_audio)
CHIP = stx7109
CORE = audio
CCPLATFORM = $(PLATFORM)
CCCHIP = stb7109
BOARD=mb411stb7109
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb411stb7109_video)
CHIP = stx7109
CORE = video
CCPLATFORM = $(PLATFORM)
CCCHIP = stb7109
BOARD=mb411stb7109
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb442_7109_audio)
CHIP = stx7109
CORE = audio
CCPLATFORM = mb442_7109_audio
CCCHIP = stb7109
BOARD=mb442stb7109
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb442_7109_video)
CHIP = stx7109
CORE = video
CCPLATFORM = mb442_7109_video
CCCHIP = stb7109
BOARD=mb442stb7109
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb442stb7109_audio)
CHIP = stx7109
CORE = audio
CCPLATFORM = mb442stb7109_audio
CCCHIP = stb7109
BOARD=mb442stb7109
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb442stb7109_video)
CHIP = stx7109
CORE = video
CCPLATFORM = mb442stb7109_video
CCCHIP = stb7109
BOARD=mb442stb7109
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb519_audio0)
CHIP   = stx7200
CCCHIP = sti7200
CORE = audio0
CCPLATFORM = mb519_$(CORE)
BOARD=mb519
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb519_video0)
CHIP   = stx7200
CCCHIP = sti7200
CORE = video0
CCPLATFORM = mb519_$(CORE)
BOARD=mb519
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb519_audio1)
CHIP   = stx7200
CCCHIP = sti7200
CORE = audio1
CCPLATFORM = mb519_$(CORE)
BOARD=mb519
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb519_video1)
CHIP   = stx7200
CCCHIP = sti7200
CORE = video1
CCPLATFORM = mb519_$(CORE)
BOARD=mb519
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb519_se_audio0)
CHIP   = stx7200
CCCHIP = sti7200
CORE = audio0
CCPLATFORM = mb519_se_$(CORE)
BOARD=mb519se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb519_se_video0)
CHIP   = stx7200
CCCHIP = sti7200
CORE = video0
CCPLATFORM = mb519_se_$(CORE)
BOARD=mb519se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb519_se_audio1)
CHIP   = stx7200
CCCHIP = sti7200
CORE = audio1
CCPLATFORM = mb519_se_$(CORE)
BOARD=mb519se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb519_se_video1)
CHIP   = stx7200
CCCHIP = sti7200
CORE = video1
CCPLATFORM = mb519_se_$(CORE)
BOARD=mb519se
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb618_audio)
CHIP = stx7111
CCCHIP = sti7111
CORE = audio
CCPLATFORM = mb618_$(CORE)
BOARD=mb618
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb618_video)
CHIP = stx7111
CCCHIP = sti7111
CORE = video
CCPLATFORM = mb618_$(CORE)
BOARD=mb618
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb618_se_audio)
CHIP = stx7111
CCCHIP = sti7111
CORE = audio
CCPLATFORM = mb618_se_$(CORE)
BOARD=mb618se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb618_se_video)
CHIP = stx7111
CCCHIP = sti7111
CORE = video
CCPLATFORM = mb618_se_$(CORE)
BOARD=mb618se
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb628_audio)
CHIP = stx7141
CCCHIP = sti7141
CORE = audio
CCPLATFORM = mb628_$(CORE)
BOARD=mb628
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb628_video)
CHIP = stx7141
CCCHIP = sti7141
CORE = video
CCPLATFORM = mb628_$(CORE)
BOARD=mb628
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb628_se_audio)
CHIP = stx7141
CCCHIP = sti7141
CORE = audio
CCPLATFORM = mb628_se_$(CORE)
BOARD=mb628se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb628_se_video)
CHIP = stx7141
CCCHIP = sti7141
CORE = video
CCPLATFORM = mb628_se_$(CORE)
BOARD=mb628se
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb680_audio)
CHIP = stx7105
CCCHIP = sti7105
CORE = audio
CCPLATFORM = mb680_$(CORE)
BOARD=mb680
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb680_video)
CHIP = stx7105
CCCHIP = sti7105
CORE = video
CCPLATFORM = mb680_$(CORE)
BOARD=mb680
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb680_se_audio)
CHIP = stx7105
CCCHIP = sti7105
CORE = audio
CCPLATFORM = mb680_se_$(CORE)
BOARD=mb680se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb680_se_video)
CHIP = stx7105
CCCHIP = sti7105
CORE = video
CCPLATFORM = mb680_se_$(CORE)
BOARD=mb680se
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb680_7106_audio)
CHIP = stx7106
CCCHIP = sti7106
CORE = audio
CCPLATFORM = mb680_7106_$(CORE)
BOARD=mb680sti7106
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb680_7106_video)
CHIP = stx7106
CCCHIP = sti7106
CORE = video
CCPLATFORM = mb680_7106_$(CORE)
BOARD=mb680sti7106
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb680_se_7106_audio)
CHIP = stx7106
CCCHIP = sti7106
CORE = audio
CCPLATFORM = mb680_se_7106_$(CORE)
BOARD=mb680sti7106lmise
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb680_se_7106_video)
CHIP = stx7106
CCCHIP = sti7106
CORE = video
CCPLATFORM = mb680_se_7106_$(CORE)
BOARD=mb680sti7106lmise
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb671_audio0)
CHIP = stx7200
CCCHIP = sti7200
CORE = audio0
CCPLATFORM = mb671_$(CORE)
BOARD=mb671
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb671_video0)
CHIP = stx7200
CCCHIP = sti7200
CORE = video0
CCPLATFORM = mb671_$(CORE)
BOARD=mb671
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb671_audio1)
CHIP = stx7200
CCCHIP = sti7200
CORE = audio1
CCPLATFORM = mb671_$(CORE)
BOARD=mb671
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb671_video1)
CHIP = stx7200
CCCHIP = sti7200
CORE = video1
CCPLATFORM = mb671_$(CORE)
BOARD=mb671
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb671_se_audio0)
CHIP = stx7200
CCCHIP = sti7200
CORE = audio0
CCPLATFORM = mb671_se_$(CORE)
BOARD=mb671se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb671_se_video0)
CHIP = stx7200
CCCHIP = sti7200
CORE = video0
CCPLATFORM = mb671_se_$(CORE)
BOARD=mb671se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb671_se_audio1)
CHIP = stx7200
CCCHIP = sti7200
CORE = audio1
CCPLATFORM = mb671_se_$(CORE)
BOARD=mb671se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb671_se_video1)
CHIP = stx7200
CCCHIP = sti7200
CORE = video1
CCPLATFORM = mb671_se_$(CORE)
BOARD=mb671se
CCUSERFLAGS +=  
endif

ifeq ($(PLATFORM),mb837_se_audio)
CHIP = stx7108
CCCHIP = stx7108
CORE = audio
CCPLATFORM = mb837_se_7108_$(CORE)
BOARD=mb837se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb837_se_video)
CHIP = stx7108
CCCHIP = stx7108
CORE = video
CCPLATFORM = mb837_se_7108_$(CORE)
BOARD=mb837se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),mb837_se_gp)
CHIP = stx7108
CCCHIP = stx7108
CORE = gp
CCPLATFORM = mb837_se_7108_$(CORE)
BOARD=mb837se
CCUSERFLAGS +=  
endif
ifeq ($(PLATFORM),hdk7108_se_audio)
CHIP = stx7108
CCCHIP = stx7108
CORE = audio
BOARD = hdk7108se
CCPLATFORM = hdk7108_se_7108_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),hdk7108_se_video)
CHIP = stx7108
CCCHIP = stx7108
CORE = video
BOARD = hdk7108se
CCPLATFORM = hdk7108_se_7108_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),hdk7108_se_gp)
CHIP = stx7108
CCCHIP = stx7108
CORE = gp
BOARD = hdk7108se
CCPLATFORM = hdk7108_se_7108_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),b2000_se_video0)
CHIP = stxh415
CCCHIP = stxh415
CORE = video0
BOARD = stih415se
CCPLATFORM = b2000_h415_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),b2000_se_video1)
CHIP = stxh415
CCCHIP = stxh415
CORE = video1
BOARD = stih415se
CCPLATFORM = b2000_h415_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),b2000_se_audio0)
CHIP = stxh415
CCCHIP = stxh415
CORE = audio0
BOARD = stih415se
CCPLATFORM = b2000_h415_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),b2000_se_audio1)
CHIP = stxh415
CCCHIP = stxh415
CORE = audio1
BOARD = stih415se
CCPLATFORM = b2000_h415_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),b2000_se_gp)
CHIP = stxh415
CCCHIP = stxh415
CORE = gp
BOARD = stih415se
CCPLATFORM = b2000_h415_$(CORE)
CCUSERFLAGS +=
endif

ifeq ($(PLATFORM),fli7610hdk_7610_audio)
CHIP = fli7610
CCCHIP = fli7610
CORE = audio
BOARD = fli7610hdk
CCPLATFORM = fli7610hdk_7610_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),fli7610hdk_7610_gp0)
CHIP = fli7610
CCCHIP = fli7610
CORE = gp0
BOARD = fli7610hdk
CCPLATFORM = fli7610hdk_7610_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),fli7610hdk_7610_gp1)
CHIP = fli7610
CCCHIP = fli7610
CORE = gp1
BOARD = fli7610hdk
CCPLATFORM = fli7610hdk_7610_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),fli7610hdk_7610_video0)
CHIP = fli7610
CCCHIP = fli7610
CORE = video0
BOARD = fli7610hdk
CCPLATFORM = fli7610hdk_7610_$(CORE)
CCUSERFLAGS +=
endif
ifeq ($(PLATFORM),fli7610hdk_7610_video1)
CHIP = fli7610
CCCHIP = fli7610
CORE = video1
BOARD = fli7610hdk
CCPLATFORM = fli7610hdk_7610_$(CORE)
CCUSERFLAGS +=
endif


endif

################################################################################

#
# Set CCCHIP to CHIP if not already defined
#
ifeq ($(CCCHIP),)
CCCHIP = $(CHIP)
endif

################################################################################

#
# Set CC defines for PLATFORM, CHIP and CPU types
#
ifneq ($(PLATFORM),)
CCUSERFLAGS += -D__$(PLATFORM)__
else
$(error PLATFORM not defined)
endif
ifneq ($(CHIP),)
CCUSERFLAGS += -D__$(CHIP)__
else
$(error CHIP not defined)
endif
ifneq ($(CPU),)
CCUSERFLAGS += -D__$(CPU)__
else
$(error CPU not defined)
endif
ifneq ($(CORE),)
CCUSERFLAGS += -D__$(CORE)__
endif

################################################################################
