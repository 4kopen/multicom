# 
# Linux specific Makefile  ------ --
#
# Copyright STMicroelectronics 2014, all rights reserved. 
#
# this makefile $ make -f Makefile create ssymbolic on all file 
# in a dedicated linux directory $(LinuxDIR) for building Multicom with Linux 

#dir name where to store the linux tree
LinuxDIR=linux
SourceDIR=`pwd`/source
all:
	@echo "Check if rebuild needed++++"
	@mkdir -p $(LinuxDIR)
	@if [ -e "$(LinuxDIR)/multicom.md5sum" ]; then  mv  $(LinuxDIR)/multicom.md5sum  $(LinuxDIR)/multicom.md5sum.old;else rm -f $(LinuxDIR)/multicom.md5sum.old; echo "empty"> $(LinuxDIR)/multicom.md5sum.old;fi
	@find ./source -type f -print0 | xargs -0 md5sum > $(LinuxDIR)/multicom.md5sum
	@if [ `md5sum $(LinuxDIR)/multicom.md5sum | head -n1 | cut -d " " -f1` != `md5sum $(LinuxDIR)/multicom.md5sum.old | head -n1 | cut -d " " -f1` ]; then echo "rebuilding"; make build;else echo "no-rebuilding";fi
	@rm $(LinuxDIR)/multicom.md5sum.old
	@mkdir -p $(LinuxDIR)/Documentation/devicetree/bindings/sti

build:
	cp  -rs $(SourceDIR)/* $(LinuxDIR)
	exit 0


clean: 
	@rm -rf $(LinuxDIR)

