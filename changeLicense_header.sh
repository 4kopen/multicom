#!/bin/sh

cd /local/multicomrepo/multicom/source

#for i in `find * -mindepth 1 -type f -name '*.c'`
for i in `find * -type f -iname *.[ch]`
do
	tail -n +11 $i > $i.tmp
	cat ../dualLicense_header.txt $i.tmp > $i
	rm $i.tmp
done
for i in `find * -type f -iname 'make*'`
do
	tail -n +11 $i > $i.tmp
	cat ../dualLicense_headerM.txt $i.tmp > $i
	rm $i.tmp
done
cd ..







